<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="st" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="law" uri="http://lawweb.lawbroker.com.tw/taglib"%>

<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/resources/css/login.css?v=${_templateVer}">
<script type="text/javascript" src="${pageContext.request.contextPath}/resources/js/views/system/template/password.js?v=${_templateVer}"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/resources/bundle/jquery-easyui-exts/validatebox-ext.js?v=${_templateVer}"></script>

<%-- 儲存 app.properties 中 law.module 的設定值到 _module --%>
<st:eval var="_module" expression="@propertyConfigurer.getProperty('law.module')" />

<%-- 取得 menu JSON string --%>
<law:functionMenuAsJSON var="_functionMenu" module="${_module}" auth="false" lang="${pageContext.request.locale}" />

<st:url var="authUrl" value="/static/j_spring_security_check" />
<st:url var="getOtpUrl" value="/getOTP" />
<st:url var="cancelOtpUrl" value="/cancelOTP" />
<st:url var="changePasswordUrl" value="/changePassword" />

<!-- 非 Chrome 瀏覽器警告 -->
<div id="warning" style="display: none; background-color: red; color: white; font-size: 18px; font-weight: bold; text-align: center; padding: 5px">
	<st:message code="law.warning.unsupportedBrowser" />
</div>

<div id="loginPane">

	<!-- 公司 Logo -->
	<div align="center" style="margin: 0px auto 30px">
		<img src="<c:url value="/resources/images/company/logo_bold.gif"/>" />
	</div>

	<div class="container">
		<div id="tabbox">
			<!-- 登入頁籤 -->
			<a id="loginTab" href="javascript:showLogin();" class="tab selected"><st:message code="law.login.tab.label" /></a>
			<!-- 變更/忘記密碼頁籤 -->
			<a id="changePasswordTab" href="javascript:showChangePassword();" class="tab"><st:message code="law.changePassword.tab.label" /></a>
		</div>

		<!-- 登入區塊 -->
		<div id="loginPanel" class="formPanel">
			<form id="loginForm" method="post" action="${authUrl}">
				<table class="formTable">
					<tr>
						<td class="moduleCell"><span id="module"></span></td>
					</tr>
					<tr>
						<td style="height: 5px"></td>
					</tr>
					<tr>
						<!-- 業代 -->
						<td class="inputCell"><input style="height: 33px; width: 250px" class="easyui-textbox" data-options="missingMessage:'<st:message code="law.login.emptyFieldWarning" />'" required="true"
							value="" id="j_username" name="j_username" type="text" iconCls="icon-identity" prompt="<st:message code="law.login.eno.inputPrompt" />" /></td>
					</tr>
					<tr>
						<!-- 密碼 -->
						<td class="inputCell"><input style="height: 33px; width: 250px" class="easyui-textbox" data-options="missingMessage:'<st:message code="law.login.emptyFieldWarning" />'" required="true"
							value="" id="j_password" name="j_password" type="password" iconCls="icon-lock" prompt="<st:message code="law.login.password.inputPrompt" />" /></td>
					</tr>
					<tr>
						<!-- 驗證碼 -->
						<td class="inputCell"><input style="height: 33px; width: 250px" id="kaptcha" name="kaptcha" type="text" class="easyui-textbox"
							data-options="missingMessage:'<st:message code="law.login.emptyFieldWarning" />'" required="true" maxlength="4" iconCls="icon-numbers"
							prompt="<st:message code="law.login.validationNo.inputPrompt" />" /></td>
					</tr>
					<tr>
						<!-- 產生驗證碼 -->
						<td style="text-align: center; vertical-align: middle;"><img src="<c:url value="/captcha"/>" width="100" height="22" id="kaptchaImage" onclick="getCaptcha('kaptchaImage');"
							title="<st:message code="law.login.validationNo.hint" />" style="cursor: pointer" /> <span class="icon-arrowLeft" style="display: inline-block; height: 8px; width: 16px; margin-right: 3px"></span><span
							style="font-size: 8px"><st:message code="law.login.validationNo.hint" /></span></td>
					</tr>
					<tr>
						<td>
							<!-- 登入系統 --> <a href="javascript:login();" class="easyui-linkbutton" iconCls="icon-ok" data-options="width:250"><st:message code="law.login.login" /></a><br /> <!-- 重新輸入 --> <a
							href="javascript:resetLogin();" class="easyui-linkbutton" iconCls="icon-undo" data-options="width:250" style="margin-top: 5px"><st:message code="law.login.clear" /></a>
						</td>
					</tr>
				</table>
			</form>
		</div>

		<!-- 變更密碼區塊 -->
		<div id="changePasswordPanel" class="formPanel">
			<form id="getOtpForm" method="post" action="${getOtpUrl}">
				<table class="formTable">
					<tr>
						<!-- 業代 -->
						<td class="inputCell"><input style="height: 33px; width: 250px" class="easyui-textbox" data-options="required:true,missingMessage:'<st:message code="law.login.emptyFieldWarning" />'"
							required="true" value="" id="eno" name="eno" type="text" iconCls="icon-identity" prompt="<st:message code="law.login.eno.inputPrompt" />" /></td>
					</tr>
					<tr>
						<!-- 驗證碼 -->
						<td class="inputCell"><input style="height: 33px; width: 250px" id="kaptcha2" name="kaptcha" type="text" class="easyui-textbox"
							data-options="missingMessage:'<st:message code="law.login.emptyFieldWarning" />'" required="true" iconCls="icon-numbers" prompt="<st:message code="law.login.validationNo.inputPrompt" />" /></td>
					</tr>
					<tr>
						<!-- 產生驗證碼 -->
						<td style="text-align: center"><img src="<c:url value="/captcha"/>" width="100" height="22" id="kaptchaImage2" onclick="getCaptcha('kaptchaImage2');"
							title="<st:message code="law.login.validationNo.hint" />" style="cursor: pointer" /> <span class="icon-arrowLeft" style="display: inline-block; height: 8px; width: 16px; margin-right: 3px"></span><span
							style="font-size: 8px"><st:message code="law.login.validationNo.hint" /></span></td>
					</tr>
					<tr>
						<td>
							<!-- 取得OTP碼 --> <a href="javascript:acquireOTP();" class="easyui-linkbutton" iconCls="icon-ok" data-options="width:250"><st:message code="law.otp.acquire" /></a>
						</td>
					</tr>
				</table>
			</form>

			<form id="changePasswordForm" method="post" action="${changePasswordUrl}" style="display: none">
				<!-- OTP 識別碼 -->
				<input type="hidden" id="otpUUID" name="otpUUID">
				<!-- 業代 -->
				<input type="hidden" id="eno2" name="eno">

				<table class="formTable">
					<tr id="otpUuidRow">
						<td style="color: blue; font-weight: bold;"></td>
					</tr>
					<tr id="otpCountDown">
						<td style="color: brown; fone-weight: bold"></td>
					</tr>
					<tr>
						<!-- OTP -->
						<td class="inputCell"><input style="height: 33px; width: 250px" class="easyui-textbox" data-options="missingMessage:'<st:message code="law.login.emptyFieldWarning" />'" required="true"
							value="" id="otp" name="otp" type="text" iconCls="icon-identity" prompt="<st:message code="law.otp.inputPrompt" />" /></td>
					</tr>
					<tr>
						<!-- 新密碼 -->
						<td class="inputCell"><input style="height: 33px; width: 250px" class="easyui-textbox"
							data-options="validType:'minLength[6]',missingMessage:'<st:message code="law.login.emptyFieldWarning" />'" required="true" value="" id="newPassword" name="newPassword" type="password"
							iconCls="icon-lock" prompt="<st:message code="law.changePassword.newPassword.inputPrompt" />" /> <!-- 密碼強度 --> <span id="pwdComplexity"></span></td>
					</tr>
					<tr>
						<!-- 再一次新密碼 -->
						<td class="inputCell"><input style="height: 33px; width: 250px" class="easyui-textbox"
							data-options="validType:'minLength[6]',missingMessage:'<st:message code="law.login.emptyFieldWarning" />'" required="true" value="" id="newPassword2" name="newPassword2" type="password"
							iconCls="icon-lock" prompt="<st:message code="law.changePassword.newPassword.reinputPrompt" />" /></td>
					</tr>

					<tr>
						<td>
							<!-- 送出 --> <a href="javascript:submitChangePassword();" class="easyui-linkbutton" iconCls="icon-ok" data-options="width:250"><st:message code="law.button.submit" /></a><br /> <!-- 取消 --> <a
							href="javascript:cancelOTP();" class="easyui-linkbutton" iconCls="icon-undo" data-options="width:250" style="margin-top: 5px"><st:message code="law.button.cancel" /></a>
						</td>
					</tr>
				</table>
			</form>
		</div>

	</div>

	<!-- footer -->
	<jsp:include page="/WEB-INF/views/system/panel/footer.jsp" />
</div>

<script type="text/javascript">
	$(document).ready(function() {

		// 設定 title 名稱
		var module = JSON.parse('${_functionMenu}');
		$("#module").text("${_module} " + module[0].attributes.name);
		debugger
		// 按 Enter 鍵時, 執行 login()
		$("#loginForm").keypress(function(event) {
			if (event.keyCode == 13) {
				$("input").blur(); // 須先將 focus 移出輸入欄位, 否則最後輸入的欄位值無法寫入 input field
				login();
			}
		});

		// 按 Enter 鍵時, 執行 acquireOTP()
		$("#getOtpForm").keypress(function(event) {
			if (event.keyCode == 13) {
				$("input").blur(); // 須先將 focus 移出輸入欄位, 否則最後輸入的欄位值無法寫入 input field
				acquireOTP();
			}
		});

		// 按 Enter 鍵時, 執行 submitchangePassword()
		$("#changePasswordForm").keypress(function(event) {
			if (event.keyCode == 13) {
				$("input").blur(); // 須先將 focus 移出輸入欄位, 否則最後輸入的欄位值無法寫入 input field
				submitChangePassword();
			}
		});

		$(".easyui-textbox").textbox(); // 強制先產生 easyui object

	});

	$(window).load(function() {

		// 輸入新密碼時, 顯示新密碼複雜度
		$("#newPassword").textbox("textbox").keyup(function(event) {
			// 截掉空白的字元
			var pwd = $("#newPassword").textbox("textbox").val().replace(/ /g, "");
			$("#newPassword").textbox("setValue", pwd);

			if (pwd.length == 0)
				$("#pwdComplexity").removeClass().text(""); // 新密碼為空時, 清空複雜度顯示文字
			else {
				var complexity = Password.testStrength(pwd); // 檢測密碼複雜度
				var complexityName, complexityClass;

				if (complexity == Password.complexity.INSUFFICIENT) {
					complexityClass = "pwdComplexityInsufficient";
					complexityName = "<st:message code="law.changePassword.pwdComplexity.insufficient" />";
				} else if (complexity == Password.complexity.WEAK) {
					complexityClass = "pwdComplexityWeak";
					complexityName = "<st:message code="law.changePassword.pwdComplexity.weak" />";
				} else if (complexity == Password.complexity.MEDIUM) {
					complexityClass = "pwdComplexityMedium";
					complexityName = "<st:message code="law.changePassword.pwdComplexity.medium" />";
				} else if (complexity == Password.complexity.STRONG) {
					complexityClass = "pwdComplexityStrong";
					complexityName = "<st:message code="law.changePassword.pwdComplexity.strong" />";
				}

				// 顯示複雜度
				$("#pwdComplexity").removeClass().addClass(complexityClass).text("<st:message code="law.changePassword.pwdComplexity" /> " + complexityName);
			}
		});

		if ("${param.login_error}" == "t") { // 登入失敗顯示錯誤訊息
			showLogin();

			$.messager.alert('<st:message code="law.login.sysInfoLabel" />', '<st:message code="law.login.loginFailed" />', 'error', function() {
				$("#j_username").textbox("textbox").focus();
			});
		} else if ("${param.accountNotExisted}" == "t") { // 帳號不存在 Employee / EmpPwd
			showLogin();

			$.messager.alert('<st:message code="law.login.sysInfoLabel" />', '<st:message code="law.login.accountNotExisted" />', 'error', function() {
				$("#j_username").textbox("textbox").focus();
			});
		} else if ("${param.accountExpired}" == "t") { // 帳號失效
			showLogin();

			$.messager.alert('<st:message code="law.login.sysInfoLabel" />', '<st:message code="law.login.accountExpired" />', 'error', function() {
				$("#j_username").textbox("textbox").focus();
			});
		} else if ("${param.accountLocked}" == "t") { // 帳號鎖住
			showLogin();

			$.messager.alert('<st:message code="law.login.sysInfoLabel" />', '<st:message code="law.login.accountLocked" />', 'error', function() {
				$("#j_username").textbox("textbox").focus();
			});
		} else if ("${param.credentialExpired}" == "t") { // 密碼逾期
			showLogin();

			$.messager.alert('<st:message code="law.login.sysInfoLabel" />', '<st:message code="law.login.credentialExpired" />', 'error', function() {
				$("#j_username").textbox("textbox").focus();
			});
		} else {
			showLogin();
			$("#j_username").textbox("textbox").focus();
		}

		// 非使用 Google Chrome 瀏覽器時, 顯示警告訊息
		if (navigator.userAgent.search("Chrome") < 0) {
			$("#warning").fadeIn("slow");
		}

		// 業代 textbox 轉英文字母為大寫
		$("#j_username").textbox("textbox").css("text-transform", "uppercase");
		$("#eno").textbox("textbox").css("text-transform", "uppercase");

		// 將 easyui textbox 輸入欄位的圖示設為不 tab stop 
		$(".textbox-icon").attr("tabIndex", -1);

		// 產生驗證碼提示 tooltip
		$("#kaptchaImage,#kaptchaImage2").tooltip({
			position : "left",
			content : "<span style='color:gray'><st:message code="law.login.validationNo.hint" /></span>",

			onShow : function() {
				$(this).tooltip("tip").css("left", $(this).offset().left - 130);
			}
		});
	});

	// 取得驗證碼
	function getCaptcha(id) {
		$('#' + id).hide().attr('src', '<c:url value="/captcha" />?' + Math.floor(Math.random() * 100)).fadeIn();
	}

	// 登入動作
	function login() {
		if ($('#loginForm').form('validate')) {
			// 須將輸入的業代值轉大寫 (雖 CSS text-transform 已將輸入轉為大寫, 但實際取得的值還是小寫)
			$("#j_username").textbox("setValue", $("#j_username").textbox("getValue").toUpperCase());

			// 送出表單
			$("#loginForm").submit();
		}
	}

	// 重新輸入
	function resetLogin() {
		$("#loginForm .easyui-textbox").textbox("reset");
		$("#j_username").textbox("textbox").focus(); // 游標移到業代欄位, LawTemplate 2.0+
	}

	// 顯示登入頁籤內容
	function showLogin() {
		$("#changePasswordTab").removeClass("selected");
		$("#loginTab").addClass("selected");

		$("#changePasswordPanel").hide();
		$("#loginPanel").show();

		getCaptcha("kaptchaImage");

		$("#j_username").textbox("textbox").focus(); // 游標移到業代欄位, LawTemplate 2.0+
	}

	// 顯示變更密碼頁籤內容
	function showChangePassword() {
		$("#loginTab").removeClass("selected");
		$("#changePasswordTab").addClass("selected");

		$("#loginPanel").hide();
		$("#changePasswordPanel").show();

		getCaptcha("kaptchaImage2");

		$("#eno").textbox("textbox").focus(); // 游標移到業代欄位, LawTemplate 2.0+
	}

	// 送出變更密碼
	function submitChangePassword() {
		if ($("#changePasswordForm").form("validate")) {
			// 比較新密碼是否一致
			if ($("#newPassword").val() != $("#newPassword2").val()) {
				$.messager.alert("<st:message code="law.login.sysInfoLabel" />", "<st:message code="law.changePassword.error.inconsistentNewPwds" />", "error", function() {
					$("#newPassword").textbox("textbox").focus();
				});
			} else {
				$("body").mask("<st:message code="law.changePassword.submitting" />"); // 遮罩

				// 須將輸入的業代值轉大寫 (雖 CSS text-transform 已將輸入轉為大寫, 但實際取得的值還是小寫)
				$("#eno").textbox("setValue", $("#eno").textbox("getValue").toUpperCase());

				// 送出表單
				$("#changePasswordForm").form("submit", {
					url : "${changePasswordUrl}",

					success : function(data) {
						$("body").unmask(); // 解除遮罩

						var result = JSON.parse(data);

						if (result.success) { // 密碼變更成功
							// messagebox 顯示訊息
							$.messager.alert("<st:message code="law.login.sysInfoLabel" />", result.message, "info", function() {
								cancelOTP();
								$("#otpCountDown").data("seconds", -1); // 設為 -1, 讓 otpCountDown() 停止倒數
							});

						} else { // 密碼變更失敗

							// messagebox 顯示錯誤訊息
							$.messager.alert("<st:message code="law.login.sysInfoLabel" />", result.message, "error", function() {

							});
						}
					}
				})
			}
		}
	}

	// 取得 OTP
	function acquireOTP() {

		if ($("#getOtpForm").form("validate")) {
			$("body").mask("<st:message code="law.otp.acquiring" />"); // 遮罩

			// 須將輸入的業代值轉大寫 (雖 CSS text-transform 已將輸入轉為大寫, 但實際取得的值還是小寫)
			$("#eno").textbox("setValue", $("#eno").textbox("getValue").toUpperCase());

			// Submit
			$("#getOtpForm").form("submit", {
				url : "${getOtpUrl}",

				success : function(data) {
					$("body").unmask(); // 解除遮罩

					var result = JSON.parse(data);

					if (result.success) { // 取得 OTP 成功

						$("#getOtpForm").form("clear").hide(); // 隱藏取得 OTP form
						$("#changePasswordForm").show(); // 顯示變更密碼 form

						// 變更密碼 form 隱藏欄位
						$("#otpUUID").val(result.message.uuid);
						$("#eno2").val(result.message.eno);

						// OTP 有效時限倒數						
						$("#otpCountDown").data("seconds", result.message.expMins * 60);
						otpCountDown();

						// 顯示 OTP 提示訊息
						$("#otpUuidRow td").text(result.message.uuidMessage);

						$.messager.alert("<st:message code="law.login.sysInfoLabel" />", result.message.phoneMessage, "info", function() {
							$("#otp").textbox("textbox").focus();
						});

					} else { // 取得 OTP 失敗

						// messagebox 顯示錯誤訊息
						$.messager.alert("<st:message code="law.login.sysInfoLabel" />", result.message, "error", function() {
							getCaptcha("kaptchaImage2"); // 更新驗證碼
							$("#kaptcha2").textbox("clear").textbox("textbox").focus(); // 清空輸入驗證碼
						});
					}
				}
			});
		}
	}

	// 取消 OTP
	function cancelOTP() {
		// 變更密碼 form
		$("#changePasswordForm").form("clear").hide(); // 清空欄位 & 隱藏 form
		$("#otpUuidRow td,#pwdComplexity").text(""); // 清空顯示文字

		// 取得 OTP form
		$("#getOtpForm").show();
		getCaptcha("kaptchaImage2");
		$("#eno").textbox("textbox").focus();
	}

	// OTP 有效時間倒數
	function otpCountDown() {
		var seconds = $("#otpCountDown").data("seconds");

		if (seconds < 0) { // 停止倒數
			return;
		} else if (seconds == 0) { // 倒數結數顯示訊息
			$.messager.alert("<st:message code="law.login.sysInfoLabel" />", "<st:message code="law.otp.error.expired" />", "warn", function() {
				cancelOTP();
			});
		} else { // 顯示倒數訊息
			$("#otpCountDown td").text("<st:message code="law.otp.countDown" /> " + --seconds);
			$("#otpCountDown").data("seconds", seconds);
			setTimeout(otpCountDown, 1000);
		}
	}
</script>