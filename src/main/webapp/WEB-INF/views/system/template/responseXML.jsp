<%@ page language="java" contentType="text/xml; charset=UTF-8" pageEncoding="UTF8"%>

<lawException>
	<level>${ex.level}</level>
	<message><![CDATA[${ex.message}]]></message>
</lawException>
