<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="st" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="tiles" uri="http://tiles.apache.org/tags-tiles"%>
<%@page import="java.util.Map,java.util.Map.Entry"%>

<%-- 保險公司經代網 POST 介接頁面 --%>

<html>
<head>
<title><st:message code="law.broker.title" /></title>
</head>
<body>
	<h2>
		<st:message code="law.broker.connecting" />
	</h2>

	<%-- Form for POST --%>
	<form id="frm" action="${url}" method="post" style="display: none">
		<%
			// POST 參數
			Map<?, ?> params = (Map<?, ?>) request.getAttribute("params");

			if (params != null) {
				for (Entry<?, ?> entry : params.entrySet()) {
					out.println("<input type='text' name='" + entry.getKey() + "' value='" + entry.getValue() + "'>");
				}
			}
		%>
	</form>

	<script type="text/javascript">
		document.getElementById("frm").submit();
	</script>
</body>
</html>