<%@ page language="java" contentType="text/html; charset=UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="st" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="ss" uri="http://www.springframework.org/security/tags"%>
<%@ taglib prefix="tiles" uri="http://tiles.apache.org/tags-tiles"%>
<%@ taglib prefix="law" uri="http://lawweb.lawbroker.com.tw/taglib"%>

<%@ include file="include/_var.jsp"%>

<%-- 以下 DOCTYPE 宣告會使 easyui datagrid detail view 在 IE 8 上顯示不正常, 暫先移除
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01//EN" "http://www.w3.org/TR/html4/strict.dtd">
--%>
<!DOCTYPE html>
<html lang="en">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta name="description" content="">
<meta name="author" content="">
<link rel="shortcut icon" href="${pageContext.request.contextPath}/resources/images/company/icon_logo.ico?v=${_templateVer}"></link>
<link rel="icon" href="${pageContext.request.contextPath}/resources/images/company/icon_logo.ico?v=${_templateVer}"></link>
<link rel="bookmark" href="${pageContext.request.contextPath}/resources/images/company/icon_logo.ico?v=${_templateVer}"></link>
<title>${_functionId == "" ? _companyTitle : _functionId}-<tiles:getAsString name="title" /></title>

<%@ include file="include/_jquery.jsp"%>

<script type="text/javascript" src="${_contextPath}/resources/js/json/json2.js?v=${_templateVer}"></script>
<script type="text/javascript" src="${_contextPath}/resources/js/common/jquery.cookie.js?v=${_templateVer}"></script>
<script type="text/javascript" src="${_contextPath}/resources/js/common/jquery.loadmask.min.js?v=${_templateVer}"></script>
<script type="text/javascript" src="${_contextPath}/resources/js/views/system/template/composite.js?v=${_templateVer}"></script>



<script src="${_contextPath}/resources/js/moment.js"></script>

<script src="${_contextPath}/resources/js/external/jquery/jquery.js"></script>
<script src="${_contextPath}/resources/js/jquery-ui.js"></script>
<script src="${_contextPath}/resources/js/boostrapComponents/bootstrap-treeview.js" type="text/javascript" ></script>
<script src="${_contextPath}/resources/js/boostrapComponents/bootstrap-datepicker.js"></script>    
<link href="${_contextPath}/resources/css/bootstrap.min.css" rel="stylesheet">
<link href="${_contextPath}/resources/css/sb-admin.css" rel="stylesheet">
<link href="${_contextPath}/resources/css/style.css" rel="stylesheet" type="text/css" >
<link href="${_contextPath}/resources/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
<link href="${_contextPath}/resources/css/jquery-ui.css" rel="stylesheet">

<link rel="stylesheet" href="${_contextPath}/resources/owl-carousel/owl.carousel.css">
<link rel="stylesheet" href="${_contextPath}/resources/owl-carousel/owl.theme.css">
<script src="${_contextPath}/resources/owl-carousel/owl.carousel.js"></script>
<script type="text/javascript" src="${_contextPath}/resources/js/common/util/datetime/DateTimeUtil.js"></script>
</head>

<body style="padding: 0px">
	<!-- 供 composite.js 取得 context path -->
	<span id="_contextPath" style="display: none">${_contextPath}</span>
	 <div id="wrapper">
		<!-- 第一層 Layout -->
		<c:if test="${UI_MODE != 'APP'}">
			<nav class="navbar navbar-inverse navbar-fixed-top" role="navigation" style="margin-bottom: 0">
				<!-- header區塊 -->
				<c:if test="${_header != ''}">
					<tiles:insertAttribute name="header" />
				</c:if>
				<!-- 功能選單區塊 -->
				<c:if test="${_menu != ''}">
					<tiles:insertAttribute name="menu" />
				</c:if>
			</nav>
		</c:if>
		<div id="page-wrapper">
           	<div id = "pagebody" class="container-fluid">
               <!--中間body區塊 - -->
               	<div id="page_body">
					<c:if test="${_body != ''}">
						<tiles:insertAttribute name="body" />
						
					</c:if>
				</div>
           	</div>
           	<!--處理訊息區塊-->
           	<span id="processingMessage"></span>
       	</div>
	</div>
	<div id="gotop">top</div>
    <!--頁尾區塊 -->
    <c:if test="${UI_MODE != 'APP'}">
		<c:if test="${_footer != ''}">
			<tiles:insertAttribute name="footer" />
		</c:if>
	</c:if>
	<div id="myModal" role="dialog" aria-hidden="true" data-backdrop="static" data-keyboard="false">
		
	</div>

<script type="text/javascript">
$(document).ready(function(){ 
     
});
$(function(){
    $("#gotop").click(function(){
        jQuery("html,body").animate({
            scrollTop:0
        },1000);
    });
    $(window).scroll(function() {
        if ( $(this).scrollTop() > 300){
            $('#gotop').fadeIn("fast");
        } else {
            $('#gotop').stop().fadeOut("fast");
        }
    });
});
$(window).load(function() {
	// 密碼狀態提示
	if ("${param.gonna_be_expired}" != "") { // 密碼即將過期
		$.messager.alert('<st:message code="law.login.sysInfoLabel" />', '<st:message code="law.login.credentialGonnaBeExpired" arguments="${param.gonna_be_expired}" />', 'warn');
	} else if ("${param.expired}" != "") { // 密碼已過期, 但在延展日內
		$.messager.alert('<st:message code="law.login.sysInfoLabel" />', '<st:message code="law.login.credentialExpiredButValid" arguments="${param.expired}" />', 'warn');
	}
});
</script>
</body>
</html>
