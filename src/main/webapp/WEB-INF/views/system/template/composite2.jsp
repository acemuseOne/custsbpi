<%@ page language="java" contentType="text/html; charset=UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="st" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="ss" uri="http://www.springframework.org/security/tags"%>
<%@ taglib prefix="tiles" uri="http://tiles.apache.org/tags-tiles"%>
<%@ taglib prefix="law" uri="http://lawweb.lawbroker.com.tw/taglib"%>

<%@ include file="include/_var.jsp"%>

<%-- 以下 DOCTYPE 宣告會使 easyui datagrid detail view 在 IE 8 上顯示不正常, 暫先移除
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01//EN" "http://www.w3.org/TR/html4/strict.dtd">
--%>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<link rel="shortcut icon" href="${pageContext.request.contextPath}/resources/images/company/icon_logo.ico?v=${_templateVer}"></link>
<link rel="icon" href="${pageContext.request.contextPath}/resources/images/company/icon_logo.ico?v=${_templateVer}"></link>
<link rel="bookmark" href="${pageContext.request.contextPath}/resources/images/company/icon_logo.ico?v=${_templateVer}"></link>
<title>${_functionId == "" ? _companyTitle : _functionId}-<tiles:getAsString name="title" /></title>

<%@ include file="include/_jquery.jsp"%>
<%@ include file="include/_easyui.jsp"%>

<script type="text/javascript" src="${_contextPath}/resources/js/json/json2.js?v=${_templateVer}"></script>
<script type="text/javascript" src="${_contextPath}/resources/js/common/jquery.cookie.js?v=${_templateVer}"></script>
<script type="text/javascript" src="${_contextPath}/resources/js/common/jquery.loadmask.min.js?v=${_templateVer}"></script>
<script type="text/javascript" src="${_contextPath}/resources/js/views/system/template/composite.js?v=${_templateVer}"></script>

<link rel="stylesheet" type="text/css" href="${_contextPath}/resources/css/composite.css?v=${_templateVer}">
<link rel="stylesheet" type="text/css" href="${_contextPath}/resources/css/icon.css?v=${_templateVer}">
<link rel="stylesheet" type="text/css" href="${_contextPath}/resources/css/loadmask/themes/${_theme}/jquery.loadmask.css?v=${_templateVer}">
</head>

<body style="padding: 0px">

	<!-- 供 composite.js 取得 context path -->
	<span id="_contextPath" style="display: none">${_contextPath}</span>

	<!-- 處理訊息 -->
	<span id="infoMessage" class="messageFrame topLeft"></span>
	<span id="warningMessage" class="messageFrame topLeft"></span>
	<span id="errorMessage" class="messageFrame topLeft"></span>
	<span id="processingMessage" class="messageFrame topLeft"></span>

	<!-- 第 0 層 Layout 為取得區塊最大寬度 -->
	<div id="layoutLevel0" class="easyui-layout" data-options="fit:true">
		<div id="panelComposite" data-options="region:'center',split:true,border:false" style="padding: 1px">
			<!-- 第一層 Layout -->
			<div id="layoutLevel1" class="easyui-layout" data-options="fit:true">
				<!-- 頁首區塊 -->
				<c:if test="${_header != ''}">
					<div id="panelHeader" data-options="region:'north',border:false,split:true,height:<tiles:getAsString name="headerHeight" />" style="padding: 0px">
						<tiles:insertAttribute name="header" />
					</div>
				</c:if>

				<!-- 頁尾區塊 -->
				<c:if test="${_footer != ''}">
					<div id="panelFooter" data-options="region:'south',border:false,split:false,height:<tiles:getAsString name="footerHeight" />" style="padding: 0px">
						<tiles:insertAttribute name="footer" />
					</div>
				</c:if>

				<!-- 功能選單區塊 -->
				<c:if test="${_menu != ''}">
					<div id="panelMenu" data-options="region:'west',iconCls:'icon-treeNodes',split:true,title:'<tiles:getAsString name="menuTitle" />',width:<tiles:getAsString name="menuWidth" />"
						style="padding: 5px">
						<tiles:insertAttribute name="menu" />
					</div>
				</c:if>

				<!-- 預覽區塊 -->
				<c:if test="${_preview != ''}">
					<div id="panelPreview"
						data-options="region:'east',iconCls:'icon-preview',split:true,title:'<tiles:getAsString name="previewTitle" />',collapsed:true,width:<tiles:getAsString name="previewWidth" />"
						style="padding: <tiles:getAsString name="previewPadding" />px">
						<tiles:insertAttribute name="preview" />
					</div>
				</c:if>

				<div data-options="region:'center',split:true,border:false">
					<!-- 第二層 Layout -->
					<div id="layoutLevel2" class="easyui-layout" data-options="fit:true">

						<!-- 組織層級區塊 -->
						<c:if test="${_orgMenuType != 'NONE'}">
							<div id="panelOrgMenu" data-options="region:'west',iconCls:'icon-employee',split:true,title:'${_orgMenuTitle}',width:<tiles:getAsString name="orgMenuWidth" />" style="padding: 5px">
								<tiles:insertAttribute name="orgMenu" />
							</div>
						</c:if>

						<div data-options="region:'center',split:true,border:false">
							<!-- 第三層 Layout -->
							<div id="layoutLevel3" class="easyui-layout" data-options="fit:true">

								<!-- 作業項目 / 查詢條件 / 查詢選項 -->
								<%@ include file="include/_subMenu.jsp"%>

								<!-- 右半部內容區塊 -->
								<div id="panelContent" data-options="region:'center',split:true,border:false">
									<c:choose>
										<c:when test="true">
											<%@ include file="include/_contentLayout.jsp"%>
										</c:when>
										<c:otherwise>
											<%@ include file="include/_etLayout.jsp"%>
										</c:otherwise>
									</c:choose>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>

	<script type="text/javascript">
	
		$(window).load(function() {
			// 密碼狀態提示
			if ("${param.gonna_be_expired}" != "") { // 密碼即將過期
				$.messager.alert('<st:message code="law.login.sysInfoLabel" />', '<st:message code="law.login.credentialGonnaBeExpired" arguments="${param.gonna_be_expired}" />', 'warn');
			} else if ("${param.expired}" != "") { // 密碼已過期, 但在延展日內
				$.messager.alert('<st:message code="law.login.sysInfoLabel" />', '<st:message code="law.login.credentialExpiredButValid" arguments="${param.expired}" />', 'warn');
			}
		});
	</script>
</body>
</html>
