<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="st" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="tiles" uri="http://tiles.apache.org/tags-tiles"%>

<%-- LawTemplate 版本 --%>
<st:eval var="_templateVer" expression="@templatePropertyConfigurer.getProperty('law.template.version')" scope="request" />

<c:set var="_theme">
	<tiles:getAsString name="theme" />
</c:set>

<c:if test="${_theme == ''}">
	<c:set var="_theme" value="default" />
</c:if>

<c:set var="_footer">
	<tiles:getAsString name="footer" />
</c:set>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01//EN" "http://www.w3.org/TR/html4/strict.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF8">
<link rel="shortcut icon" href="${pageContext.request.contextPath}/resources/images/company/icon_logo.ico?v=${_templateVer}"></link>
<link rel="icon" href="${pageContext.request.contextPath}/resources/images/company/icon_logo.ico?v=${_templateVer}"></link>
<link rel="bookmark" href="${pageContext.request.contextPath}/resources/images/company/icon_logo.ico?v=${_templateVer}"></link>
<title><st:message code='law.composite.companyTitle' />-<tiles:getAsString name="title" /></title>

<%@ include file="include/_jquery.jsp"%>
<%@ include file="include/_easyui.jsp"%>

<script type="text/javascript" src="${pageContext.request.contextPath}/resources/js/json/json2.js?v=${_templateVer}"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/resources/js/common/jquery.loadmask.min.js?v=${_templateVer}"></script>

<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/resources/css/loadmask/themes/${_theme}/jquery.loadmask.css?v=${_templateVer}">
<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/resources/css/icon.css?v=${_templateVer}">
</head>
<body>
	<div class="easyui-layout" data-options="fit:true">
		<div data-options="region:'center',border:true">
			<tiles:insertAttribute name="body" />

			<c:if test="${_footer != ''}">
				<tiles:insertAttribute name="footer" />
			</c:if>
		</div>
	</div>
</body>
</html>