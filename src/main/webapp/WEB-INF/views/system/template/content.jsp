<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="tiles" uri="http://tiles.apache.org/tags-tiles"%>

<c:set var="_contextPath" scope="request">
	${pageContext.request.contextPath}
</c:set>

<script type="text/javascript">
	$(document).ready(function() {
		//設定 layout panel 縮合時的 Title
		//_setPanelCollapseTitle("#panelContent"); // 設定縮合後顯示 west/east/north panel 的 title	
	});
</script>

<%@ include file="include/_contentLayout.jsp"%>
