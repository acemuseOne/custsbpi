<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="tiles" uri="http://tiles.apache.org/tags-tiles"%>

<c:set var="_information">
	<tiles:getAsString name="information" />
</c:set>

<c:set var="_button">
	<tiles:getAsString name="button" />
</c:set>

<c:set var="_form">
	<tiles:getAsString name="form" />
</c:set>


<c:set var="_body">
	<tiles:getAsString name="body" />
</c:set>

<c:set var="_description">
	<tiles:getAsString name="description" />
</c:set>

<div id="panelBody">
	<tiles:insertAttribute name="body" />
</div>
		