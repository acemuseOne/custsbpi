<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="tiles" uri="http://tiles.apache.org/tags-tiles"%>

<c:set var="_information">
	<tiles:getAsString name="information" />
</c:set>

<c:set var="_button">
	<tiles:getAsString name="button" />
</c:set>

<c:set var="_form">
	<tiles:getAsString name="form" />
</c:set>

<c:set var="_body">
	<tiles:getAsString name="body" />
</c:set>

<c:set var="_description">
	<tiles:getAsString name="description" />
</c:set>

<div id="layoutLevel4" class="easyui-layout" data-options="fit:true">

	<!-- 訊息區塊 -->
	<c:if test="${_information != ''}">
		<div id="panelInformation" data-options="region:'north',split:true,border:<tiles:getAsString name="informationBorder" />,title:'<tiles:getAsString name="informationTitle" />'"
			style="height: <tiles:getAsString name="informationHeight" />px; padding: <tiles:getAsString name="informationPadding" />px">
			<tiles:insertAttribute name="information" />
		</div>
	</c:if>

	<div data-options="region:'center',split:true,border:false">
		<!-- 第五層 Layout -->
		<div id="layoutLevel5" class="easyui-layout" data-options="fit:true">

			<!-- 按鈕區塊 -->
			<c:if test="${_button != ''}">
				<div id="panelButton" data-options="region:'north',split:true,border:true,height:36" style="padding: 0px">
					<tiles:insertAttribute name="button" />
				</div>
			</c:if>

			<div data-options="region:'center',split:true,border:false">

				<!-- 第六層 Layout -->
				<div id="layoutLevel6" class="easyui-layout" data-options="fit:true">

					<!-- 編輯區塊 -->
					<c:if test="${_form != ''}">
						<div id="_form" height="<tiles:getAsString name="formHeight" />" title="<tiles:getAsString name="formTitle" />" style="display: none">
							<tiles:insertAttribute name="form" />
						</div>
					</c:if>

					<!-- 主頁區塊 -->
					<div id="panelBody" data-options="region:'center',split:true,border:<tiles:getAsString name="bodyBorder" />,title:'<tiles:getAsString name="bodyTitle" />'"
						style="padding: <tiles:getAsString name="bodyPadding" />px">
						<tiles:insertAttribute name="body" />
					</div>
				</div>
			</div>
		</div>
	</div>

	<!-- 說明區塊 -->
	<c:if test="${_description != ''}">
		<div id="panelDescription"
			data-options="region:'south',split:true,border:<tiles:getAsString name="descriptionBorder" />,title:'<tiles:getAsString name="descriptionTitle" />',height:<tiles:getAsString name="descriptionHeight" />"
			style="padding: <tiles:getAsString name="descriptionPadding" />px">
			<tiles:insertAttribute name="description" />
		</div>
	</c:if>
</div>
