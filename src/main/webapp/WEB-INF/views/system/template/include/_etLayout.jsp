<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="tiles" uri="http://tiles.apache.org/tags-tiles"%>

<script type="text/javascript" src="${_contextPath}/resources/js/common/jquery.form.min.js"></script>

<div id="layoutLevel4" class="easyui-layout" data-options="fit:true">
	<div data-options="region:'center',split:true,border:false">

		<!-- 第五層 Layout -->
		<div id="layoutLevel5" class="easyui-layout" data-options="fit:true">
			<div id="panelButton" data-options="region:'north',split:true,border:true,height:36" style="padding: 0px">
				<tiles:insertAttribute name="button" />
			</div>

			<div data-options="region:'center',split:true,border:false">

				<!-- 第六層 Layout -->
				<div id="layoutLevel6" class="easyui-layout" data-options="fit:true">

					<!-- 上傳區塊 -->
					<div data-options="region:'north',split:true,border:true,title:'上傳資料',height:<tiles:getAsString name="formHeight" />" style="padding: 5px">
						<form id="_etUpload" method="POST" enctype="multipart/form-data">
							Program No: <input id="progNo" type="text" name="_etArgs" value="2b10"> Corp No: <input type="text" name="_etArgs" value="28"> <input type="radio" name="_etArgs" value="A"
								checked>New <input type="radio" name="_etArgs" value="B">Old <input type="file" name="fileUpload" multiple>
						</form>
					</div>

					<!-- 主頁區塊 -->
					<div id="panelBody" data-options="region:'center',split:true,border:false" style="padding-top: 0px">

						<div class="easyui-layout" data-options="fit:true">
							<div data-options="region:'north',split:true,border:true,height:100">
								<!-- 表格 -->
								<table id="_transStatus" class="easyui-datagrid" data-options="title:'轉換狀態',border:false,fit:true,rownumbers:true,fitColumns:false,url:'etTransLog'">
									<thead>
										<tr>
											<th field="transFilename" align="center">轉換檔名</th>
											<th field="extSuccessCnt" align="center">轉換成功筆數</th>
											<th field="extFailCnt">轉換失敗筆數</th>

											<th field="exeSuccessCnt" align="center">檢查成功筆數</th>
											<th field="exeFailCnt">檢查失敗筆數</th>

											<th field="insertCnt" align="center">已匯入</th>
											<th field="uninsertCnt">未匯入</th>

										</tr>
									</thead>
								</table>
							</div>

							<div data-options="region:'center',split:true,border:true,title:'轉換結果'" style="padding-top: 3px">
								<div id="queryTabs" class="easyui-tabs" data-options="fit:true,border:false,plain:true">
									<div id="_logTab" title="轉換記錄" style="padding: 5px" data-options="iconCls:'icon-log'">
										<div class="easyui-layout" data-options="fit:true">
											<div data-options="region:'north',split:true,title:'轉換失敗資料',height:150" style="padding: 3px">
												<pre id="_badText"></pre>
											</div>
											<div data-options="region:'center',split:true,title:'轉換記錄'" style="padding: 2px">
												<pre id="_logText"></pre>
											</div>

										</div>
									</div>

									<div id="_failTab" title="檢核失敗" style="padding: 5px" data-options="iconCls:'icon-sad'">
										<tiles:insertAttribute name="body" />
									</div>

									<div id="_okTab" title="檢核成功" style="padding: 5px" data-options="iconCls:'icon-smile'">
										<tiles:insertAttribute name="body" />
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>

	</div>
</div>

<script type="text/javascript">
	$(document).ready(function() {
		// 送出鈕
		_addSubmitButton(function() {
			_testConnection();
			_maskContent(true);

			$('#_etUpload').ajaxSubmit({
				url : "etImport",

				dataType : "json",

				success : function(data) {
					var transTime = data.OUT_TRANSTIME;
					var logFile = data.OUT_LOGFILE;
					var badFile = data.OUT_BADFILE;
					var bad = $.trim(data.BAD);
					var log = $.trim(data.LOG);

					// 轉換記錄
					$("#_badText").html(bad);
					$("#_logText").html(log);

					// 轉換狀態
					$("#_transStatus").datagrid({
						url : "etTransLog",

						onBeforeLoad : function(param) {
							param.transTime = transTime;
						}
					});

					_enableTransferButton(false);
				},

				error : function() {
					_enableVerifyButton(false);
				},

				complete : function() {
					_maskContent(false);
				}
			});
		}, true, "送出", {
			enabled : [ "Verify" ]
		});

		// 檢核資料
		_addVerifyButton(function() {
		}, false, "檢核資料");
	});
</script>