<%@ page language="java" contentType="text/html; charset=UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="st" uri="http://www.springframework.org/tags"%>

<%--

	功用: 載入 app.properties 中設定的 easyui 版本

--%>

<c:set var="_jqueryVer">
	<st:eval expression="@templatePropertyConfigurer.getProperty('jquery.version')" />
</c:set>

<script type="text/javascript" src="${pageContext.request.contextPath}/resources/js/jquery-${_jqueryVer}.min.js"></script>