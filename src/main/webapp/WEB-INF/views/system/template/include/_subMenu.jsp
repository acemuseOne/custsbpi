<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="tiles" uri="http://tiles.apache.org/tags-tiles"%>
<%@ taglib prefix="st" uri="http://www.springframework.org/tags"%>

<c:choose>
	<%-- 作業項目 --%>
	<c:when test="${_subMenu != '' && _subMenuQuery == '' && _subMenuResult == ''}">
		<div id="panelSubMenu" data-options="region:'west',iconCls:'icon-todoList',split:true,border:true,width:<tiles:getAsString name="subMenuWidth" />" title="${_subMenuTitle}"
			style="padding:<tiles:getAsString name="subMenuPadding" />px">
			<tiles:insertAttribute name="subMenu" />
		</div>
	</c:when>

	<%-- 查詢條件 --%>
	<c:when test="${_subMenu == '' && _subMenuQuery != '' && _subMenuResult == ''}">
		<div id="panelSubMenu" data-options="region:'west',iconCls:'icon-search',split:true,border:true,width:<tiles:getAsString name="subMenuWidth" />" title="${_subMenuQueryTitle}"
			style="padding:<tiles:getAsString name="subMenuQueryPadding" />px">
			<tiles:insertAttribute name="subMenuQuery" />
		</div>
	</c:when>

	<%-- 作業項目 + 查詢條件 --%>
	<c:when test="${_subMenu != '' && _subMenuQuery != '' && _subMenuResult == ''}">
		<div id="panelSubMenu" data-options="region:'west',headerCls:'subMenuHeader',iconCls:'icon-todoList',split:true,border:false,width:<tiles:getAsString name="subMenuWidth" />" title="${_subMenuTitle}">

			<div class="easyui-layout" data-options="fit:true">
				<div id="panelSubMenuInner" data-options="region:'north',split:true,border:true,height:<tiles:getAsString name="subMenuHeight" />" style="padding:<tiles:getAsString name="subMenuPadding" />px">
					<tiles:insertAttribute name="subMenu" />
				</div>

				<div id="panelSubMenuQuery" data-options="region:'center',split:true,border:true,iconCls:'icon-search'" title="${_subMenuQueryTitle}"
					style="padding: <tiles:getAsString name="subMenuQueryPadding" />px">
					<tiles:insertAttribute name="subMenuQuery" />
				</div>
			</div>
		</div>
	</c:when>

	<%-- 查詢條件 + 查詢選項 --%>
	<c:when test="${_subMenu == '' && _subMenuQuery != '' && _subMenuResult != ''}">
		<div id="panelSubMenu" data-options="region:'west',headerCls:'subMenuHeader',iconCls:'icon-search',split:true,border:false,width:<tiles:getAsString name="subMenuQueryWidth" />"
			title="${_subMenuQueryTitle}">

			<div class="easyui-layout" data-options="fit:true">
				<div id="panelSubMenuQuery" data-options="region:'north',split:true,border:true,height:<tiles:getAsString name="subMenuQueryHeight" />"
					style="padding:<tiles:getAsString name="subMenuQueryPadding" />px">
					<tiles:insertAttribute name="subMenuQuery" />
				</div>

				<div id="panelSubMenuResult" data-options="region:'center',split:true,border:true,iconCls:'icon-orderedList'" title="${_subMenuResultTitle}"
					style="padding: <tiles:getAsString name="subMenuResultPadding" />px">
					<tiles:insertAttribute name="subMenuResult" />
				</div>
			</div>
		</div>
	</c:when>

	<%-- 作業項目 + 查詢條件 + 查詢選項 --%>
	<c:when test="${_subMenu != '' && _subMenuQuery != '' && _subMenuResult != ''}">
		<div id="panelSubMenu" data-options="region:'west',headerCls:'subMenuHeader',iconCls:'icon-todoList',split:true,border:false,width:<tiles:getAsString name="subMenuWidth" />" title="${_subMenuTitle}">

			<div class="easyui-layout" data-options="fit:true">
				<div id="panelSubMenuInner" data-options="region:'north',split:true,border:true,height:<tiles:getAsString name="subMenuHeight" />" style="padding:<tiles:getAsString name="subMenuPadding" />px">
					<tiles:insertAttribute name="subMenu" />
				</div>

				<div data-options="region:'center',split:true,border:false">
					<div class="easyui-layout" data-options="fit:true">
						<div id="panelSubMenuQuery" data-options="region:'north',split:true,border:true,iconCls:'icon-search',height:<tiles:getAsString name="subMenuQueryHeight" />" title="${_subMenuQueryTitle}"
							style="padding:<tiles:getAsString name="subMenuQueryPadding" />px">
							<tiles:insertAttribute name="subMenuQuery" />
						</div>

						<div id="panelSubMenuResult" data-options="region:'center',split:true,border:true,iconCls:'icon-orderedList'" title="${_subMenuResultTitle}"
							style="padding: <tiles:getAsString name="subMenuResultPadding" />px">
							<tiles:insertAttribute name="subMenuResult" />
						</div>
					</div>
				</div>
			</div>
		</div>
	</c:when>
</c:choose>
