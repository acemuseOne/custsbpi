<%@ page language="java" contentType="text/html; charset=UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="st" uri="http://www.springframework.org/tags"%>

<%--

	功用: 載入 app.properties 中設定的 jquery 版本
	注意: ${theme} 值須先自行設定, 未設定則自動使用 "default"
	
--%>

<st:eval var="_easyuiVer" expression="@templatePropertyConfigurer.getProperty('easyui.version')" />

<c:if test="${_theme == ''}">
	<c:set var="_theme" value="default" />
</c:if>

<script type="text/javascript" src="${pageContext.request.contextPath}/resources/bundle/jquery-easyui-${_easyuiVer}/jquery.easyui.min.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/resources/bundle/jquery-easyui-${_easyuiVer}/locale/easyui-lang-${pageContext.request.locale}.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/resources/bundle/jquery-easyui-${_easyuiVer}/easyloader.js"></script>

<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/resources/bundle/jquery-easyui-${_easyuiVer}/themes/${_theme}/easyui.css">
<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/resources/bundle/jquery-easyui-${_easyuiVer}/themes/icon.css">