<%@ page language="java" contentType="text/html; charset=UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="st" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="tiles" uri="http://tiles.apache.org/tags-tiles"%>
<%@ taglib prefix="law" uri="http://lawweb.lawbroker.com.tw/taglib"%>

<%--

	功用: composite.jsp 使用變數 
	
 --%>

<%-- 將功能代碼存到變數 functionId (必須指定為 request scope 才可在同一頁面的其它 JSP 取用) --%>
<c:set var="_functionId" scope="request">
	<tiles:getAsString name="functionId" />
</c:set>

<c:if test="${_functionId== ''}">
	<%-- 外部連結內嵌的網頁(未設定functionId, 由網址列參數取得) --%>
	<c:set var="_functionId">
		${param.funId}
	</c:set>
</c:if>

<%-- 將解析功能代碼後的結果存到 _thisFunction (必須指定為 request scope 才可在同一頁面的其它 JSP 取用) --%>
<law:hierarchy var="_thisFunction" functionId="${_functionId}" lang="${pageContext.request.locale}" scope="request" />

<%-- LawTemplate 版本 --%>
<st:eval var="_templateVer" expression="@templatePropertyConfigurer.getProperty('law.template.version')" scope="request" />

<%-- 其它變數 --%>
<c:set var="_contextPath" scope="request">
	${pageContext.request.contextPath}
</c:set>

<c:set var="_theme">
	<tiles:getAsString name="theme" />
</c:set>

<c:if test="${_theme == ''}">
	<c:set var="_theme" value="default" />
</c:if>

<st:message var="_companyTitle" code="law.composite.companyTitle" />

<c:set var="_header">
	<tiles:getAsString name="header" />
</c:set>

<c:set var="_footer">
	<tiles:getAsString name="footer" />
</c:set>

<c:set var="_menu">
	<tiles:getAsString name="menu" />
</c:set>

<c:set var="_orgMenu">
	<tiles:getAsString name="orgMenu" />
</c:set>

<c:set var="_orgMenuType" scope="request">
	<c:choose>
		<c:when test="${_orgMenu == ''}">
			NONE
		</c:when>
		<c:otherwise>
			<tiles:getAsString name="orgMenuType" />
		</c:otherwise>
	</c:choose>
</c:set>

<c:set var="_orgMenuTitle">
	<c:choose>
		<c:when test="${_orgMenuType == 'ORG'}">
			<st:message code="law.orgmenu.orgTitle" />
		</c:when>
		<c:when test="${_orgMenuType == 'REF'}">
			<st:message code="law.orgmenu.refTitle" />
		</c:when>
		<c:otherwise>
			orgMenuType not defined
		</c:otherwise>
	</c:choose>
</c:set>

<st:message var="_subMenuTitle" code="law.submenu.title" />
<st:message var="_subMenuQueryTitle" code="law.submenu.queryTitle" />
<st:message var="_subMenuResultTitle" code="law.submenu.resultTitle" />

<c:set var="_subMenu">
	<tiles:getAsString name="subMenu" />
</c:set>

<c:set var="_subMenuQuery">
	<tiles:getAsString name="subMenuQuery" />
</c:set>

<c:set var="_subMenuResult">
	<tiles:getAsString name="subMenuResult" />
</c:set>

<c:set var="_preview">
	<tiles:getAsString name="preview" />
</c:set>

<c:set var="_bodyTitle">
	<tiles:getAsString name="bodyTitle" />
</c:set>

<c:set var="_title">
	<tiles:getAsString name="title" />
</c:set>

