<%@ page language="java" contentType="text/html; charset=UTF8" pageEncoding="UTF8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="st" uri="http://www.springframework.org/tags"%>

<style type="text/css">
.message {
	color: red;
	font-size: 30px;
	text-align: center;
	margin: 30px auto;
}

.exitButton {
	position: absolute;
	left: 50%;
	top: 30%;
	margin-top: -10px;
	margin-left: -20px;
}

.debug {
	padding: 5px;
}

.stackTrace {
	color: gray;
	font-size: 13px;
}

.cause {
	color: red;
}

.causeMsg {
	font-weight: bold;
}

.exClass {
	color: red;
}

.exMsg {
	font-weight: bold;
}

.stElement {
	display: block;
	margin-left: 30px;
}

.lawClass {
	color: blue;
}

.exFileName {
	color: green;
}

.exLineNo {
	color: brown;
}
</style>

<div id="a" class="easyui-layout" data-options="fit:true">
	<div id="b" data-options="region:'center',border:false">
		<div class="easyui-accordion" data-options="fit:true">
			<div title="Error message">
				<div class="message">${ex.message}</div>
			</div>

			<div title="Debug information" class="debug">
				<div>${ex.stackTraceAsHtml}</div>
			</div>
		</div>
	</div>
</div>