<%@ page language="java" contentType="text/html; charset=UTF8" pageEncoding="UTF8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="s" uri="http://www.springframework.org/tags"%>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title></title>
</head>
<body style="padding:0px">
	<div style="color: red; font-size: 24px; padding: 5px">
		<c:choose>
			<c:when test="${msg} != ''">
				${msg}
			</c:when>
			<c:otherwise>
				${ex.message}
			</c:otherwise>
		</c:choose>
	</div>
</body>
</html>