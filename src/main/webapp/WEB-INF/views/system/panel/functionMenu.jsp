<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF8"%>
<%@ taglib prefix="law" uri="http://lawweb.lawbroker.com.tw/taglib"%>
<%@ taglib prefix="st" uri="http://www.springframework.org/tags"%>
<!-- 功能選單 UI -->
<!-- <ul id="_functionMenuTree"></ul> -->

<!-- 以下為 CODE -->

<%-- 儲存 app.properties law.module 的設定值到 _module --%>
<st:eval var="_module" expression="@propertyConfigurer.getProperty('law.module')" />

<%-- 取得 menu JSON string --%>
<law:functionMenuAsJSON var="_functionMenu" module="${_module}" lang="${pageContext.request.locale}" />
          <div class="collapse navbar-collapse navbar-ex1-collapse">
			<!-- 調整旁邊選單大小 sb-admin.css >>.side-nav -->
			  <!-- 字體顏色 sb-admin.css >>.navbar-inverse .navbar-nav>.active>a .navbar-inverse .navbar-nav>li>a -->
                <ul class="nav navbar-nav side-nav">
                   <div id="_functionMenuTree"></div>
                </ul>
            </div>
<!-- 功能選單初始 -->
<script type="text/javascript">
 	$(document).ready(function() {
 	 	$('#_functionMenuTree').treeview({
    		expandIcon: 'fa fa-folder-o',
    		collapseIcon: 'fa fa-folder-open-o',
    		showTags: true,
    		levels:3,
    		backColor: 'firebrick', 
    		borderColor:'firebrick',
    		enableLinks:true,
    		emptyIcon:'fa fa-file-text-o',
    		data: JSON.parse('${_functionMenu}'),
    		onNodeSelected: function(event, node) {
      		  debugger;
				if (node.id != "${_thisFunction.uuid}") { // 如果點選的功能選單節點不是目前頁面的 functionId, 才執行跳頁
				var hierarchy = _getTreeNodeHierarchy("_functionMenuTree", node, functionMenuItemFormatter);
				_triggerEvent("menu", "menu:changeMenuItem", hierarchy);
				var url = node.attributes.url;
				  debugger;
				if (url) {
					if (url.substring(0, 4) == "http") { // 外部網頁連結
						var target = node.attributes.target;
						 debugger;
						switch (target) {
						case "NEW":
							window.open(url);
							break;
						case "BODY":
							location.href = "${_contextPath}/inlinePage?id=" + node.id + "&funId=" + node.attributes.functionId + "&url=" + url;
							break;
						default:
							location.href = url;
						}
					} else { // 內部網頁連結
						location.href = "${_contextPath}" + url;
						
					}
				}
			} else { // 觸發 menu:select 事件 (header.jsp 接收以顯示"目前位置"階層路徑)
// 				var hierarchy = _getTreeNodeHierarchy("_functionMenuTree", node, functionMenuItemFormatter);
// 				_triggerEvent("menu", "menu:changeMenuItem", hierarchy);
				var url = node.attributes.url;
				location.href = "${_contextPath}" + url;
			}
    		},
    		onSearchComplete: function(event, data) {
    		  debugger;
  			}	
    	});
// 		$("#_functionMenuTree").tree({
// 			data : JSON.parse('${_functionMenu}'), // 一定要用 ' 單引號, 因為 _functionMenu 裡使用 " 雙引號
// 			lines : true,
// 			formatter : functionMenuItemFormatter,

// 			onBeforeSelect : function(node) {
// 				// 非功能節點, 則取消選擇動作
// 				if (node.attributes.level < 2)
// 					return false;
// 			},

// 			// 點選時開啟連結
// 			onSelect : function(node) {
				
// 				if (node.id != "${_thisFunction.uuid}") { // 如果點選的功能選單節點不是目前頁面的 functionId, 才執行跳頁
// 					var url = node.attributes.url;

// 					if (url) {
// 						if (url.substring(0, 4) == "http") { // 外部網頁連結
// 							var target = node.attributes.target;

// 							switch (target) {
// 							case "NEW":
// 								window.open(url);
// 								break;
// 							case "BODY":
// 								location.href = "${_contextPath}/inlinePage?id=" + node.id + "&funId=" + node.attributes.functionId + "&url=" + url;
// 								break;
// 							default:
// 								location.href = url;
// 							}
// 						} else { // 內部網頁連結
// 							location.href = "${_contextPath}" + url;
// 						}
// 					}
// 				} else { // 觸發 menu:select 事件 (header.jsp 接收以顯示"目前位置"階層路徑)
// 					var hierarchy = _getTreeNodeHierarchy("_functionMenuTree", node, functionMenuItemFormatter);
// 					_triggerEvent("menu", "menu:changeMenuItem", hierarchy);
// 				}
// 			},

// 			onLoadSuccess : function() {
// 				_showTooltipForTreeNodeWhenExceedingContainer("panelMenu", "_functionMenuTree");
// 			}
// 		});

// 		// 加入"全部展開" tool bar
// 		_addMenuTool("icon-expandAll", function() {
// 			$("#_functionMenuTree").tree("expandAll");
// 		});

// 		// 加入"全部縮合" tool bar
// 		_addMenuTool("icon-collapseAll", function() {
// 			$("#_functionMenuTree").tree("collapseAll");
// 		});

// 		// 根據目前頁面的 functionId 點選功能選單上的節點 (目的是讓目前功能選項 highlight)
// 		var thisFunctionNode = $("#_functionMenuTree").tree("find", "${_thisFunction.uuid}");

// 		if (thisFunctionNode)
// 			$("#_functionMenuTree").tree("select", thisFunctionNode.target);
 	});

	// 功能選項的格式化函式
	function functionMenuItemFormatter(node) {
		var nodeText = node.attributes.code + "-" + node.attributes.name;

		if (node.attributes.level < 2)
			nodeText = "<span style='font-weight:bolder'>" + nodeText + "</span>";

		return nodeText;
	}
</script>