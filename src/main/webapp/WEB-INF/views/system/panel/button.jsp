<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="st" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="ss" uri="http://www.springframework.org/security/tags"%>

<script type="text/javascript" src="${_contextPath}/resources/js/views/system/template/button.js?v=${_templateVer}"></script>
<script type="text/javascript" src="${_contextPath}/resources/js/views/system/template/menu-button.js?v=${_templateVer}"></script>

<!-- button 列 -->
<div id="_toolbar" class="datagrid-toolbar"></div>

<!-- button 貯存槽 -->
<div id="_buttonRepository" style="display: none">
	<!-- 查詢 -->
	<a href="#" id="_btnQuery" class="easyui-linkbutton" data-options="iconCls:'icon-search',plain:true"><st:message code="law.button.query" /></a>
	<!-- 編輯 -->
	<a href="#" id="_btnEdit" class="easyui-linkbutton" data-options="iconCls:'icon-edit',plain:true"><st:message code="law.button.edit" /></a>
	<!-- 清空 -->
	<a href="#" id="_btnClear" class="easyui-linkbutton" data-options="iconCls:'icon-clear',plain:true"><st:message code="law.button.clear" /></a>
	<!-- 儲存 -->
	<a href="#" id="_btnSave" class="easyui-linkbutton" data-options="iconCls:'icon-save',plain:true"><st:message code="law.button.save" /></a>
	<!-- 儲存離開 -->
	<a href="#" id="_btnSaveExit" class="easyui-linkbutton" data-options="iconCls:'icon-saveExit',plain:true"><st:message code="law.button.saveExit" /></a>
	<!-- 新增 -->
	<a href="#" id="_btnNew" class="easyui-linkbutton" data-options="iconCls:'icon-add',plain:true"><st:message code="law.button.new" /></a>
	<!-- 刪除 -->
	<a href="#" id="_btnDelete" class="easyui-linkbutton" data-options="iconCls:'icon-delete',plain:true"><st:message code="law.button.delete" /></a>
	<!-- 匯入 -->
	<a href="#" id="_btnImport" class="easyui-linkbutton" data-options="iconCls:'icon-import',plain:true"><st:message code="law.button.import" /></a>
	<!-- 匯出 -->
	<a href="#" id="_btnExport" class="easyui-linkbutton" data-options="iconCls:'icon-export',plain:true"><st:message code="law.button.export" /></a>
	<!-- 列印 -->
	<a href="#" id="_btnPrint" class="easyui-linkbutton" data-options="iconCls:'icon-print',plain:true"><st:message code="law.button.print" /></a>
	<!-- 作廢 -->
	<a href="#" id="_btnDiscard" class="easyui-linkbutton" data-options="iconCls:'icon-discard',plain:true"><st:message code="law.button.discard" /></a>
	<!-- 執行 -->
	<a href="#" id="_btnExecute" class="easyui-linkbutton" data-options="iconCls:'icon-execute',plain:true"><st:message code="law.button.execute" /></a>
	<!-- 拋轉 -->
	<a href="#" id="_btnTransfer" class="easyui-linkbutton" data-options="iconCls:'icon-transfer',plain:true"><st:message code="law.button.transfer" /></a>
	<!-- 離開 -->
	<a href="#" id="_btnExit" class="easyui-linkbutton" data-options="iconCls:'icon-exit',plain:true"><st:message code="law.button.exit" /></a>
	<!-- 取消 -->
	<a href="#" id="_btnCancel" class="easyui-linkbutton" data-options="iconCls:'icon-cancel',plain:true"><st:message code="law.button.cancel" /></a>
	<!-- 更新 -->
	<a href="#" id="_btnRefresh" class="easyui-linkbutton" data-options="iconCls:'icon-refresh',plain:true"><st:message code="law.button.refresh" /></a>
	<!-- 回復 -->
	<a href="#" id="_btnUndo" class="easyui-linkbutton" data-options="iconCls:'icon-undo',plain:true"><st:message code="law.button.undo" /></a>
	<!-- 檢核 -->
	<a href="#" id="_btnVerify" class="easyui-linkbutton" data-options="iconCls:'icon-verify',plain:true"><st:message code="law.button.verify" /></a>
	<!-- 送出 -->
	<a href="#" id="_btnSubmit" class="easyui-linkbutton" data-options="iconCls:'icon-submit',plain:true"><st:message code="law.button.submit" /></a>
</div>