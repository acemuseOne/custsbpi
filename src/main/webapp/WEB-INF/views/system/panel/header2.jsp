<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib prefix="s" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="st" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="ss" uri="http://www.springframework.org/security/tags"%>

<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/resources/css/header.css">

<table id="_headerTB" style="width: 100%; border: 0px; border-collapse: collapse; font-size: 12px;">
	<tr>
		<!-- Logo 圖片 -->
		<td style="width: 260px"><a href="${_contextPath}"><img src="${_contextPath}/resources/images/company/logo.gif" style="border: 0px; position: absolute; left: 2px; top: 0px" /></a></td>

		<!-- 目前位置 -->
		<td style="vertical-align: bottom; padding-bottom: 2px; padding-right: 10px;">
			<div style="width: 100%; text-align: left; overflow: hidden; white-space: break-word;">
				<span class="icon-flag" style="display: inline-block; height: 16px; width: 16px;"></span> <span style="font-size: 12px"> <span style="font-weight: bold"><st:message
							code="law.header.functionHierarchy" /></span>: LawERP <!-- 功能選項階層 --> <span id="_menuItemHierarchy" listenTo="menu"></span> <!-- 查詢選項階層 --> <span id="_subMenuItemHierarchy" listenTo="subMenu"
					style="color: blue"></span>
				</span>
			</div>
		</td>

		<!-- 登入資訊 -->
		<td style="width: 200px;">
			<table style="font-size: 12px; width: 100%; border-collapse: collapse; border: 0px">
				<tr>
					<!-- 登入者 -->
					<td alt="<st:message code="law.header.loginEno" />" class="icon-rank${_user.RANK}${_user.ESEX == '1' ? 'm':'f'} _itemIcon"><span>${_user.ENO} <b>${_user.ENAME}</b></span></td>

					<!-- 登出系統 -->
					<td class="icon-exit _itemIcon"><a style="color: blue; text-decoration: none"
						href="${_contextPath}/${initParam['spring.profiles.active'] == 'cas' ? 'j_spring_cas_security_logout' : 'static/j_spring_security_logout'}"><st:message code="law.header.logout" /></a></td>
				</tr>
				<tr>
					<!-- 登入時間 -->
					<td alt="<st:message code="law.header.loginTime" />" class="icon-clock _itemIcon"><fmt:formatDate pattern="yyyy-MM-dd HH:mm:ss" value="${_user.LOGIN_TIME}" /></td>

					<!-- 版本資訊 -->
					<td class="icon-info _itemIcon"><span id="_versionInfo"><st:message code="law.header.versionInfo" /></span></td>
				</tr>
			</table>
		</td>
	</tr>
</table>

<div style="display: none">
	<!-- 版本資訊內容 -->
	<div id="_versionInfoContent">
		<table id="_versionInfoTable" style="width: 190px; border:; font-size: 12px;">
			<tr>
				<td style="width: 60px; text-align: right;"><b><st:eval expression="@propertyConfigurer.getProperty('law.module')" /></b>：</td>
				<td><st:eval expression="@propertyConfigurer.getProperty('law.app.version')" /></td>
			</tr>
			<tr>
				<td style="text-align: right;"><b>LawTemplate</b>：</td>
				<td><st:eval expression="@templatePropertyConfigurer.getProperty('law.template.version')" /> (<st:eval expression="@templatePropertyConfigurer.getProperty('law.template.releaseDate')" />)</td>
			</tr>
		</table>
	</div>
</div>

<script type="text/javascript">
	$(document).ready(function() {
		$("#_headerTB").find("td[alt]").tooltip({
			position : "left",
			
			content : function() {
				return $(this).attr("alt");
			},
			
			onShow : function() {
				$(this).tooltip("tip").css("top", $(this).offset().top - 2);
			}
		});

		// 版本資訊 Tooltip
		$("#_versionInfo").tooltip({
			content : $("#_versionInfoContent"),
			onShow : function() {
				$(this).tooltip('arrow').css("left", 186);
				$(this).tooltip("tip").css("left", $(this).offset().left - 165).css("top", $(this).offset().top + 23);
			}
		});

		// 接收切換查詢選項的事件, 並將選項路徑顯示於功能選項路徑後
		$("#_subMenuItemHierarchy").on("subMenu:changeMenuItem", function(event, data) {
			var hierarchy = "";

			for (var i = 0; i < data.length; i++) {
				var html = $("<div>" + data[i] + "</div>").text(); // 將任何 html tag 的部份移除, 只取 text

				if (i == data.length - 1)
					html = "<span style='font-size:larger;font-weight:bold'>" + html + "</span>";

				hierarchy += " / " + html;
			}

			$(this).html(hierarchy);
		});

		// 接收切換功能選項的事件, 並將選項路徑顯示於目前位置
		$("#_menuItemHierarchy").on("menu:changeMenuItem", function(event, data) {
			var hierarchy = "";

			for (var i = 0; i < data.length; i++) {
				hierarchy += " / " + $("<div>" + data[i] + "</div>").text(); // 將任何 html tag 的部份移除, 只取 text
			}

			$(this).html(hierarchy);
		});
	});
</script>
