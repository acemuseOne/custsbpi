<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF8"%>

<iframe src="${param.url}" frameborder="0" style="width: 100%; height: 100%; border: 0px" seamless></iframe>

<script type="text/javascript">
	$(window).load(function() {
		// 設定網頁 title
		var node = $("#_functionMenuTree").tree("find", "${param.id}");

		if (node) {
			document.title = node.attributes.functionId + "-" + node.attributes.name;
		}
	});
</script>