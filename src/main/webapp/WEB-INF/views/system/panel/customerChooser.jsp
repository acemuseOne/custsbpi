<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="st" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="ss" uri="http://www.springframework.org/security/tags"%>

<script type="text/javascript" src="${pageContext.request.contextPath}/resources/js/common/util/datetime/DateTimeUtil.js"></script>

<style>
.lastName, .strokes {
	display: block;
	border: 1px solid #BFBFC6;
	float: left;
	margin: 1px;
	line-height: 30px;
	text-align: center;
	vertical-align: middle;
	font-size: 16px;
	font-weight: bold;
	text-align: center;
	cursor: pointer;
}

.lastName {
	width: 30px;
	height: 30px;
}

.strokes {
	width: 155px;
	height: 30px;
}

.lastNameBlurred {
	background-color: #EAF2FF;
}

.lastNameClicked {
	background-color: #FBEC88;
}

.customerChooserBody {
	background-color: gray;
	padding: 10px;
}
</style>

<div id="_customerChooser" class="easyui-layout" data-options="fit:true,border:false">
	<!-- 筆劃 -->
	<div id="_lastNameStrokesPanel" data-options="region:'north',height:80,split:true,border:true" style="padding: 2px"></div>

	<div id="_customerPanel" data-options="region:'center',title:'',split:true,border:false">
		<div class="easyui-layout" data-options="fit:true,border:false">
			<!-- 姓氏 -->
			<div id="_lastNamePanel" data-options="region:'north',height:150,split:true,border:true" style="padding: 2px"></div>

			<!-- 客戶資訊 -->
			<div id="_customerPanel" data-options="region:'center',title:'',split:true,border:true">
				<table id="_customerList"></table>
			</div>
		</div>
	</div>
</div>

<div id="_customerListTools">
	<table>
		<tr>
			<td><a href="#" class="easyui-linkbutton" data-options="iconCls:'icon-check',plain:true" onclick="CustomerChooser.select();">確定</a></td>
			<td><a href="#" class="easyui-linkbutton" data-options="iconCls:'icon-cancel',plain:true" onclick="CustomerChooser.cancel();">取消</a></td>
		</tr>
	</table>
</div>

<script>
	$(document).ready(function() {
		$("#_lastNameStrokesPanel").mask("資料查詢中...");

		$.post("../system/customerLastNames2", {
			eno : "${param.eno}"
		}, function(data) {
			$("#_customerChooser").data("lastNames", data); // 將查到的資料儲存起來

			$.each(data, function(index, value) {
				var label = value.MIN_STROKE + " ~ " + value.MAX_STROKE + "劃" + (index == data.length - 1 ? "+英" : "") + " (" + value.COUNT + "筆)"; // 最後的筆劃包含英文字
				$("#_lastNameStrokesPanel").append("<span class='strokes' onclick='CustomerChooser.selectStrokes(" + index + ")'>" + label + "</span>");
			});

			CustomerChooser.setMouseEffect($("#_lastNameStrokesPanel span"));
			$("#_lastNameStrokesPanel").unmask();
		}, "json");

		var multiple = eval("${param.multiple}");

		$("#_customerList").datagrid({
			url : "../system/customersByLastName",
			idField : "UUID",
			selectOnCheck : true,
			checkOnSelect : true,
			singleSelect : !multiple,
			multiple : multiple,
			rownumbers : true,
			checkbox : true,
			toolbar : "#_customerListTools",
			maximized : true,
			border : false,
			fitColumns : false,
			nowrap : false,
			sortName : "CNAME",
			sortOrder : "asc",
			pagination : true,
			pageSize : 10,
			pageList : [ 10, 15, 20 ],

			columns : [ [ {
				field : "CHECKBOX",
				checkbox : true
			}, {
				field : "UUID",
				hidden : true
			}, {
				field : "CNAME",
				title : "姓名",
				sortable : true,
				width : 80,
				fixed : true,
				halign : "center",
				align : "left"
			}, {
				field : "ID",
				title : "身份證號",
				sortable : true,
				fixed : true,
				width : 75,
				halign : "center",
				align : "center"
			}, {
				field : "BIRTHDAY",
				title : "生日",
				sortable : true,
				width : 70,
				fixed : true,
				halign : "center",
				align : "center",
				formatter : function(value, row, index) {
					return DateTimeUtil.transferIsoDate(value);
				}
			}, {
				field : "ADDR_M",
				title : "地址",
				sortable : true,
				width : 250,
				halign : "center",
				align : "left"
			} ] ]
		});

	});

	CustomerChooser = new function() {
		return {
			selectStrokes : function(index) {
				// 移除原有姓氏
				$("#_lastNamePanel").children().remove();

				// 取得客戶姓氏資料
				var strokes = $("#_customerChooser").data("lastNames");

				if (strokes[index].COUNT > 0) {
					$.each(strokes[index].LAST_NAMES, function(index, value) {
						$("#_lastNamePanel").append("<span class='lastName' onclick=CustomerChooser.selectLastName('" + value + "')>" + value + "</span>");
					});

					this.setMouseEffect($("#_lastNamePanel span"));
				}
			},

			selectLastName : function(lastName) {
				_testConnection();

				$("#_customerList").datagrid("load", {
					eno : "${param.eno}",
					lastName : lastName
				});
			},

			setMouseEffect : function($e) {
				$e.mouseenter(function() {
					$(this).addClass("lastNameBlurred");
				}).mouseleave(function() {
					$(this).removeClass("lastNameBlurred");
				}).click(function() {
					$e.removeClass("lastNameClicked");
					$(this).addClass("lastNameClicked");
				});
			},

			select : function() {
				var rows = $("#_customerList").datagrid("getSelections");

				if (rows.length == 0)
					_showError("您尚未選取任何項目!");
				else
					_triggerEvent("customerChooser", "customerChooser:select", eval("${param.multiple}") ? rows : rows[0]);
			},

			cancel : function() {
				_triggerEvent("customerChooser", "customerChooser:cancel");
			}
		};
	};
</script>