<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="st" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="sf" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="security" uri="http://www.springframework.org/security/tags"%>
<link rel="stylesheet" type="text/css" href="${_contextPath}/resources/css/organizationMenu.css">

<%-- 儲存 app.properties 中 law.module 的設定值到 _module --%>
<st:eval var="_module" expression="@propertyConfigurer.getProperty('law.module')" />

<!-- 查詢輸入 UI -->
<input id="_enoSearchbox" name="_enoSearchbox" class="easyui-searchbox" style="padding: 2px" listenTo="orgMenu orgMenuTree" data-options="icons:[{iconCls:'icon-delete3',handler:_clearSearch}]"></input>

<!-- 組織層級 UI -->
<ul id="_orgMenuTree" style="margin-top: 5px"></ul>

<!-- 以下為 CODE -->

<c:set var="_orgMenuURL">
	<c:choose>
		<%-- 所屬組織層級 --%>
		<c:when test="${_orgMenuType == 'ORG' }">
			${_contextPath}/system/orgMenu
		</c:when>
		<%-- 引薦人層級 --%>
		<c:when test="${_orgMenuType == 'REF' }">
			${_contextPath}/system/refMenu
		</c:when>
	</c:choose>
</c:set>

<script type="text/javascript">
	// 查詢業代的 cookie name
	var _searchEnoCookieName = "${_module}_SearchEno_${_user.ENO}"; // 個人搜尋業代記錄 cookie name
	var _selectedEnoCookieName = "${_module}_SelectedEno_${_user.ENO}"; // 個人目前點選業代記錄 cookie name

	$(window).load(function() { // 須在所有元件都載入完成後才執行, 因要發出 orgMenu:select event, 讓其他先載入完成的元件可等待接收此 event (LawTemplate 1.4+)
		// 查詢輸入初始
		$("#_enoSearchbox").searchbox({
			prompt : "<st:message code="law.orgmenu.searchPrompt" />",

			//查詢業代
			searcher : _searchEno
		}).on("orgMenu:resize", function(event, data) { // 接收 orgMenu 發出的 panel resize 事件以調整 searchbox 寬度
			_fitWidthForSearchbox(data.width);
		}).on("orgMenuTree:expand orgMenuTree:collapse", function() { // 接收組織樹發出的展開/縮合事件以調整 searchbox 寬度
			_fitWidthForSearchbox($("#panelOrgMenu").panel("options").width);
		}).searchbox("textbox").css("text-transform", "uppercase"); // 英文字母轉大寫

		// 組織層級樹初始
		$("#_orgMenuTree").tree({
			lines : true,

			// 過濾不顯示的節點
			loadFilter : function(data, parent) {
				var eno = $("#_enoSearchbox").searchbox("getValue");

				if (data.length > 0 && data[0].attributes.ORG != "5") { // root node 非行政人員才顯示
					if (eno != "") { // 搜尋欄不為空
						// 記錄查詢業代於 cookie
						$.cookie(_searchEnoCookieName, data[0].attributes.ENO, {
							path : "/"
						});
					}

					return data;
				} else {
					if (eno != "") {
						_showWarning(_getMessage("<st:message code="law.orgmenu.searchFailed" javaScriptEscape="true" />", [ eno.toUpperCase() ]));
						$("#_enoSearchbox").searchbox("textbox").focus();
					} else {
						$.removeCookie(_searchEnoCookieName, {
							path : "/"
						});
					}

					return []; // 一定要 return 一空陣列
				}
			},

			// 查詢成功後動作
			onLoadSuccess : function(node, data) {

				_triggerEvent("orgMenuTree", "orgMenuTree:expand"); // 調整 searchbox 寬度, 因應組織樹過長產生 scrollbar
				_showTooltipForTreeNodeWhenExceedingContainer("panelOrgMenu", "_orgMenuTree");
				_maskOrgMenu(false); // 移除區塊遮罩

				// 組織層級載入完成後, 自動 select root node
				if (data.length > 0) {
					var selectedEno = $.cookie(_selectedEnoCookieName);
					var selectedNode = null;

					if (selectedEno)
						selectedNode = $(this).tree("find", selectedEno);

					if (!selectedNode) {
						selectedEno = data[0].id;
						selectedNode = $(this).tree("find", selectedEno);
					}

					if (selectedNode) {
						$(this).tree("select", selectedNode.target);
						$(this).tree("scrollTo", selectedNode.target);

						_showInformation(_getMessage("<st:message code="law.orgmenu.searchResult" javaScriptEscape="true" />", [ selectedEno, selectedNode.attributes.ENAME ]));
					}
				}

				$("#_enoSearchbox").searchbox("textbox").focus(); // 查詢完畢後游標回到輸入欄位
			},

			onExpand : function() {
				_triggerEvent("orgMenuTree", "orgMenuTree:expand");
			},

			onCollapse : function() {
				_triggerEvent("orgMenuTree", "orgMenuTree:collapse");
			},

			// 點選節點觸發動作
			onSelect : function(node) {
				// 將點選的 ENO 儲存於 cookie
				$.cookie(_selectedEnoCookieName, node.attributes.ENO, {
					path : "/"
				});

				_triggerEvent("orgMenu", "orgMenu:select", node); // 觸發事件 orgMenu:select
			},

			// 節點顯示資訊, 1.3.3+ 才有用
			formatter : function(node) {
				if (node.disabled)
					return "<span class='disabledOrgNode'>" + node.text + "</span>";
				else
					return node.text;
			},

			onBeforeSelect : function(node) {
				if (node.disabled)
					return false;
			}
		});

		// 加入"全部展開" tool bar
		_addOrgMenuTool("icon-expandAll", function() {
			$("#_orgMenuTree").tree("expandAll");
		});

		// 加入"全部縮合" tool bar
		_addOrgMenuTool("icon-collapseAll", function() {
			$("#_orgMenuTree").tree("collapseAll");
		});

		// 頁面開啟時自動查詢業代 
		var autoSearchEno = "";

		if ("${_user.ORG}" == "5") { // 行政人員, 查詢最後一次查詢的業代
			autoSearchEno = $.cookie(_searchEnoCookieName); // 由 cookie 中取得上一次查詢的業代, LawTemplate 1.3+

			if (!autoSearchEno || autoSearchEno == "") {
				_fitWidthForSearchbox($("#panelOrgMenu").panel("options").width); // 調整搜尋輸入欄位寬度
				$("#_enoSearchbox").searchbox("textbox").focus(); // 游標移至輸入欄位
				return;
			}
		} else
			// 非行政人員, 查詢自己
			autoSearchEno = "${_user.ENO}";

		$("#_orgMenuTree").tree({
			url : "${_orgMenuURL}",
			onBeforeLoad : function(node, param) {
				param.eno = autoSearchEno;

				var a = _getMessage("<st:message code="law.orgmenu.searching" javaScriptEscape="true" />", [ autoSearchEno ]);
				_maskOrgMenu(true, a); // 區塊遮罩
			}
		});
	});

	// 清空查詢結果
	function _clearSearch(e) {
		$("#_enoSearchbox").searchbox("clear"); // 清空查詢輸入欄位
		$("#_orgMenuTree").tree("loadData", []); // 清空組織樹

		// 清空查詢業代 cookie
		$.cookie(_searchEnoCookieName, "", {
			path : "/"
		});

		_triggerEvent("orgMenu", "orgMenu:select", null); // 觸發事件 orgMenu:select
	}

	// 查詢業代
	function _searchEno(value, name) {

		var eno = $.trim(value).toUpperCase();
		$("#_enoSearchbox").searchbox("setValue", eno);

		if (eno == "") {
			_showError("<st:message code="law.orgmenu.searchPrompt" />!");
			$("#_enoSearchbox").searchbox("textbox").focus();
		} else {
			if ("${_user.ORG}" == "5") { // 行政人員可查詢
				$("#_orgMenuTree").tree({
					url : "${_orgMenuURL}",
					onBeforeLoad : function(node, param) {
						param.eno = eno;

						_maskOrgMenu(true, _getMessage("<st:message code="law.orgmenu.searching" javaScriptEscape="true" />", [ eno ])); // 區塊遮罩						
					}
				});
			} else { // 非行政人員只能查詢自已及以下業務
				var node = $("#_orgMenuTree").tree("find", eno);

				if (node)
					$("#_orgMenuTree").tree("select", node.target);
				else
					_showWarning(_getMessage("<st:message code="law.orgmenu.searchFailed" javaScriptEscape="true" />", [ eno ]));
			}
		}
	}

	// 調整 searchbox 寬度
	function _fitWidthForSearchbox(width) {
		$("#_enoSearchbox").searchbox("resize", width - 17 - ($("#panelOrgMenu")[0].clientHeight < $("#panelOrgMenu")[0].scrollHeight ? 17 : 0));
	}
</script>
