<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib prefix="s" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="st" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="ss" uri="http://www.springframework.org/security/tags"%>


<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/resources/css/header.css">

		<div class="navbar-header">
          <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-ex1-collapse">
              <span class="sr-only">Toggle navigation</span>
              <span class="icon-bar"></span>
              <span class="icon-bar"></span>
              <span class="icon-bar"></span>
          </button>
         	<!--<a class="navbar-brand" href="second.html">SB Admin</a>-->
		<img src="${_contextPath}/resources/img/logo.gif" height="40" width="245" >
		<span id="_menuItemHierarchy" listenTo="menu"></span>
		</div>
		<ul class="nav navbar-right top-nav">
             <li class="dropdown">
                 <a href="#" class="dropdown-toggle" data-toggle="dropdown">登入者 : <i class="fa fa-user"></i> ${_user.ENO} <b>${_user.ENAME}</b> <b class="fa fa-chevron-circle-down"></b></a>
                 <ul class="dropdown-menu">
                     <li>
                         <a data-toggle="tooltip" data-placement="bottom"  title="登入時間"><i class="fa fa-fw fa-user"></i><fmt:formatDate pattern="yyyy-MM-dd HH:mm:ss" value="${_user.LOGIN_TIME}" /></a>
                     </li>
                     <li>
                          <a href="#"><i class="fa fa-fw fa-envelope"></i><span id="_versionInfo"><st:message code="law.header.versionInfo" /></span></a>
                     </li>
                     <li class="divider"></li>
                     <li>
                        <a href="${_contextPath}/${initParam['spring.profiles.active'] == 'cas' ? 'j_spring_cas_security_logout' : 'static/j_spring_security_logout'}"><i class="fa fa-fw fa-power-off"></i><st:message code="law.header.logout" /></a>
                     </li>
                 </ul>
             </li>
         </ul>
            


<script type="text/javascript">
	$(document).ready(function() {
// 		$("#_headerTB").find("td[alt]").tooltip({
// 			position : "left",
			
// 			content : function() {
// 				return $(this).attr("alt");
// 			},
			
// 			onShow : function() {
// 				$(this).tooltip("tip").css("top", $(this).offset().top - 2);
// 			}
// 		});

		// 版本資訊 Tooltip
// 		$("#_versionInfo").tooltip({
// 			content : $("#_versionInfoContent"),
// 			onShow : function() {
// 				$(this).tooltip('arrow').css("left", 186);
// 				$(this).tooltip("tip").css("left", $(this).offset().left - 165).css("top", $(this).offset().top + 23);
// 			}
// 		});
		// 接收切換查詢選項的事件, 並將選項路徑顯示於功能選項路徑後
// 		$("#_subMenuItemHierarchy").on("subMenu:changeMenuItem", function(event, data) {
// 			var hierarchy = "";

// 			for (var i = 0; i < data.length; i++) {
// 				var html = $("<div>" + data[i] + "</div>").text(); // 將任何 html tag 的部份移除, 只取 text

// 				if (i == data.length - 1)
// 					html = "<span style='font-size:larger;font-weight:bold'>" + html + "</span>";

// 				hierarchy += " / " + html;
// 			}

// 			$(this).html(hierarchy);
// 		});

		// 接收切換功能選項的事件, 並將選項路徑顯示於目前位置
		$("#_menuItemHierarchy").on("menu:changeMenuItem", function(event, data) {
			var hierarchy = "";

			for (var i = 0; i < data.length; i++) {
				hierarchy += " / " + $("<div>" + data[i] + "</div>").text(); // 將任何 html tag 的部份移除, 只取 text
			}
			debugger
			$(this).html(hierarchy);
		});
	});
</script>
