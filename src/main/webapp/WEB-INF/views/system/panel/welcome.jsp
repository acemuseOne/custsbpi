<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags"%>

<link rel="stylesheet" type="text/css" href="${_contextPath}/resources/css/welcome.css">

<!-- 背景圖 -->
<div class="welcomeMsg"></div>

<sec:authorize access="hasAnyRole('DEPT_I1','DEPT_I2','DEPT_I3')">
	<div class="release current">
		<p class="title">LawTemplate V2.3 Release Notes</p>
		<div class="item">發佈日期：2015-04-07</div>
		<div class="item">更新功能：</div>
		<ul>
			<li>新增保險公司 (遠雄, 友邦, 安聯, 中泰, 中信, 中國, 富邦, 國華) 經代網頁開啟功能</li>
			<li>修改變更/忘記密碼機制 (OTP)</li>
		</ul>
	</div>
	
	<div class="release old">
		<p class="title">LawTemplate V2.2 Release Notes</p>
		<div class="item">發佈日期：2015-02-25</div>
		<div class="item">更新功能：</div>
		<ul>
			<li>LawButton 新增 methods: setShownButtons(), setHiddenButtons()</li>
			<li>新增 Button 區塊按鈕下拉選單功能: LawMenuButton</li>
			<li>修正百家姓客戶選取器問題</li>
			<li>新增 lawui-customerchooser UI 元件</li>
			<li>新增 lawui-treechooser UI 元件</li>
			<li>新增 lawui-unitchooser UI 元件</li>
			<li>最大上傳檔案容量限制設定於 pom.xml property: maxUploadSize</li>
		</ul>
	</div>

	<div class="release old">
		<p class="title">LawTemplate V2.1 Release Notes</p>
		<div class="item">發佈日期：2015-01-14</div>
		<div class="item">更新功能：</div>
		<ul>
			<li>修改 Button 區塊按鈕建立及操作方式</li>
			<li>組織層級區塊工具列新增"清除查詢結果"按鈕</li>
			<li>修正組織層級區塊中業代輸入欄位寬度未自動符合區塊問題</li>
		</ul>
	</div>

	<div class="release old">
		<p class="title">LawTemplate V2.0 Release Notes</p>
		<div class="item">發佈日期：2014-09-30</div>
		<div class="item">更新功能：</div>
		<ul>
			<li>easy ui 版本升級: 1.3.5 =&gt; 1.4</li>
			<li>jQuery 版本升級: 1.10.0 =&gt; 1.11.1</li>
			<li>spring framework 版本升級: 3.2.3 =&gt; 4.1.1</li>
			<li>spring security 版本升級: 3.1.4 =&gt; 3.2.5</li>
			<li>apache tiles 版本升級: 2.2.2 =&gt; 3.0.3</li>
			<li>DAO 新增成員 NamedParameterJdbcTemplate (npjt); 將移除 SimpleJdbcTemplate (sjt)</li>
			<li>law-model 不相依於 law-web：law-web 中的 EmailSender 移至 law-utility</li>
			<li>ajax request 前不再須先呼叫 _testConnection()</li>
			<li>新密碼機制 (登入 / 忘記密碼 / 變更密碼)</li>
			<li>權限模組：資料過濾及遮罩</li>
			<li>加入 Maven 專案管理</li>
		</ul>
	</div>

	<div class="release old">
		<p class="title">LawTemplate V1.4.0 Release Notes</p>
		<div class="item">發佈日期：2014-06-30</div>
		<div class="item">更新功能：</div>
		<ul>
			<li>組織層級類型區分為二類：所屬組織層級＆引薦人層級</li>
			<li>功能選單外部網頁開啟方式</li>
			<li>多 DB 來源：law99, mate57, law2</li>
			<li>作業項目 / 查詢條件 / 查詢項目</li>
			<li>百家姓客戶選取功能</li>
		</ul>
	</div>

	<!-- Release Notes -->
	<div class="release old">
		<p class="title">LawTemplate V1.3.1 Release Notes</p>
		<div class="item">發佈日期：2014-01-06</div>
		<div class="item">更新功能：</div>
		<ul>
			<li>easy ui 版本升級: 1.3.4 =&gt; 1.3.5</li>
		</ul>
	</div>

	<div class="release old">
		<p class="title">LawTemplate V1.3.0 Release Notes</p>
		<div class="item">發佈日期：2013-12-03</div>
		<div class="item">更新功能：</div>
		<ul>
			<li>easy ui 版本升級: 1.3.3 =&gt; 1.3.4; 原 resources\bundle\jquery-easyui-1.3.3\exts\*.js 移至 resources\bundle\jquery-easyui-exts\</li>
			<li>spring framework 版本升級: 3.0.7 =&gt; 3.2.3</li>
			<li>spring security 版本升級: 3.0.7 =&gt; 3.1.4</li>

			<li>在 web.xml 設定登入方式為「單機模式」(standalone) 或「CAS 單點登入」(cas)</li>
			<li>新增功能權限設定及使用者角色</li>
			<li>單位行政可於組織層級中查詢所屬單位業務</li>
			<li>組織層級最近一次查詢成功之業代記錄於 cookie, 保留至使用者關閉瀏覽器</li>
			<li>welcome 頁面裡的版本 release notes 只有 "資訊部" 人員看得到</li>
			<li>templates.xml 新增 Content 樣版, 用以切換右半部內容區塊</li>
			<li>SubMenu 區塊切分為「查詢選項」及「作業項目」</li>
			<li>Composite 樣版新增 informationBorder, bodyBorder, descriptionBorder. 預設值皆為 true</li>

			<li>_addXXXButton() 多第四(btnGroups) 參數</li>
			<li>新增 JS 公用 API: _setEditorHeight(), _hideInfoPanel(), _showInfoPanel(), _loadContent(), _testConnection(), _showProcessing(), _hideProcessing(), _maskXXX()</li>

		</ul>
	</div>

	<!-- Release Notes -->
	<div class="release old">
		<p class="title">LawTemplate V1.2.1 Release Notes</p>
		<div class="item">發佈日期：2013-10-11</div>
		<div class="item">更新功能：</div>
		<ul>
			<li>Composite 樣版新增編輯區塊設定</li>
			<li>新增公用 Javascript API: _openEditor(), _closeEditor()</li>
		</ul>
	</div>

	<div class="release old">
		<p class="title">LawTemplate V1.2.0 Release Notes</p>
		<div class="item">發佈日期：2013-08-21</div>
		<div class="item">更新功能：</div>
		<ul>
			<li>佈署至正式區不須再更改 persistence.xml 裡的資料庫連線設定</li>
			<li>記錄各功能網頁 (網址: /XXX/show) 點擊資訊</li>
			<li>Log Level 調為 ERROR (log4j.properties)</li>
			<li>Hibernate SQL 查詢敍述設定調為不顯示 (applicationContext.xml)</li>
			<li>頁首 "現在的位置" 中 "查詢選項" 最後路徑的字體變粗並加大顯示</li>
			<li>原 tiles.xml 依用途分類為 error.xml, functions.xml, system.xml, templates.xml. 開發者欲新增功能頁面, 只須在 <span class="emphasize">functions.xml</span> 中繼承 Composite 複合樣版
			</li>
			<li>新增 "預覽" (Preview) 區塊及相關公用 Javascript API</li>
			<li>新增 Body tiles 樣版, 便於根據此樣版產生的網頁動態載入至主頁(Body)或預覽(Preview)區塊</li>
			<li>Composite 複合樣版中查詢選項(subMenu), 訊息(information), 主頁(body), 說明(description), 預覽(preview)等區塊新增 padding 屬性, 其預設值皆為 5</li>
			<li>新增公用 Javascript API <span class="emphasize">_showTooltipForTreeNodeWhenExceedingContainer()</span> - 當樹節點title長度超出其容器邊界範圍時顯示tooltip
			</li>
			<li>替換 lawCore.jar &amp; lawModule.jar 為 <span class="emphasize">law-model.jar</span> &amp; <span class="emphasize">law-utility.jar</span> &amp; <span class="emphasize">law-web.jar</span></li>
		</ul>
	</div>

	<div class="release old">
		<p class="title">LawTemplate V1.1.0 Release Notes</p>
		<div class="item">發佈日期：2013-06-24</div>
		<div class="item">更新功能：</div>
		<ul>
			<li>複合樣版 Composite 採用 EasyUI 最新版本 1.3.3 及 jQuery 1.10.1
				<ul>
					<li class="important">注意：EasyUI datagrid 元件必須指定 pageSize (每頁資料列數) 屬性, 否則使用分頁參數時, 會在後端 Controller 發生 Binding Exception!</li>
				</ul>
			</li>
			<li>Spring Framework 3.0.5 =&gt; 3.0.7</li>
			<li>新增 EasyUI 擴充元件 Jascript Library: resources\bundle\jquery-easyui-1.3.3\exts\*.js</li>
			<li class="important">系統版本資訊設定移至 \WEB-INF\config\system\app.properties 屬性 law.app.version (參照 LawTemplate 1.1.0 前端開發手冊「2.1.2 Web Application Properties 設定」2. )</li>
			<li class="important">原 tiles.xml 中的複合樣版 Composite 所使用的區塊 JSP 檔案搬移至同一層目錄 panel\ 下</li>
			<li>新增複合樣版 Composite 設定屬性 theme, 可指定要套用的樣式. 預設值為 default, 其他設定值為: black, bootstrap, cupertino, dark-hive, gray, metro, metro-blue, metro-gray, metro-green, metro-orange, metro-red,
				pepper-grinder, sunny
				<ul>
					<li class="important">注意：目前測試結果發現, 如使用右列樣式, 會使 EasyUI 元件 Tooltip 發生問題: cupertino, dark-hive, pepper-grinder, sunny</li>
				</ul>
			</li>
			<li>使用自製 CSS 或 Javascript 等資源檔, 請將檔案置於開發 JSP 頁面同一層目錄或子目錄下, 資源檔路徑參照 LawTemplate 1.1.0 前端開發手冊「2.3 開發準則」5. 使用自製資源檔</li>
			<li>新增取得系統(DB)日期 Controller URL: /system/sysDate (回傳格式: YYYYMMDD)</li>
			<li>DateTimeUti l.js 新增 DateTimeUtil.convertDateToISOString(date) 函式, 取得 date 型態物件的 ISO 日期格式字串</li>
			<li>修改及新增「button」區塊中的按鈕操作方式 (加入 &amp; 移除 &amp; 啟用/不啟用. 參照 LawTemplate 1.1.0 前端開發手冊「3.4 Button類」公用 Javascript API)</li>
		</ul>

		<span>更新檔案：</span>
		<ul>
			<li>/WEB-INF/lib/lawCore.jar</li>
			<li>/WEB-INF/lib/lawModule.jar</li>

			<li>/WEB-INF/views/system/layout/template/composite.jsp</li>
			<li>/WEB-INF/views/system/layout/template/simplified.jsp</li>
			<li>/WEB-INF/views/system/welcome.jsp</li>
			<li>/WEB-INF/views/system/layout/header.jsp
			<li>/WEB-INF/views/pl/qy/qcr/subMenu.jsp</li>

			<li>/resources/bundle/jquery-easyui-1.3.3/plugins/jquery.layout.js</li>
			<li>/resources/js/views/system/layout/template/composite.js</li>
			<li>/resources/js/common/util/datetime/DateTimeUtil.js</li>
			<li>/resources/bundle/jquery-easyui-1.3.3/locale/easyui-lang-zh_TW.js</li>
			<li>/resources/bundle/jquery-easyui-1.3.3/locale/easyui-lang-zh_CN.js</li>
			<li>....</li>
			<li>/resources/css/icon.css</li>
			<li>/resources/css/welcome.css</li>
			<li>....</li>
			<li>/resources/images/button/*
			<li>/WEB-INF/config/messages/*</li>
			<li>....</li>
			<li>/WEB-INF/config/system/app.properties</li>
			<li>....</li>
		</ul>
	</div>

	<div class="release old">
		<p class="title">LawTemplate V1.0.2 Release Notes</p>
		<div class="item">發佈日期：2013-06-17</div>
		<div class="item">更新功能：</div>
		<ul>
			<li>修改登入失敗時, 游標仍會停留在"業代"輸入欄問題
			<li>修改公用 Javascript API _getTreeNodeHierarchy() 多第三個參數"Tree node 格式化函式", 以處理 tree node 顯示格式</li>
		</ul>

		<span>更新檔案：</span>
		<ul>
			<li>/WEB-INF/views/system/layout/template/composite.jsp</li>
			<li>/WEB-INF/views/system/auth/login.jsp</li>
			<li>/WEB-INF/views/system/welcome.jsp</li>
			<li>/WEB-INF/views/system/layout/header.jsp
			<li>/WEB-INF/views/pl/qy/qcr/subMenu.jsp</li>
			<li>/WEB-INF/lib/lawModule.jar</li>
			<li>/resources/js/views/system/layout/template/composite.js</li>
		</ul>
	</div>

	<div class="release old">
		<p class="title">LawTemplate V1.0.1 Release Notes</p>
		<div class="item">發佈日期：2013-06-11</div>
		<div class="item">更新功能：</div>
		<ul>
			<li>提供 _triggerChangeSubMenuItemEvent() 讓開發者可以在頁首 (header.jsp) 顯示目前 "查詢選項" 項目的階層路徑</li>
			<li>新增公用 Javascript API: _triggerEvent()</li>
			<li>新增公用 Javascript API: _getTreeNodeHierarchy()</li>
			<li>新增公用 Javascript API: _triggerChangeSubMenuItemEvent()</li>
		</ul>
	</div>
</sec:authorize>

