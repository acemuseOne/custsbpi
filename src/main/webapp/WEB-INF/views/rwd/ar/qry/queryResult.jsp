<?xml version="1.0" encoding="UTF-8" ?>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ page import="tw.com.lawbroker.model.enumeration.tr.*" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
	<c:forEach var="data" items="${rows}">
		<tr id = '${data.SIGNUP_UUID }'>
			<td data-title="活動內容">
<%-- 				<button type="button" class="btn btn-info" data-toggle="modal" onclick="loadQueryForm('${data.SIGNUP_UUID}','${data.SESSION_UUID}')">檢視活動</button> --%>
				<button class="fa fa-search btn  btn-info " type="button" onclick="loadQueryForm('${data.SIGNUP_UUID}','${data.SESSION_UUID}')">檢視活動</button>
			</td>
			<c:choose>
				<c:when test="${data.SIGNUP_STATUS_NAME == '正常'}">
		        	<td data-title="取消報名"><button type="button" class="btn btn-danger " data-toggle="modal" onclick="cancelSignup('${data.SIGNUP_UUID}','${data.TYPE_CODE}')">取消</button></td>
		    	</c:when>    
		    	<c:otherwise>
		        	<td data-title="取消報名">　</td>
		    	</c:otherwise>
			</c:choose>
			<td data-title="報名狀態">${data.SIGNUP_STATUS_NAME}</td>
			<td data-title="活動名稱">${data.ACTIVITY_NAME}</td>
			<td data-title="場次名稱">${data.SESSION_NAME}</td>
			<td data-title="場次時間">${data.sdt}  至${ data.edt} </td>
			<td data-title="報名費用"><i class="fa fa-usd"></i>${data.PAYABLE}</td>
			<td data-title="繳費狀態">${data.FEE_STATUS}</td>
			<td data-title="報名人員">${data.SIGNUP_ENO_NAME}</td>
			<td data-title="參加人員" name ="ATT_PERSON">${data.ATT_PERSON}</td>
		</tr>
	</c:forEach>
