<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ page import="tw.com.lawbroker.model.enumeration.tr.*" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<script type="text/javascript" src="${_contextPath}/resources/js/common/util/datetime/DateTimeUtil.js"></script>


                <!-- Page Heading -->
                <div class="row">
                    <div class="col-lg-12">
                        <h3 class="page-header  ">
                            <div class="fa fa-pencil-square-o">我的活動查詢</div>
                        </h3>
                        <ol class="breadcrumb">
                           <i class="fa fa-search btn btn-success" onclick="showRow('queryRow')">查詢條件</i>
                           <span>　</span>
                           <i></i><a href="javascript:showRow('actRow')">活動列表</a>
                           <span id="sessionSpan" style="display:none">
                           	<i class="fa fa-arrow-right"></i>活動場次說明
                           </span>
                        </ol>
                    </div>
                </div>
                <!-- /中間要加入的table or form -->
				<div class="row uiRow" id="queryRow" style="display:none">
					<div class="col-md-12">
						<div class="panel panel-default">
	                        <div class="panel-heading">
	                            	<i class="fa fa-pencil-square-o"></i>查詢條件
	                        </div>
	                        <div class="panel-body">
			                  <div id="formback">
								<form role="form" id="subMenuQueryForm" >
								    <div class=" col-lg-12">
										<label for="actName">活動名稱:</label>
								      	<input type="text" class="form-control" id="actName" name="actName">
								    </div>
								    <div class=" col-lg-12">
								      	<label for="sessionName">場次名稱:</label>
										<input type="text" class="form-control" id="sessionName" name="sessionName">
								    </div>
								 	 <div class=" col-lg-6 col-xs-12">
								     	<label for="sessionStartDate">場次開始日期:</label>
										 <div  class="datepicker input-group date " >
										    <input id="sessionStartDate"  class="form-control" type="text" name="sessionStartDate" readonly />  
								 			<span class="input-group-addon"><i class="glyphicon glyphicon-calendar"></i></span>
										</div>
									</div>
									<div class=" col-lg-6 col-xs-12">
								     	<label for="sessionEndDate">場次結束日期:</label>
										 <div  class="datepicker input-group date " >
										    <input id="sessionEndDate" class="form-control" type="text" name="sessionEndDate" readonly />
								 			<span class="input-group-addon"><i class="glyphicon glyphicon-calendar"></i></span>
										</div>
									</div>
									<div class=" col-lg-12">
										<label for="signupStatus">報名狀態:</label>
								      	<select class="form-control" id="signupStatus" name="signupStatus"></select>
									</div>
									<div class=" col-lg-6">
										<label for="signupEno">報名業代:</label>
								      	<input type='text' class="form-control" id="signupEno" name="signupEno"/>
									</div>
									<div class=" col-lg-6">
										<label for="signupName">報名人員姓名:</label>
								      	<input type='text' class="form-control" id="signupName" name="signupName" readonly/>
									</div>
									<div class=" col-lg-12">
										<label for="idType">參加人員身份:</label>
								      	<select class="form-control" id="idType" name="idType"></select>
									</div>
									<div id="attendEnoRow" style="display:none">
										<div class=" col-lg-6">
											<label for="signupEno">參加同仁業代:</label>
									      	<input type='text' class="form-control" id="attendEno" name="attendEno" />
										</div>
										<div class=" col-lg-6">
											<label for="signupName">參加同仁姓名:</label>
									      	<input type='text' class="form-control" id="attendEnoName" name="attendEnoName" readonly/>
										</div>
									</div>
									<div id="attendNameRow"  class=" col-lg-12" style="display:none">
										<label for="signupName">參加人員姓名:</label>
								      	<input type='text' class="form-control" id="attendName" name="attendName" />
									</div>
									<button type="button" id="clearBtn" class="btn btn-warning"  onclick="_resetMyForm('subMenuQueryForm')">清空</button>
								 	<button  id="searchBtn" class="btn btn-info" onclick="$('#subMenuQueryForm').submit();">查詢</button>
								 </form>
								</div>
	                        </div>
	                    </div>
					</div>
				</div>
				<div class="row uiRow" id="actRow">
					<div class="col-md-12">
						<div class="panel panel-default">
	                        <div class="panel-heading">
	                                                                                 活動列表　<span id="queryCondition"></span>
	                        </div>
	                        
	                        <div class="panel-body">
	                        	<div class="tab-content">
				       				 <div id="act" class="tab-pane fade in active">
				       				  <div class="col-lg-12">
				                        <div class="table-responsive">
				                            <table id="querResultTable" class=" demo-table break-table table-hover"  >
												<thead>
													<tr>
														<th>活動內容</th>
														<th>取消報名</th>
														<th>報名狀態 </th>
														<th>活動名稱</th>
														<th>場次名稱</th>
														<th>場次時間</th>
														<th>報名費用</th>
														<th>繳費狀態</th>
														<th>報名人員</th>
														<th>參加人員</th>
													</tr>
												</thead>
												<tbody>
													
												</tbody>
											</table>
				                          </div>
				                       </div>
				       				 </div>
       							</div> 
	                        </div>
	                    </div>
					</div>
				</div>		
				<div class="row uiRow" id="sessRow" style="display:none">
					<div class="col-md-12">
						<div class="panel panel-default">
		                    <div class="panel-heading">
		                    	活動場次說明
		                    </div>
		                    <div class="panel-body" id ="detailPanel">
		                    	
		                    </div>
		                 </div>
		             </div>
	              </div>		  

<script type="text/javascript">
	$(document).ready(function(){ 
		debugger
		initIdType();
		initSignupStatus();
		initSignupEno();
		initAttendEno();
		initSubMenuQueryForm();
		initPage();
	});
	
	function initPage(){
		var start = moment().subtract(1, 'months').format('YYYY-MM-DD');
		var end = moment().add(1, 'months').format('YYYY-MM-DD');
		$('#sessionStartDate').val(start);
		$('#sessionEndDate').val(end);
		$('#signupStatus').val('01');
		$('#signupEno').val('${_user.ENO}');
		debugger
		$('#subMenuQueryForm').submit();
		debugger
	}

	function initSubMenuQueryForm(){
		$( "#subMenuQueryForm" ).submit(function( event ) {
			openMask();
			var param = new Object();
			<%//取出queryForm查詢條件%>
			param.actType = $("#actType").attr( "dataId");
			param.actName = $('#actName').val().trim();
			param.sessionName = $('#sessionName').val().trim();
			param.sessionStartDate = $('#sessionStartDate').val().trim();
			param.sessionEndDate   = $('#sessionEndDate').val().trim()
			param.signupStatus     = $('#signupStatus').val();
			param.signupEno        = $('#signupEno').val().trim();
			param.signupName       = $('#signupName').val().trim();  //suming add
			param.idType           = $('#idType').val();
			param.attendEno        = $('#attendEno').val().trim();
			param.attendEnoName    = $('#attendEnoName').val().trim();
			param.attendName       = $('#attendName').val().trim();
			debugger
			if(! param.signupEno){
				<%//使用者沒有輸入報名人員%>
				if( ! param.attendEno && ! param.attendName){
					<%//使用者沒有輸入參加同仁業代，參加人員姓名，後端DAO取出參加人員或是報名人員為使用者的紀錄%>
				}else{
					if(param.attendEno == '${_user.ENO}'){
						<%//使用者輸入的參加人員是本人，要查什麼都可以%>
					}else{
						<%//補上報名人員為使用者,讓系統查詢報名人員為使用者，參加人員為查詢條件的記錄%>
						$('#signupEno').val('${_user.ENO}');
						$('#signupEno').blur();
						param.signupEno = '${_user.ENO}';
						//_showInformation('系統查詢報名人員為您且符合查詢條件的記錄！');
					}
				}
			}else if(param.signupEno == '${_user.ENO}'){
				<%//使用者輸入報名人員且為使用者本人%>
				<%//可以查詢任何條件，不處理%>
			}else{
				<%//使用者輸入報名人員但非使用者本人%>
				<%//使用者只能查詢參加人員是使用者本人的紀錄%>
				param.idType    = '<%= IdTypeEnum.TYPE_01.getCode() %>';
				param.attendEno = '${_user.ENO}';
			}
			
			if(param.actType){
				<%//使用者選取活動類別，取出對應的活動代碼(CODE)%>
				param.actTypeCode = $("#actType").attr("actCode");
				debugger
			}
			<%//檢核使用者最少輸入一項查詢條件,活動日期迄值不可早於起值，報名日期迄值不可早於起值%>
			var hasCondition = false;
			var queryCondition = '';
			
			if(param.actType){
				hasCondition = true;				
				queryCondition = queryCondition + '；活動類別:' +$('#actType').val().trim();
			}
			
			if(param.actName){
				hasCondition = true;
				queryCondition = queryCondition + '；活動名稱:' + param.actName;
			}
			
			if(param.sessionName){
				hasCondition = true;
				queryCondition = queryCondition + '；場次名稱:' + param.sessionName;
			}
			
			var rs = compareDate(param.sessionStartDate, param.sessionEndDate);
			if(rs < 0){
				_showError("場次日期起日不可大於迄日!");
				return false;
			}else if(rs < 4){
				hasCondition = true;
				queryCondition = queryCondition + '；場次日期:' + param.sessionStartDate + '~' + param.sessionEndDate;
			}
							
			if(param.signupDeptCode){
				hasCondition = true;
				queryCondition = queryCondition + '；報名單位:' + $('#signupDeptCode').val().trim();
			}
			
			if(param.signupStatus){
				hasCondition = true;
				queryCondition = queryCondition + '；報名狀態:' + $('#signupStatus option:selected').text();
			}
			
			if(param.signupEno){
				hasCondition = true;
				queryCondition = queryCondition + '；報名人員:' + param.signupEno;
			}
			
			if(param.idType){
				hasCondition = true;				
				queryCondition = queryCondition + '；參加人員身份:' + $('#idType').val().trim();
			}
			
			if(param.attendEno){
				hasCondition = true;
				queryCondition = queryCondition + '；參加業代:' + param.attendEno;
			}
			
			if(param.attendName){
				hasCondition = true;
				queryCondition = queryCondition + '；參加人員姓名:' + param.attendName;
			}				
			
			if(hasCondition){
				<%//通過檢核，發出event%>
				
				$.ajax({
					type: 'POST',
					url: '${_contextPath}/RWARQRY/querySignup',
					data:getStatus('subMenuQueryForm'),
					success:  function(result) {
						$('#querResultTable > tbody').html(result);
						$('#queryCondition').html("("+queryCondition.substring(1)+")");
						var rowCount = $('#querResultTable > tbody > tr').length;
						if(rowCount==0){
							_showError("查無資料！");
						}
						showRow('actRow');
						closeMask()
					},
					dataType: 'html'
				});
				
				debugger
			}else{
				_showError("請至少輸入一項查詢條件！");
			}
			return false; // 不送出 submit
		});
	}
	
	function initAttendEno(){
		$('#attendEno').blur(function(){
			$('#attendEno').val($('#attendEno').val().toUpperCase());
			if($('#attendEno').val()){
				$.ajax({
					type: 'POST',
					url: '${_contextPath}/TR/getEName',
					data: [ {
						name : "eno",
						value : $('#attendEno').val().trim()
					}],
					success: function(result) {
						
						if(!result.ename){
							_showError("無法使用業代取得參加同仁中文姓名,請重新輸入！");
						}
						$('#attendEnoName').val(result.ename);
					},
					dataType: 'json',
					error: function(jqXHR, textStatus, errorThrown){
						console.info(errorThrown);
					}
				});
			}else{
				$('#attendEnoName').val("");
			}
		});
	}
	
	function initSignupEno(){
		$('#signupEno').blur(function(){
			$('#signupEno').val($('#signupEno').val().toUpperCase());
			if($('#signupEno').val()){
				$.ajax({
					type: 'POST',
					url: '${_contextPath}/TR/getEName',
					data: [ {
						name : "eno",
						value : $('#signupEno').val().trim()
					}],
					success: function(result) {
						if(!result.ename){
							_showError("無法使用業代取得參加同仁中文姓名,請重新輸入！");
						}
					$('#signupName').val(result.ename);
						
					},
					dataType: 'json',
					error: function(jqXHR, textStatus, errorThrown){
						console.info(errorThrown);
					}
				});
			}else{
				$('#signupName').val("");
			}
		});
	}
	
	 function initIdType(){
		 var data =   [{id:'', text:'　'},
			            {id:'<%= IdTypeEnum.TYPE_01.getCode() %>',text:'<%= IdTypeEnum.TYPE_01.getName() %>'},
			            {id:'<%= IdTypeEnum.TYPE_02.getCode() %>',text:'<%= IdTypeEnum.TYPE_02.getName() %>'},
			            {id:'<%= IdTypeEnum.TYPE_03.getCode() %>',text:'<%= IdTypeEnum.TYPE_03.getName() %>'}
			            ]
		 _makeCombobox("idType", data, "text", "id");
		 $( "#idType" ).change(function() {
			 var idType = $( this ).val();
		    	if(idType == 01){
		    		$('#attendEnoRow').show();
		    		$('#attendNameRow').hide();
		    		$('#attendName').val('');
		    	}else if(idType == ''){
		    		$('#attendEnoRow').hide();
		    		$('#attendEno').val('');
		    		$('#attendEnoName').val('');
		    		$('#attendName').val('');
		    		$('#attendNameRow').hide();
		    	}else{
		    		$('#attendEnoRow').hide();
		    		$('#attendEno').val('');
		    		$('#attendEnoName').val('');
		    		$('#attendNameRow').show();
		    	}
			});
	 	}
	
		function initSignupStatus(){
			 var data = [{id:'', text:'　'},
			            {id:'<%= SignupStatusEnum.TYPE_01.getCode() %>',text:'<%= SignupStatusEnum.TYPE_01.getName() %>'},
			            {id:'<%= SignupStatusEnum.TYPE_02.getCode() %>',text:'<%= SignupStatusEnum.TYPE_02.getName() %>'},
			            {id:'<%= SignupStatusEnum.TYPE_03.getCode() %>',text:'<%= SignupStatusEnum.TYPE_03.getName() %>'}
			            ]	
			 _makeCombobox("signupStatus", data, "text", "id");
		}
	 
	$(".datepicker").datepicker({ 
	    autoclose: true, 
	    format:'yyyy-mm-dd'
	});
	
	function showRow(rowId){
		$(".uiRow").hide();
		$("#"+rowId).show();
		
		if(rowId == 'actRow'){
			$('#sessionSpan').hide();
		}else if(rowId == 'sessRow'){
			$('#sessionSpan').show();
		}
	}
</script>
		
<script type="text/javascript">
//queryResult.jsp
	function cancelSignup(signupUUID,typeCode){
		bootbox.dialog({
			  message: "確認取消報名？",
			  title: "確認執行",
			  buttons: {
			    success: {
			      label: "確定",
			      className: "btn-default",
			      callback: function() {
					debugger
					$.post( "${_contextPath}/RWARQRY/cancelSignup",{'trdmTypeCode':typeCode, 'signupUUID': signupUUID }, function( data ) {
						if(data.SIGNUP_CODE == '00'){
							//$("#"+signupUUID).remove();
							$('#subMenuQueryForm').submit();
							_showInformation(data.SIGNUP_MSG);
						}else{
							_showError(data.SIGNUP_MSG);
						}
					},"json");
			      }
			    },
			    danger: {
			      label: "取消",
			      className: "btn-default",
			      callback: function() {
			    	  //do nothing
			      }
			    }
			  }
			}); 
	}
	
	function loadQueryForm(signupUUID, sessionUUID){
		openMask();
		debugger
		var owl = $("#owl-sche").data('owlCarousel');
		if(owl){
			owl.destroy();
		}
		
		$.ajax({
			type: 'POST',
			url: '${_contextPath}/RWARQRY/queryActivityContents',
			data: {'signupUUID': signupUUID, 'sessionUUID':sessionUUID},
			success:  function(result) {
				showRow('sessRow');
				$('#detailPanel').html(result);
				$("#owl-sche").owlCarousel({items:4});
				closeMask();
			},
			dataType: 'html'
		});
	}
	
</script>
