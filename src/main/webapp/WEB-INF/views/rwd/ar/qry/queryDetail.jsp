<?xml version="1.0" encoding="UTF-8" ?>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
								<h4 class="page-header">
		                          <div class="fa fa-file-text-o">活動說明</div>
		                        </h4>
		       				 	<div class="table-responsive ">
			                        <table id="detailTable" class=" demo-table activity-table"  >
											<tbody>
												<tr>
													<td data-title="活動名稱:" >${activityData.actName}</td>
													<td data-title="場次名稱:" >${activityData.sessionName}</td>
													<td data-title="活動類別:" >${activityData.actTypeName}</td>
													<td data-title="承辦單位:" >${activityData.actOrg}</td>
													<td data-title="聯絡方式:" >E-mail：${activityData.email}　電話：${activityData.phone}</td>
													<td data-title="場次時間:" >${activityData.sessionSdt} 至 ${activityData.sessionEdt}</td>
													<td data-title="活動地點:" >${activityData.sessionCity} ${activityData.sessionArea}</td>
													<td data-title="活動說明:" >${activityData.actReadmeShort}
														<a  data-toggle="popover"  id="actMemo_tooltip" data-placement="top"  data-content="${activityData.actReadme}"><i class="fa fa-info-circle"></i></a>
													</td>
													<td data-title="備註事項:" >${activityData.actMemoShort}
														<a  data-toggle="popover"  id="actMemo_tooltip" data-placement="top"  data-content="${activityData.actMemo}"><i class="fa fa-info-circle"></i></a>
													</td>
													<td data-title="附加檔案：">　</td>
												</tr>
											</tbody>
									</table>
							 	</div>
							 	<h4 class="page-header">
		                          <div class="fa fa-file-text-o">活動時程</div>
		                        </h4>
		                  		<div id="owl-sche" class="owl-carousel">
		                  			<c:forEach var="data" items="${scheduleData}" varStatus="loopStatus">
			                         <div class=" sessionData ${loopStatus.index % 2 == 0 ? 'alert alert-info' : ''}" >
				                       	  <div class="row">
				                       	  	<div class="col-md-3 col-xs-4" >日期 </div>
				                       	  	<div class="col-md-9 col-xs-8" >${data.schDate}</div>
				                       	  </div>
				                       	  <div class="row">
				                       	  	<div class="col-md-3 col-xs-4">時間</div>
				                       	  	<div class="col-md-9 col-xs-8">${data.schStime} ~ ${data.schEtime}</div>
				                       	  </div>
				                       	  <div class="row">
				                       	  	<div class="col-md-3 col-xs-4">主題 </div>
				                       	  	<div class="col-md-9 col-xs-8">${data.subject}</div>
				                       	  </div>
				                       	  <div class="row">
				                       	  	<div class="col-md-3 col-xs-4">主講人  </div>
				                       	  	<div class="col-md-9 col-xs-8">${data.lectureName}</div>
				                       	  </div>
				                       	  <div class="row">
				                       	  	<div class="col-md-3 col-xs-4">場地 </div>
				                       	  	<div class="col-md-9 col-xs-8">${data.area}</div>
				                       	  </div>
				                      </div>
			                      </c:forEach>
								</div>
<script type="text/javascript">
$(document).ready(function() {
	$('[data-toggle="popover"]').popover();
});
</script>
