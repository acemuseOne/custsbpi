<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ page import="tw.com.lawbroker.model.enumeration.tr.*" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

		
         <div class="panel-body">
            <div class="bs-example bs-normalTab">
                <ul class="nav nav-tabs" id="myTab">
                	<li class="active"><a data-toggle="tab" class=" glyphicon glyphicon-ok-sign" href="#canSignUP">開放報名(${canSignUpCount}筆)</a></li>
					<li><a data-toggle="tab" class=" glyphicon glyphicon-minus-sign" href="#NotYet">尚未開放報名(${NotYetSignUpCount}筆)</a></li>
			   </ul>
			</div>
			<div class="bs-example bs-stackTab">
                 <ul class="nav nav-pills nav-stacked "  id="myTab">
               		<li  role="presentation" class="active"><a data-toggle="tab" class=" glyphicon glyphicon-ok-sign " href="#canSignUP">開放報名(${canSignUpCount}筆)</a></li>
					<li  role="presentation"><a data-toggle="tab" class=" glyphicon glyphicon-minus-sign" href="#NotYet">尚未開放報名(${NotYetSignUpCount}筆)</a></li>
		   	  </ul>
			</div>
			<div class="tab-content">
    		<div id="NotYet" class="tab-pane fade ">
      			<div class="col-lg-12">
                     <div class="table-responsive " style="overflow:visible">
                           <table id="activityTableN" class="demo-table break-table table-striped table-hover "  >
							<thead>
								<tr>
									<th>活動內容</th>
									<th>活動名稱</th>
									<th>活動期間</th>
								</tr>
							</thead>
							<tbody>
								<c:forEach var="data" items="${NotYetSignUp}">
									<tr>
										<td data-title="活動內容"><button class="fa fa-search btn  btn-primary " type="button" onclick="showActContent('${data.actUUID}')">檢視活動</button></td>
										<td data-title="活動名稱">${data.actName}</td>
										<td data-title="活動期間">${data.actSdt} 至 ${data.actEdt}</td>
									</tr>
								</c:forEach>
							</tbody>
						</table>
                  	</div>
               </div>
			</div>
			<div id="canSignUP" class="tab-pane fade in active" >
			 	<div class="col-lg-12">
			 	  <div class="table-responsive" style="overflow:visible">
					    <table id="activityTable" class="demo-table break-table table-striped table-hover ">
							<thead>
								<tr>
									<th>活動內容</th>
									<th>活動名稱</th>
									<th>活動期間</th>
								</tr>
							</thead>
							<tbody>
								<c:forEach var="data" items="${canSignUp}">
								<tr>
									<td data-title="活動內容"><button class="fa fa-search btn  btn-primary " type="button" onclick="showActContent('${data.actUUID}')">檢視活動</button></td>
									<td data-title="活動名稱">${data.actName}</td>
									<td data-title="活動期間">${data.actSdt} 至 ${data.actEdt}</td>
								</tr>
								</c:forEach>
							</tbody>
						</table>
                   </div>
                 </div>
     			</div>
     		</div>  
        </div>
