<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ page import="tw.com.lawbroker.model.enumeration.tr.*" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
                <div class="row">
                    <div class="col-lg-12">
                        <h3 class="page-header  ">
                            <div class="fa fa-pencil-square-o">活動線上報名</div>
                        </h3>
                        
                        <ol class="breadcrumb">
                           <i class="fa fa-search btn btn-success" onclick="showRow('queryRow')">查詢條件</i>
                           <span>　</span>
                           <i></i><a href="javascript:showRow('actRow')">活動列表</a>
                           <span id="sessionSpan" style="display:none">
                           	<i class="fa fa-arrow-right"></i><a href="javascript:showRow('sessionRow')">場次列表</a>
                           </span>
                           <span id="signupSpan" style="display:none">
                           	<i class="fa fa-arrow-right"></i>報名場次
                           </span>
                        </ol>
                       
                    </div>
                </div>
                <!-- /中間要加入的table or form -->
                 
		<div class="row uiRow" id="queryRow" style="display:none">
			<div class="col-md-12">
				<div class="panel panel-default">
	               <div class="panel-heading">
	                    	<i class="fa fa-pencil-square-o"></i>查詢條件
	               </div>
			         <div class="panel-body" >
			            <form role="form" id="subMenuQueryForm">
							<input type="hidden"  id="actUUID" name="activityUUID" >
							<input type="hidden"  id="sessUUID" name="sessionUUID" >
							
							 <div class=" col-lg-12">
								<label for="actName">活動代碼:</label>
						      	<input type="text" class="form-control" id="actCode" name="actCode" required>
						    </div>
						    <div class=" col-lg-12">
								<label for="actName">活動名稱:</label>
						      	<input type="text" class="form-control" id="actName" name="actName" required>
						    </div>
						    <div class=" col-lg-12">
						      	<label for="sessionName">場次名稱:</label>
								<input type="text" class="form-control" id="sessionName" name="sessionName">
						    </div>
					 	 	<div class=" col-lg-6 col-xs-12">
						     	<label for="sessionStartDate">場次開始日期:</label>
								 <div  class="datepicker input-group date " >
								    <input id="sessionStartDate"  class="form-control" type="text" name="sessionStartDate" readonly />  
						 			<span class="input-group-addon"><i class="glyphicon glyphicon-calendar"></i></span>
								</div>
							</div>
							<div class=" col-lg-6 col-xs-12">
						     	<label for="sessionEndDate">場次結束日期:</label>
								 <div  class="datepicker input-group date " >
								    <input id="sessionEndDate" class="form-control" type="text" name="sessionEndDate" readonly />
						 			<span class="input-group-addon"><i class="glyphicon glyphicon-calendar"></i></span>
								</div>
							</div>
					 	 	<div class=" col-lg-6 col-xs-12">
						     	<label for="sessionStartDate">報名起始日期:</label>
								 <div  class="datepicker input-group date " >
								    <input id="signUpStartDate"  class="form-control" type="text" name="signUpStartDate" readonly />  
						 			<span class="input-group-addon"><i class="glyphicon glyphicon-calendar"></i></span>
								</div>
							</div>
							<div class=" col-lg-6 col-xs-12">
						     	<label for="sessionEndDate">報名結束日期:</label>
								 <div  class="datepicker input-group date " >
								    <input id="signUpEndDate" class="form-control" type="text" name="signUpEndDate" readonly />
						 			<span class="input-group-addon"><i class="glyphicon glyphicon-calendar"></i></span>
								</div>
							</div>
							<button type="button" id="clearBtn" class="btn btn-warning"  onclick="_resetMyForm('subMenuQueryForm')">清空</button>
							<button  id="searchBtn" class="btn btn-info" onclick="$('#subMenuQueryForm').submit();">查詢</button>
					 	</form>
			        </div>
			    </div>
			</div>
		</div>
		<div class="row uiRow" id="actRow" >
			<div class="col-md-12">
				<div class="panel panel-default">
	                <div class="panel-heading">
	                    	活動列表　<span id="queryCondition"></span>
	                </div>
	             	<div class="panel-body" id = "activityList">
			            <div class="bs-example bs-normalTab">
		                  <ul class="nav nav-tabs" id="actTab">
	                  		<li><a data-toggle="tab" class=" glyphicon glyphicon-ok-sign" href="#aceSch">開放報名(0筆)</a></li>
	 						<li><a data-toggle="tab" class=" glyphicon glyphicon-minus-sign" href="#act">尚未開放報名(0筆)</a></li>
						   </ul>
						</div>
						<div class="bs-example bs-stackTab">
		                  <ul class="nav nav-pills nav-stacked"  id="actTab">
	                 		<li  role="presentation" class="active"><a data-toggle="tab" class=" glyphicon glyphicon-ok-sign " href="#aceSch">開放報名(0筆)</a></li>
	 						<li  role="presentation"><a data-toggle="tab" class=" glyphicon glyphicon-minus-sign" href="#act">尚未開放報名(0筆)</a></li>
					   	  </ul>
						</div>
						<div class="tab-content">
	      				<div id="act" class="tab-pane fade in active">
			      			<div class="col-lg-12">
			                     <div class="table-responsive ">
		                            <table id="activityTable" class="demo-table break-table table-striped table-hover "  >
										<thead>
											<tr>
												<th>活動內容</th>
												<th>活動名稱</th>
												<th>活動期間</th>
											</tr>
										</thead>
										<tbody>
										</tbody>
									</table>
			                  	</div>
			               </div>
						</div>
						<div id="aceSch" class="tab-pane fade" >
						 	<div class="col-lg-12">
						 	  <div class="table-responsive" style="overflow:visible">
								    <table id="activityTableN" class="demo-table break-table table-hover ">
										<thead>
											<tr>
												<th>活動內容</th>
												<th>活動名稱</th>
												<th>活動期間</th>
											</tr>
										</thead>
										<tbody>
										</tbody>
									</table>
			                   </div>
			                 </div>
			     			</div>
			     		</div>  
			        </div>
			    </div>
			</div>
		</div>
		<div class="row uiRow" id="sessionRow" style="display:none">
			<div class="col-md-12">
				<div class="panel panel-default">
	                <div class="panel-body" id = "activityContent">
	               </div>
	            </div>
			</div>
		</div>
		
		<div id = "activitySignup">
			<div class="panel panel-default uiRow" id="schRow" style="display:none" >
	        </div>
			<div class="row uiRow" id="signupRow" style="display:none">
			</div>
		</div>
		
<script type="text/javascript">
//onlAll.jsp
	$(document).ready(function(){ 
	    $("#owl-sche").owlCarousel({items:4});
	    $("#owl-fee").owlCarousel({items:4});
	    initSubMenuQueryForm();
	    initPage();
	});
	
	function initPage(){
		var start = moment().subtract(1, 'months').format('YYYY-MM-DD');
		var end = moment().add(1, 'months').format('YYYY-MM-DD');
		$('#sessionStartDate').val(start);
		$('#sessionEndDate').val(end);
		debugger
		$('#subMenuQueryForm').submit();
		debugger
	}
	
	function initSubMenuQueryForm(){
		$( "#subMenuQueryForm" ).submit(function( event ) {
			openMask();
			debugger
			var param = new Object();
			<%//取出queryForm查詢條件%>
			param.actCode = $('#actCode').val().trim();
			param.actName = $('#actName').val().trim();
			param.sessionName = $('#sessionName').val().trim();
			param.sessionStartDate = $('#sessionStartDate').val().trim();
			param.sessionEndDate   = $('#sessionEndDate').val().trim();
			param.signUpStartDate  = $('#signUpStartDate').val().trim();
			param.signUpEndDate    = $('#signUpEndDate').val().trim();
			
			var hasCondition = false;
			var queryCondition = '';
			
			if(param.actCode){
				hasCondition = true;
				queryCondition = queryCondition + '；活動代碼:' + param.actCode;
			}
			if(param.actName){
				hasCondition = true;
				queryCondition = queryCondition + '；活動名稱:' + param.actName;
			}
			
			if(param.sessionName){
				hasCondition = true;
				queryCondition = queryCondition + '；場次名稱:' + param.sessionName;
			}
			
			var rs = compareDate(param.sessionStartDate, param.sessionEndDate);
			if(rs < 0){
				_showError("場次日期起日不可大於迄日!");
				return false;
			}else if(rs < 4){
				hasCondition = true;
				queryCondition = queryCondition + '；場次日期:' + param.sessionStartDate + '~' + param.sessionEndDate;
			}
							
			rs = compareDate(param.signUpStartDate, param.signUpEndDate);
			if(rs < 0){
				_showError("報名日期起日不可大於迄日!");
				return false;
			}else if(rs < 4){
				hasCondition = true;
				queryCondition = queryCondition + '；報名日期:' + param.signUpStartDate + '~' + param.signUpEndDate;
			}				
			debugger
			if(hasCondition){
				$.ajax({
					type: 'POST',
					url: '${_contextPath}/RWARONL/queryActivityList',
					data:getStatus('subMenuQueryForm'),
					success:  function(result) {
						closeMask();
						debugger
						$('#queryCondition').html("("+queryCondition.substring(1)+")");
						$('#activityList').html(result);
						var rowCount = $('#activityTable > tbody > tr').length;
						var rowCountN = $('#activityTableN > tbody > tr').length;
						if(rowCount==0 && rowCountN==0){
							_showError("查無資料！");
						}
						showRow('actRow');
					},
					dataType: 'html'
				});
				debugger
			}else{
				_showError("請至少輸入一項查詢條件！");
			}
			return false; // 不送出 submit
		});
	}
	
	
	$(".datepicker").datepicker({ 
	    autoclose: true, 
	    format:'yyyy-mm-dd'
	});
	
	function hideSignup(dataId){
		debugger
		$("#"+dataId).hide();
	}
	
	function showRow(rowId){
		$(".uiRow").hide();
		$("#"+rowId).show();
		$('#schRow').hide();
		if(rowId == 'sessionRow'){
			$('#sessionSpan').show();
			$('#signupSpan').hide();
		}else if(rowId == 'actRow'){
			$('#sessionSpan').hide();
			$('#signupSpan').hide();
		}else if(rowId == 'signupRow'){
			$('#sessionSpan').show();
			$('#signupSpan').show();
			$('#schRow').show();
		}
	}
</script>


<script type="text/javascript">
//signupActivity.jsp
	function initSignUpForm(){
		$( "#signupForm" ).submit(function( event ) {
			openMask();
			var signupData = ConvertFormToJSON('signupForm');
			var param = getStatus('subMenuQueryForm');
			var data = [];
			data.push(signupData);
			param.signupData = JSON.stringify(data);
			debugger
				$.ajax({
					type: 'POST',
					url: '${_contextPath}/RWARONL/signupSession',
					data:param,
					success:  function(result) {
						closeMask();
						debugger
						if(result[0].SIGNUP_CODE == '00'){
							<%//報名成功%>
							showSignupData();
							querysignupCount();
							_showInformation(result[0].SIGNUP_MSG);
						}else{
							_showError(result[0].SIGNUP_MSG);
						}
					},
					dataType: 'json'
				});
				return false; // 不送出 submit
			});
		}
	
	
	function showSignupData(){
		openMask();
		var owl = $("#owl-signupData").data('owlCarousel');
		if(owl){
			owl.destroy();
		}
		$("#sessionUUID").val()
		var object = getStatus('subMenuQueryForm');
		debugger
		$.ajax({
			type: 'POST',
			url: '${_contextPath}/RWARONL/signupData',
			data:getStatus('subMenuQueryForm'),
			success:  function(result) {
				closeMask();
				debugger
				$('#owl-signupData').html(result);
				$("#owl-signupData").owlCarousel({items : 4});
			},
			dataType: 'html'
		});
		
	}
	
	function cancelSignup(signupUUID){
		bootbox.dialog({
			  message: "確認取消報名？",
			  title: "確認執行",
			  buttons: {
			    success: {
			      label: "確定",
			      className: "btn-default",
			      callback: function() {
			    	var typeCode = null;
					debugger
					$.post( "${_contextPath}/RWARQRY/cancelSignup",{'trdmTypeCode':typeCode, 'signupUUID': signupUUID }, function( data ) {
						if(data.SIGNUP_CODE == '00'){
							debugger
							showSignupData();
							querysignupCount();
							_showInformation(data.SIGNUP_MSG);
						}else{
							
							_showError(data.SIGNUP_MSG);
						}
					},"json");
					
			      }
			    },
			    danger: {
			      label: "取消",
			      className: "btn-default",
			      callback: function() {
			    	  //do nothing
			      }
			    }
			  }
			}); 
	}
	
	function querysignupCount(){
		$.ajax({
			type: 'POST',
			url: '${_contextPath}/RWARONL/querysignupCount',
			data:getStatus('subMenuQueryForm'),
			success:  function(result) {
				debugger
				$('#headerData').html(result.value);
			},
			dataType: 'json'
			
		});
	}
</script>
<script type="text/javascript">
//activityList.jsp
	function showActContent(actUUID){
		openMask();
		$('#actUUID').val(actUUID);
		$.ajax({
			type: 'POST',
			url: '${_contextPath}/RWARONL/activityContent',
			data:getStatus('subMenuQueryForm'),
			success:  function(result) {
				$('#activityContent').html(result);
				showRow('sessionRow');
				closeMask();
			},
			dataType: 'html'
		});
	}
</script>
<script type="text/javascript">
//activityContent.jsp
	
	function signupAct(sessionUUID){
		$('#sessUUID').val(sessionUUID);
		openMask();
		$.ajax({
			type: 'POST',
			url: '${_contextPath}/RWARONL/signupActivity',
			data:getStatus('subMenuQueryForm'),
			success:  function(result) {
				closeMask();
				$('#activitySignup').html(result);
				showRow('signupRow');
				showSignupData();
				querysignupCount();
			},
			dataType: 'html'
		});
	}
</script>
</html>
