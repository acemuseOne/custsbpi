<%@ page language="java" contentType="text/html; charset=UTF-8"pageEncoding="UTF-8"%>
<%@ page import="tw.com.lawbroker.model.enumeration.tr.*"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="st" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="sf" uri="http://www.springframework.org/tags/form"%>
			<div class="panel panel-default uiRow" id="schRow" >
				<div class="panel-body">
					<div class="bs-example bs-normalTab">
						<ul class="nav nav-tabs" id="schTab">
							<li class="active"><a data-toggle="tab" class=" fa fa-list-alt" href="#sche">場次時程</a></li>
							<li><a data-toggle="tab" class="fa fa-usd" href="#fee">報名費用</a></li>
						</ul>
					</div>
					<div class="bs-example bs-stackTab">
						<ul class="nav nav-pills nav-stacked " id="schTab">
							<li role="presentation" class="active"><a data-toggle="tab" class="fa fa-list-alt" href="#sche">場次時程</a></li>
							<li role="presentation"><a data-toggle="tab" class=" fa fa-usd" href="#fee">報名費用</a></li>
						</ul>
					</div>
					<div class="tab-content">
						<div id="sche" class="tab-pane fade in active">
							<div id="owl-sche" class="owl-carousel">
								<c:forEach var="data" items="${scheduleData}" varStatus="loopStatus">
									<div id="sch_${data.shcUUID}" class="${loopStatus.index % 2 == 0 ? 'scheDataOdd ' : 'scheDataEven '}">
										<div class="row">
											<div class="col-md-3 col-xs-4">日期</div>
											<div class="col-md-9 col-xs-8">${data.shcDate}</div>
										</div>
										<div class="row">
											<div class="col-md-3 col-xs-4">時間</div>
											<div class="col-md-9 col-xs-8">${data.sTime} ~ ${data.eTime} </div>
										</div>
										<div class="row">
											<div class="col-md-3 col-xs-4">主題</div>
											<div class="col-md-9 col-xs-8">${data.subject}</div>
										</div>
										<div class="row">
											<div class="col-md-3 col-xs-4">主講人</div>
											<div class="col-md-9 col-xs-8">${data.lectureName}</div>
										</div>
										<div class="row">
											<div class="col-md-3 col-xs-4">場地</div>
											<div class="col-md-9 col-xs-8">${data.area}</div>
										</div>
									</div>
								</c:forEach>
							</div>
						</div>
						<div id="fee" class="tab-pane fade ">
							<div id="owl-fee" class="owl-carousel">
									<div id="fee_${feeData[0].feeUUID}" class="scheDataOdd">
										<div class="row">
											<div class="col-md-5 col-xs-5"></div>
											<div class="col-md-7 col-xs-7">原價</div>
										</div>
										<div class="row fee-row">
											<div class="col-md-3 col-xs-3"></div>
											<div class="col-md-3 col-xs-3">同仁</div>
											<div class="col-md-3 col-xs-3">保戶</div>
											<div class="col-md-3 col-xs-3">來賓</div>
										</div>
										<div class="row">
											<div class="col-md-3 col-xs-3">報名費</div>
											<div class="col-md-3 col-xs-3">${feeData[0].priceEmp}</div>
											<div class="col-md-3 col-xs-3">${feeData[0].pricePol}</div>
											<div class="col-md-3 col-xs-3">${feeData[0].priceCus}</div>
										</div>
										<div class="row">
											<div class="col-md-3 col-xs-3">保證金</div>
											<div class="col-md-3 col-xs-3">${feeData[1].priceEmp}</div>
											<div class="col-md-3 col-xs-3">${feeData[1].pricePol}</div>
											<div class="col-md-3 col-xs-3">${feeData[1].priceCus}</div>
										</div>
										<div class="row">
											<div class="col-md-3 col-xs-3">其他費用</div>
											<div class="col-md-3 col-xs-3">${feeData[2].priceEmp}</div>
											<div class="col-md-3 col-xs-3">${feeData[2].pricePol}</div>
											<div class="col-md-3 col-xs-3">${feeData[2].priceCus}</div>
										</div>
									</div>
									<div id="fee_${feeData[1].feeUUID}" class="scheDataEven">
										<div class="row">
											<div class="col-md-5 col-xs-5"></div>
											<div class="col-md-7 col-xs-7">優惠價</div>
										</div>
										<div class="row fee-row">
											<div class="col-md-3 col-xs-3"></div>
											<div class="col-md-3 col-xs-3">同仁</div>
											<div class="col-md-3 col-xs-3">保戶</div>
											<div class="col-md-3 col-xs-3">來賓</div>
										</div>
										<div class="row">
											<div class="col-md-3 col-xs-3">報名費</div>
											<div class="col-md-3 col-xs-3">${feeData[0].onsaleEmp}</div>
											<div class="col-md-3 col-xs-3">${feeData[0].onsalePol}</div>
											<div class="col-md-3 col-xs-3">${feeData[0].onsaleCus}</div>
										</div>
										<div class="row">
											<div class="col-md-3 col-xs-3">保證金</div>
											<div class="col-md-3 col-xs-3">${feeData[1].onsaleEmp}</div>
											<div class="col-md-3 col-xs-3">${feeData[1].onsalePol}</div>
											<div class="col-md-3 col-xs-3">${feeData[1].onsaleCus}</div>
										</div>
										<div class="row">
											<div class="col-md-3 col-xs-3">其他費用</div>
											<div class="col-md-3 col-xs-3">${feeData[2].onsaleEmp}</div>
											<div class="col-md-3 col-xs-3">${feeData[2].onsalePol}</div>
											<div class="col-md-3 col-xs-3">${feeData[2].onsaleCus}</div>
										</div>
									</div>
									<div id="fee_${feeData[2].feeUUID}" class="scheDataOdd">
										<div class="row">
											<div class="col-md-5 col-xs-5"></div>
											<div class="col-md-7 col-xs-7">延遲價</div>
										</div>
										<div class="row fee-row">
											<div class="col-md-3 col-xs-3"></div>
											<div class="col-md-3 col-xs-3">同仁</div>
											<div class="col-md-3 col-xs-3">保戶</div>
											<div class="col-md-3 col-xs-3">來賓</div>
										</div>
										<div class="row">
											<div class="col-md-3 col-xs-3">報名費</div>
											<div class="col-md-3 col-xs-3">${feeData[0].delayEmp}</div>
											<div class="col-md-3 col-xs-3">${feeData[0].delayCus}</div>
											<div class="col-md-3 col-xs-3">${feeData[0].delayPol}</div>
										</div>
										<div class="row">
											<div class="col-md-3 col-xs-3">保證金</div>
											<div class="col-md-3 col-xs-3">${feeData[1].delayEmp}</div>
											<div class="col-md-3 col-xs-3">${feeData[1].delayCus}</div>
											<div class="col-md-3 col-xs-3">${feeData[1].delayPol}</div>
										</div>
										<div class="row">
											<div class="col-md-3 col-xs-3">其他費用</div>
											<div class="col-md-3 col-xs-3">${feeData[2].delayEmp}</div>
											<div class="col-md-3 col-xs-3">${feeData[2].delayCus}</div>
											<div class="col-md-3 col-xs-3">${feeData[2].delayPol}</div>
										</div>
									</div>
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="row uiRow" id="signupRow" >
				<div class="col-md-12">
					<div class="panel panel-default">
						<div class="panel-heading"><i class="fa fa-pencil-square-o"></i>報名場次</div>
						<div class="panel-body">
							<form  role="form" id="signupForm">
								<c:if test="${not empty fieldList}">
									<c:forEach var="field" items="${fieldList}">
										${field}
									</c:forEach>
								</c:if>
								<button onclick="showRow('sessionRow')" class="btn btn-danger" type="button">取消</button>
								<button type="button" id="addBtn" class="btn btn-success" onclick="$('#signupForm').submit();">送出</button>
							</form>
							<h4 class="page-header" >
								 <div class="fa fa-file-text-o">報名資料 <small id ="headerData">單位:32名/20名　個人:6名/8名</small></div>
				            </h4>
				            <div id="owl-signupData" class="owl-carousel">
								<jsp:include page="signupData.jsp"/>
							</div>
						</div>
					</div>
				</div>
			</div>


<script type="text/javascript">
	$(document).ready(function() {
	    $("#owl-sche").owlCarousel({items:4});
	    $("#owl-fee").owlCarousel({items:4});
		$("#owl-signupData").owlCarousel({items : 4});
		$(".datepicker").datepicker({ 
		    autoclose: true, 
		    format:'yyyy-mm-dd'
		});
		$('#signupForm').validator();
		initSignUpForm();
		changeIdType()
	});
	
	function changeIdType(){
		$('#idtype').change(function() {
			var idType = $('#idtype').val();
			$('#eno').val("");
			$('#name').val("");
			//選擇同仁 姓名欄位改成readonly
			if(idType == 01){
				$('#eno').prop('readonly', false);
				$('#name').prop('readonly', true);
			}else{
			//選擇保戶來賓 業代欄位改成readonly
				$('#eno').prop('readonly', true);
				$('#name').prop('readonly', false);
			}
		});
		
		$('#eno').blur(function(){
			$('#eno').val($('#eno').val().toUpperCase());
			if($('#eno').val()){
				$.ajax({
					type: 'POST',
					url: '${_contextPath}/TR/getEName',
					data: [ {
						name : "eno",
						value : $('#eno').val().trim()
					}],
					success: function(result) {
						
						if(!result.ename){
							_showError("無法使用業代取得參加同仁中文姓名,請重新輸入！");
						}
						$('#name').val(result.ename);
					},
					dataType: 'json',
					error: function(jqXHR, textStatus, errorThrown){
						console.info(errorThrown);
					}
				});
			}else{
				$('#name').val("");
			}
		});
		
		
		
	}
</script>

