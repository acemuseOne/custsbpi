<%@ page language="java" contentType="text/html; charset=UTF-8"pageEncoding="UTF-8"%>
<%@ page import="tw.com.lawbroker.model.enumeration.tr.*"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>

				<div class="col-lg-12">
					<h4 class="page-header">
						<div class="fa fa-file-text-o">活動說明</div>
					</h4>
					<table id="contentTable" class=" demo-table activity-table">
						<tbody>
							<tr>
								<td data-title="活動代碼:">${activity.actCode}　</td>
								<td data-title="活動名稱:">${activity.actName}　</td>
								<td data-title="活動說明:">${activity.readMeShort}
									<a data-toggle="popover"　id="actMemo_tooltip" data-placement="top" data-content="${activity.readMe}"><i class="fa fa-info-circle"></i></a>
								</td>
								<td data-title="活動期間:">${activity.actSdt}至${activity.actEdt}，活動時數：${activity.hours}小時　</td>
								<td data-title="活動對象:">${activity.target}　</td>
								<td data-title="報名人數:">${activity.limitNumber}　</td>
								<td data-title="承辦單位:">${activity.org}，負責人員：${activity.eName}</td>
								<td data-title="聯絡方式:">E-Mail：${activity.email} 電話:${activity.tell}</td>
								<td data-title="備註事項:">${activity.memoShort}
									<a data-toggle="popover"　id="actMemo_tooltip" data-placement="top" data-content="${activity.memo}"><i class="fa fa-info-circle"></i></a>
								</td>
								<td data-title="附加檔案:">　</td>
							</tr>
						</tbody>
					</table>
				</div>
				<h4 class="page-header">
					<div class="fa fa-files-o">場次列表</div>
				</h4>
				
				<div id="owl-session" class="owl-carousel">
					<c:forEach var="data" items="${sessionList}">
						<div id="001" class="sessionData">
							<div class="row">
								<div class="col-md-3 col-xs-4">場次名稱</div>
								<div class="col-md-9 col-xs-8">${data.sessName}</div>
							</div>
							<div class="row">
								<div class="col-md-3 col-xs-4">場次時間</div>
								<div class="col-md-9 col-xs-7">${data.sessSdt} 至 ${data.sessEdt}</div>
							</div>
							<div class="row">
								<div class="col-md-3 col-xs-4">報名時間</div>
								<div class="col-md-9 col-xs-7">${data.signupSdt} 至  ${data.signupEdt}</div>
							</div>
							<div class="row">
								<div class="col-md-3 col-xs-4">活動地點 </div>
								<div class="col-md-9 col-xs-8">${data.city}</div>
							</div>
							<div class="row">
								<div class="col-md-3 col-xs-4">報名資格 </div>
								<div class="col-md-9 col-xs-8">${data.noticeTo}</div>
							</div>
							<div class="row">
								<div class="col-md-3 col-xs-4">人數限制 </div>
								<div class="col-md-9 col-xs-8">${data.upperLimit}</div>
							</div>
							<div class="row">
								<div class="col-md-3 col-xs-4">剩餘名額  </div>
								<div class="col-md-9 col-xs-8">${data.singUpCount}</div>
							</div>
							<div class="row">
								<div class="col-md-3 col-xs-4"></div>
								<div class="col-md-9 col-xs-8">
									<button onclick="signupAct('${data.sessionUUID}')" class="btn  btn-primary" type="button">我要報名</button>
								</div>
							</div>
						</div>
						</c:forEach>
				</div>

<script type="text/javascript">
	$(document).ready(function() {
		$("#owl-session").owlCarousel({items : 4});
		$('[data-toggle="popover"]').popover();
	});
	
</script>

