<%@ page language="java" contentType="text/html; charset=UTF-8"pageEncoding="UTF-8"%>
<%@ page import="tw.com.lawbroker.model.enumeration.tr.*"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="st" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="sf" uri="http://www.springframework.org/tags/form"%>
<%/*
對應的資料結構  List->List->Map
[[{value=01, key=身份別}, {value=Z00XXX, key=業代}, {value=王大名, key=姓名}, {signupUUID=2016650}]]
*/%>
				<c:forEach items="${signupDataList}" var="dataMap" varStatus="loopStatus">
					<div id="Data_1" class="${loopStatus.index % 2 == 0 ? 'scheDataOdd ' : 'scheDataEven '}">
					<div class="row">
						<div class="col-md-3 col-xs-4">項次</div>
						<div class="col-md-9 col-xs-8">${loopStatus.index+1}</div>
					</div>
					 <c:forEach items="${dataMap}" var="data" varStatus="status">
						<c:choose>
						  <c:when test="${status.last}">
							  <div class="row">
									<div class="col-md-3 col-xs-4"></div>
									<div class="col-md-9 col-xs-8">
										<button onclick="cancelSignup('${data.signupUUID}')" class="btn  btn-danger"type="button">取消報名</button>
									</div>
								</div>
						  </c:when>
						  <c:otherwise>
						  	<div class="row">
								<div class="col-md-3 col-xs-4">${data.key}</div>
								<div class="col-md-9 col-xs-8">${data.value}</div>
							</div>
						  </c:otherwise>
						</c:choose>
					 </c:forEach>
					</div>
				</c:forEach>



