<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ page import="tw.com.lawbroker.model.enumeration.tr.*" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<c:forEach var="data" items="${rows}" >
	<tr>
		<td data-title="照會資訊 ">${data.contactInfo}</td>
		<td data-title="照會內容">${data.contactContent}</td>
		<td data-title="回覆內容">${data.contactReply}</td>
	</tr>
</c:forEach>