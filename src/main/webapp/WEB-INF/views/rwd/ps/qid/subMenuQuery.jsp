<?xml version="1.0" encoding="UTF-8" ?>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<!-- Page Heading -->
<div class="row">
    <div class="col-lg-12">
        <h3 class="page-header  ">
            <div class="fa fa-pencil-square-o">保單資料綜合查詢</div>
        </h3>
        <ol class="breadcrumb">
			<li>
           	    <i class="fa fa-search btn btn-success" onclick="showRow('body1') ">查詢條件</i>
				<i class="breadcrumb2 fa fa-arrow-right"></i><a class="breadcrumb2" href="javascript:showRow('body2')">保單列表</a> 
                <i class="breadcrumb3 fa fa-arrow-right"></i><a class="breadcrumb3" href="javascript:showRow('body3')">保單明細</a> 
			</li>
		</ol>
	</div>
</div>

<!-- /中間要加入的table or form -->
<div id = "subMenuQuery"  class="uiRow row">
	<div class="col-md-12">
		<div class="panel panel-info">
		    <div class="panel-heading">
				<span class="glyphicon glyphicon-search"></span>資料查詢 
		    </div>
		    <div class="panel-body">
				<form id="inputform"  class="col-lg-3 col-md-6 col-sm-8" role="form">
					<div class="form-group" >
						<label for="eno">業代:</label>
				      	<input type='text' class="form-control" id="eno" readOnly value="${_user.ENO}"/>
					</div>
					<div class="form-group">
						<label for="name">保戶姓名:</label>
						<div class=" input-group">
                            <input type="text" class="form-control" id="customerName" placeholder="請輸入保戶姓名">
                            <span class="input-group-btn">
                                <button class="btn btn-default" type="button"><i class="glyphicon glyphicon-user"></i>
                                </button>
                            </span>
                        </div>
						<input type="hidden" id="customerID">
					</div>
					<div class="form-group">
						<label for="pwd">保戶身份證號:</label>
						<input type="text" class="form-control" id="idNo">
					</div>
					<div class="form-group">
						<label for="no">保單號碼:</label>
				        <input type='text' class="form-control" id="insurNo" />
					</div>
				    <input type="reset"  value = "清空"    onclick="$('#customerID' ).val('');" class="btn btn-warning">
					<input type="button"  value = "查詢"   onclick="searchBtn();" class="btn btn-info">
				</form>
			</div>
		</div>
	</div>
</div>

<div id = "subQueryResult"  class="uiRow row" style="display:none" >
	<div class="col-md-12">
		<div class="panel panel-info">
		    <div class="panel-heading">
				<span class="glyphicon glyphicon-search"></span>保單列表
		    </div>
			<div id="subMenuTree"></div>
		</div>
	</div>
</div>

<div id = "queryResult"  class="uiRow row" style="display:none" >
	<div id="myTab"  class="panel-body">
	
		<ul class="nav nav-pills" >
	     	<li><a data-toggle="tab" class=" fa fa-file-text-o" href="#act">保單明細</a></li>
			<li><a data-toggle="tab" class=" fa fa-file-text-o" href="#aceSch">新契約照會</a></li>
			<li><a data-toggle="tab" class=" fa fa-file-text-o" href="#actTran">保服申請</a></li>
			<li><a data-toggle="tab" class=" fa fa-file-text-o" href="#act4">代繳記錄</a></li>
			<li><a data-toggle="tab" class=" fa fa-file-text-o" href="#act5">理賠記錄</a></li>
	   </ul>
	</div>
	
	
	<div class="tab-content">
<!-- 	<div id="owl-tbl" class="owl-carousel"> -->
	   	<div id="act" class="tab-pane fade in active">
	   		<div class="col-lg-12">
	     		<div class="table-responsive activitys-outTable ">
					<%@include file="qidDetail.jsp" %>
	           	</div>
			</div>
		</div>
	  	<div id="aceSch" class="tab-pane fade">
	  	 	<div class="col-lg-12">
	  	 		<div class="table-responsive">
	  	 			<a href="javascript:previewContract()" class="fa fa-search btn  btn-primary" type="button">新契約照會影像檔</a><br><br>
					<iframe id="contractAttachment" name="frameAttachment" frameborder="0" scrolling="no" style="display:none;width: 99%; height: 700px"></iframe>
	  	 			<div id = "qidContract">
		  	 			<table id="contactTable" class="demo-table break-table  table-hover person-table"  >
							<thead>
								<tr>
									<th>照會資訊 </th>
									<th>照會內容 </th>
									<th>回覆內容  </th>
								</tr>
							</thead>
							<tbody id="contactData">
							</tbody>
						</table>
	  	 			</div>
	           	</div>
	       	</div>
		</div>
	
	
		<div id="actTran" class="tab-pane fade">
			<div class="col-lg-12">
				<div class="table-responsive">
					<a href="javascript:previewServiceContract()" class="fa fa-search btn  btn-primary" type="button">保服申請影像檔</a><br><br>
					<iframe id="serviceContractAttachment" name="frameAttachment" frameborder="0" scrolling="no" style="display:none;width: 99%; height: 700px"></iframe>
	  	 			<table id="serviceContentTable" class="demo-table break-table  table-hover person-table"  >
						<thead>
							<tr>
								<th>保服申請</th>
								<th>保服申請項目  </th>
							</tr>
						</thead>
						<tbody id="serviceContentData">
	  		  			</tbody>
					</table>
	            </div>
	        </div>
		</div>
	
	
		<div id="act4" class="tab-pane fade">
			<div class="col-lg-12">
				<div class="table-responsive">
					<%@include file="qidPayment.jsp" %>
				</div>
			</div>
		</div>
	
		<div id="act5" class="tab-pane fade">
			<div class="col-lg-12">
				<div class="table-responsive activitys-outTable">
					<%@include file="qidClaimRecords.jsp" %>
				</div>
			</div>
		</div>
<!-- 	</div>	 -->
	</div>  
</div>

<div id="gotop">top</div>

<form id="QueryStatusForm" listenTo="subMenuQueryForm subMenuResultTree queryResult sessionDataForm">
	<input type="hidden" id="h_insurNo"         name="insurNo" value=""/> 
	<input type="hidden" id="h_customerID"      name="customerID" value=""/> 
	<input type="hidden" id="h_idNo"            name="idNo" value=""/> 
	<input type="hidden" id="h_corp"    		name="corp" value=""/> 
	<input type="hidden" id="h_polNo"     		name="polNo" value=""/> 
	<input type="hidden" id="h_applyNo"     	name="applyNo" value=""/> 
</form>
<!-- /中間要加入的table or form -->


<script type="text/javascript">

$(document).ready(function(){ 
// 	initTree();
	$("#owl-tbl").owlCarousel({items:1});
	$(".breadcrumb2").hide();
	$(".breadcrumb3").hide();
});	  

<%//查詢姓名 autocomplete//%>
$(function() {
   
    $( "#customerName" ).autocomplete({
      minLength: 1,
      source: function(request, response){
    	  $.ajax({
              url: "${_contextPath}/system/customersByLastName",
              dataType: "json",
              method: "post",
              data: {eno:'${_user.ENO}',lastName:request.term},
              success: function(data) {
                response(data.rows);
              }
            });
      },
		select: function( event, ui ) {
			$( "#customerName" ).val(ui.item.CNAME +"("+ getAge(ui.item.BIRTHDAY) +")");
			$( "#customerID" ).val(ui.item.ID);
        	return false;
		}
	}).autocomplete( "instance" )._renderItem = function( ul, item ) {
		return $( "<li>" )
        .append( "<a>" + item.CNAME +"("+ getAge(item.BIRTHDAY) +")"+ "<br/>"+item.ADDR_M+"</a>" )
        
        .appendTo( ul );
    };
  });
	
	
	$(function(){
	    $("#gotop").click(function(){
	        jQuery("html,body").animate({
	            scrollTop:0
	        },1000);
	    });
	    $(window).scroll(function() {
	        if ( $(this).scrollTop() > 300){
	            $('#gotop').fadeIn("fast");
	        } else {
	            $('#gotop').stop().fadeOut("fast");
	        }
	    });
	});
	
	function showRow(body){
		$(".uiRow").hide();
		if(body=='body1'){
			$("#subMenuQuery").show();
			$(".breadcrumb2").hide();
			$(".breadcrumb3").hide();
		}else if(body=='body2'){
			$("#subQueryResult").show();
			$(".breadcrumb2").show();
			$(".breadcrumb3").hide();
		}else{
			$("#queryResult").show();
			$(".breadcrumb2").show();
			$(".breadcrumb3").show();
		}
	}
	
	function getTree() {
		var data1 = '';
		 $.ajax({
		     url: '${_contextPath}/RWPSQID/showClientPolicy',
		     type: 'post',
		     data:{idNo:$('#h_idNo').val(),
		    	   insurNo:$('#h_insurNo').val(),
		    	   customerID:$( "#customerID" ).val()
		    	   },
		     dataType: "json",
		     async:false,
		     success: function (data) {
				 data1=data;
		     }
		 });
		return data1;
	}
	
	function initTree() {
		$('#subMenuTree').treeview({
			expandIcon: 'fa fa-folder-o',
			collapseIcon: 'fa fa-folder-open-o',
			showTags: true,
			levels:3,
			data:getTree(),
			enableLinks:true,
			emptyIcon:'fa fa-file-text-o',
			onNodeSelected: function(event, data) {
				if(data.attributes.selected == 'grand'){
					if(data.state.expanded){
						$('#subMenuTree').treeview('collapseNode', [  data.nodeId, { silent: true, ignoreChildren: false } ]);
					}else{
		    			$('#subMenuTree').treeview('expandNode', [ data.nodeId, { levels: 1, silent: true } ]);
					}
					$('#subMenuTree').treeview('toggleNodeSelected', [ data.nodeId, { silent: true } ]);
				}else{
					$('#h_corp').val(data.attributes.corp);
					$('#h_polNo').val(data.attributes.polno);
					$('#h_applyNo').val(data.attributes.id);
					queryresult();
				}
			}
		});
	}
	
	function searchBtn(){
		$('#h_insurNo').val($("#insurNo" ).val());
		$('#h_idNo').val($( "#idNo" ).val());
		$('#h_customerID').val($( "#customerID" ).val());
		<%//保戶姓名，保戶身份證號至少輸入一個條件 //%>
		if(!$('#h_idNo').val() && !$('#h_customerID').val()){
			_showError("保戶姓名，保戶身份證號至少需輸入一個 !");
			return ;
		}
		
		<%//只輸入保戶姓名 //%>
		if($('#h_customerID').val() && !$('#h_idNo').val() ){
			$('#h_idNo').val($('#h_customerID').val());
			return ;
		}
		
		<%//保戶姓名，保戶身份證號不一樣 //%>
		if($('#h_customerID').val() && $('#h_idNo').val() ){
			if($('#h_customerID').val() != $('#h_idNo').val()) {
				_showError("保戶姓名，保戶身份證號不一樣!");
				return ;
			}
		}
		initTree();
		showRow('body2');
	}
	
	function queryresult(){
		$('#myTab li:eq(0) a').tab('show');
		showRow('body3');
		$("#polAttachment").hide();
		$("#contractAttachment").hide();
		$("#serviceContractAttachment").hide();
		$("#paymentAttachment").hide();
		$("#claimRecordAttachment").hide();
		showPayment();
		showPolicy();
		showContract();
		showServiceContent();
		showClaimRecord();
	}
	
	<%//計算歲數//%>
	function getAge(birthday) {
		if (birthday) {
			var year = Number(birthday.substr(0, 4));
			var month = Number(birthday.substr(4, 2));
			var day = Number(birthday.substr(6, 2));
			var today = new Date();
			var pAge = (today.getFullYear() - year) * 12 + (today.getMonth() - month);

			var pMod = pAge % 12;
			pAge = Math.floor(pAge / 12);

			if ((pMod >= 7) || (pMod == 6 && (today.getUTCDate() - day)) > 0) {
				pAge = pAge + 1;
			}

			return pAge;
		} else
			return "";
	}
	
	<%//保單明細//%>
	function showPolicy(){	
		<%//保單資訊//%>
		$.ajax({
			type: 'POST',
			url: '${_contextPath}/RWPSQID/showPolicy',
			data: {applyNo:$('#h_applyNo').val()},
			success: function(data) {
				debugger;
				$('#polInfo').find('td').eq(0).text(data.ID);
				$('#polInfo').find('td').eq(1).text(data.IDNAME);
				$('#polInfo').find('td').eq(2).text(dateFormate(data.IDBIRTHDAY)+'  '+getAge(data.IDBIRTHDAY)+'歲');
				$('#polInfo').find('td').eq(3).text(data.IDSEX);
				$('#polInfo').find('td').eq(4).text(nullFormat(data.IDTEL_H));
				$('#polInfo').find('td').eq(5).text(nullFormat(data.IDTEL_O));
				$('#polInfo').find('td').eq(6).text(nullFormat(data.IDTEL_MV));
				$('#polInfo').find('td').eq(7).text(data.IDADDR_M);
				$('#polInfo').find('td').eq(8).text(nullFormat(data.IDEMAIL));
				$('#polInfo').find('td').eq(9).text(data.IDADDR_P);
				$('#polInfo').find('td').eq(10).text(data.IDCCORP);
				$('#polInfo').find('td').eq(11).text(data.IDCOCCUP);
				$('#polInfo').find('td').eq(12).text(data.IDJOB_KIND);
				
				$('#payPolInfo').find('td').eq(0).text(data.PAYID);
				$('#payPolInfo').find('td').eq(1).text(data.PAYIDNAME);
				$('#payPolInfo').find('td').eq(2).text(dateFormate(data.PAYIDBIRTHDAY)+'  '+getAge(data.PAYIDBIRTHDAY)+'歲');
				$('#payPolInfo').find('td').eq(3).text(data.PAYIDSEX);
				$('#payPolInfo').find('td').eq(4).text(nullFormat(data.PAYIDTEL_H));
				$('#payPolInfo').find('td').eq(5).text(nullFormat(data.PAYIDTEL_O));
				$('#payPolInfo').find('td').eq(6).text(nullFormat(data.PAYIDTEL_MV));
				$('#payPolInfo').find('td').eq(7).text(data.PAYIDADDR_M);
				$('#payPolInfo').find('td').eq(8).text(nullFormat(data.PAYIDEMAIL));
				$('#payPolInfo').find('td').eq(9).text(data.PAYIDADDR_P);
				$('#payPolInfo').find('td').eq(10).text(data.PAYIDCCORP);
				$('#payPolInfo').find('td').eq(11).text(data.PAYIDCOCCUP);
				$('#payPolInfo').find('td').eq(12).text(data.PAYIDJOB_KIND);
				
				$('#pol1').text(nullFormat(data.CORP_NAME));
				$('#pol2').text(nullFormat(data.POLNO));
				$('#pol3').text(nullFormat(data.STATUS_NAME));
				$('#pol4').text(nullFormat(data.PAY_TYPE_NAME));
				$('#pol5').text(nullFormat(data.TKIND_NAME));
				$('#pol6').text(dateFormate(nullFormat(data.DUE_DT)));
				$('#pol7').text(dateFormate(nullFormat(data.DUE_DT2)));
				$('#pol8').text(nullFormat(data.POL_AGE));
				$('#pol9').text(nullFormat(data.MAXYEAR));
				$('#pol10').text(nullFormat(data.MAXAGE));
				$('#pol11').text(dateFormate(nullFormat(data.POLNO_DT)));
				$('#pol12').text(nullFormat(data.PAY_WAY_NAME));
				$('#pol13').text(dateFormate(nullFormat(data.FROM_DT)));
				$('#pol14').text(dateFormate(nullFormat(data.POLNO_RET_DT)));
				$('#pol15').text(dateFormate(nullFormat(data.PAYID_DT)));
				$('#pol16').text(nullFormat(data.REALNO));
				$('#pol17').text(nullFormat(data.NEW_PROC_NO));
				$('#pol18').text(nullFormat(data.POLNO_NO));
				$('#pol19').text(nullFormat(data.applyNo));
				$('#pol20').text(nullFormat(data.ENO));
				$('#pol21').text(nullFormat(data.ENO2));
				$('#pol22').text(nullFormat(data.BILLNO));
			},
			dataType: 'json',
		});
		
		<%//保單明細/%>
		$('#pol').empty();
		$.ajax({
			type: 'POST',
			url: '${_contextPath}/RWPSQID/showPolicyContent',
			data: {applyNo:$('#h_applyNo').val()},
			success: function(data) {
				$('#pol').append(data);
			},
			dataType: 'html',
		});
	};
	
	<%//新契約照會/%>
	function showContract(){
		$('#contactData').empty();
		$.ajax({
			type: 'POST',
			url: '${_contextPath}/RWPSQID/showNewProc',
			data: {applyNo:$('#h_applyNo').val()},
			success: function(data) {
				$('#contactData').append(data);
			},
			dataType: 'html',
		});
	}
	
	<%//保服申請//%>
	function showServiceContent(){
		$('#serviceContentData').empty();
		$.ajax({
			type: 'POST',
			url: '${_contextPath}/RWPSQID/showServiceContent',
			data: {corp:$('#h_corp').val(),
				  polNo:$('#h_polNo').val()},
			success: function(data) {
				$('#serviceContentData').append(data);
			},
			dataType: 'html',
		});
	}
	
	
	
	<%//代繳紀錄//%>
	function showPayment(){
		<%//首期代繳紀錄//%>
		$.ajax({
			type: 'POST',
			url: '${_contextPath}/RWPSQID/showFirstPayment',
			data: {applyNo:$('#h_applyNo').val()},
			success: function(data) {
				$('#firstPayment').find('td').eq(0).text(nullFormat(data.CASH));
				$('#firstPayment').find('td').eq(1).text(nullFormat(data.TRANS_AMT2));
				$('#firstPayment').find('td').eq(3).text(nullFormat(data.CRAD_AMT));
				$('#firstPayment').find('td').eq(4).text(nullFormat(data.CARD_NO));
				$('#firstPayment').find('td').eq(6).text(nullFormat(data.CARD_EXPIRE));
				$('#firstPayment').find('td').eq(7).text(nullFormat(data.CHECK1));
				$('#firstPayment').find('td').eq(8).text(nullFormat(data.CHK_AMT));
				$('#firstPayment').find('td').eq(9).text(nullFormat(data.CHK_DT));
				$('#firstPayment').find('td').eq(10).text(nullFormat(data.CHK_NO));
				$('#firstPayment').find('td').eq(12).text(nullFormat(data.BANK));
				$('#firstPayment').find('td').eq(13).text(nullFormat(data.TRANS_AMT));
				$('#firstPayment').find('td').eq(14).text(nullFormat(data.TRANS_NO));
				$('#firstPayment').find('td').eq(15).text(nullFormat(data.AMT_TOT));
				$('#firstPayment').find('td').eq(16).text(nullFormat(data.AMT_OVER));
				$('#firstPayment').find('td').eq(17).text(nullFormat(data.PAY_RELN));
				$('#firstPayment').find('td').eq(18).text(nullFormat(data.BILLNAME));
				$('#firstPayment').find('td').eq(19).text(nullFormat(data.BILLID));
			},
			dataType: 'json',
		});
		
		<%//續期代繳紀錄//%>
		$('#renewalPaymentData').empty();
		$.ajax({
			type: 'POST',
			url: '${_contextPath}/RWPSQID/showRenewalPayment',
			data: {corp:$('#h_corp').val(),
				   polNo:$('#h_polNo').val()},
// 			data: {corp:'07',polNo:'B00040362L'},
// 			data: {corp:'15',polNo:'1012156680'},
			success: function(data) {
				$('#renewalPaymentData').append(data);
			},
			dataType: 'html',
		});
	}
	
	<%//理賠紀錄//%>
	function showClaimRecord(){
		$('#claimRecordsData').empty();
		$.ajax({
			type: 'POST',
			url: '${_contextPath}/RWPSQID/showClaimRecords',
			data: {corp:$('#h_corp').val(),
				   polNo:$('#h_polNo').val()},
			success: function(data) {
				$('#claimRecordsData').append(data);
			},
			dataType: 'html',
		});
	}
	
	function polClaimRecord(apply_no,apply_dt,new_proc_no,accident_dt,accident_type_name,accident_note,
			send_org_name,process_eno,process_ename,apply_staff,apply_ename,payment_type_name,chk_no,apply_doc,claim_policy){
		$('#claimRecordsDetailArea').show();
		$('#claimRecordsDetail').find('td').eq(0).text(apply_no);
		$('#claimRecordsDetail').find('td').eq(1).text(apply_dt);
		$('#claimRecordsDetail').find('td').eq(2).text(new_proc_no);
		$('#claimRecordsDetail').find('td').eq(3).text(accident_dt);
		$('#claimRecordsDetail').find('td').eq(4).text(accident_type_name);
		$('#claimRecordsDetail').find('td').eq(5).text(accident_note);
		$('#claimRecordsDetail').find('td').eq(6).text(send_org_name);
		$('#claimRecordsDetail').find('td').eq(7).text(process_eno + " " + process_ename);
		$('#claimRecordsDetail').find('td').eq(8).text(apply_staff + " " + apply_ename);
		$('#claimRecordsDetail').find('td').eq(9).text(payment_type_name);
		$('#claimRecordsDetail').find('td').eq(10).text(apply_doc);
		$('#claimRecordsDetail').find('td').eq(11).text(claim_policy);
	}
	
	function applyNoClaimPolicies(applyNo){
		$('#claimPaymentsData').empty();
		$('#claimPaymentsArea').show();
		$.ajax({
			type: 'POST',
			url: '${_contextPath}/RWPSQID/showClaimPayments',
			data: {applyNo:applyNo},
			success: function(data) {
				$('#claimPaymentsData').append(data);
			},
			dataType: 'html',
		});
	}
	
	function claimContacts(applyNo){
		$('#claimContactsData').empty();
		$('#claimContactArea').show();
		$.ajax({
			type: 'POST',
			url: '${_contextPath}/RWPSQID/showClaimContacts',
			data: {applyNo:applyNo},
			success: function(data) {
				$('#claimContactsData').append(data);
			},
			dataType: 'html',
		});
	}
	
	function nullFormat(data){
		if(data == null){
			return '　';
		}
		return data;
	}
	
	function dateFormate(data){
		if(data != '　'){
			return 	moment(data,"YYYYMMDD").format("YYYY-MM-DD");		
		}else{
			return '　';
		}
	}
	
	function previewPol(){
// 		var data = 'https://lawwebot.lawbroker.com.tw/LawPL/PLPSQID/attachment?typeString=2F01&applyNoString='+$('#h_applyNo').val()+'&procRecDtString= &polnoString='+$('#h_polNo').val();
		var data = '${_contextPath}/RWPSQID/attachment?typeString=2F01&applyNoString='+$('#h_applyNo').val()+'&procRecDtString= &polnoString='+$('#h_polNo').val();
		$("#polAttachment").show();
		$("#polAttachment").attr("src",data);
	}
	
	function previewContract(){
// 		var data = 'https://lawwebot.lawbroker.com.tw/LawPL/PLPSQID/attachment?typeString=7R5A&applyNoString='+$('#h_applyNo').val()+'&procRecDtString= &polnoString='+$('#h_polNo').val();
		var data = '${_contextPath}/RWPSQID/attachment?typeString=7R5A&applyNoString='+$('#h_applyNo').val()+'&procRecDtString= &polnoString='+$('#h_polNo').val();
		$("#contractAttachment").show();
		$("#contractAttachment").attr("src",data);
	}
	
	function previewServiceContract(){
// 		var data = 'https://lawwebot.lawbroker.com.tw/LawPL/PLPSQID/attachment?typeString=&applyNoString=&procRecDtString= &polnoString=';
		var data = '${_contextPath}/RWPSQID/attachment?typeString=&applyNoString=&procRecDtString= &polnoString=';
		$("#serviceContractAttachment").show();
		$("#serviceContractAttachment").attr("src",data);
	}
	
	function previewPayment(){
// 		var data = 'https://lawwebot.lawbroker.com.tw/LawPL/PLPSQID/attachment?typeString=&applyNoString=&procRecDtString= &polnoString=';
		var data = '${_contextPath}/RWPSQID/attachment?typeString=&applyNoString=&procRecDtString= &polnoString=';
		$("#paymentAttachment").show();
		$("#paymentAttachment").attr("src",data);
	}
	
	function previewClaimRecord(applNo){
// 		var data = 'https://lawwebot.lawbroker.com.tw/LawPL/PLPSQID/attachment?typeString=7F03&applyNoString='+applNo+'&procRecDtString= &polnoString='+$('#h_polNo').val();
		var data = '${_contextPath}/RWPSQID/attachment?typeString=7F03&applyNoString='+applNo+'&procRecDtString= &polnoString='+$('#h_polNo').val();
		$("#claimRecordAttachment").show();
		$("#claimRecordAttachment").attr("src",data);
	}
</script>


