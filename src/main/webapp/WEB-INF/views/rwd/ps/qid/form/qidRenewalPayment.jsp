<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ page import="tw.com.lawbroker.model.enumeration.tr.*" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
	<c:forEach var="data" items="${rows}" varStatus="loopStatus">
		<tr>
			<td data-title="應繳日期">${data.PAY_DT}　</td>	
			<td data-title="繳費原因">${data.LOCAL_NAME}　</td>
			<td data-title="續期送金單">${data.BILLNO}　</td>
			<td data-title="繳別">${data.PAY_TYPE_NAME}　</td>
			<td data-title="應繳保費">${data.TOT_INV}　</td>
			<td data-title="匯款/現金金額">${data.CASH}　</td>
			<td data-title="支票明細">
				<c:if test="${data.CHK_AMT != null}">
					1.票據金額:${data.CHECK1}</br>
					2.票面金額:${data.CHK_AMT}</br>
					3.到期日:${data.CHK_DT}</br>
					4.票號:${data.CHK_NO}</br>
					5.帳號:${data.BANK_NO}</br>
					6.付款行庫:${data.BANK}</br>
				</c:if>
			</td>
		</tr>
	</c:forEach>
