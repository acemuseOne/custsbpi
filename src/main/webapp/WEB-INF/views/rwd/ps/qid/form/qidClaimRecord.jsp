<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<c:forEach var="data" items="${rows}" >
	<tr>
		<td data-title="申請案號">${data.APPLY_NO}</td>
		<td data-title="申請日期">${data.APPLY_DT}</td>
		<td data-title="事故類別 ">${data.ACCIDENT_TYPE_NAME}</td>
		<td data-title="事故說明 ">${data.ACCIDENT_NOTE}</td>
		<td data-title="保險金給付方式">${data.PAYMENT_TYPE_NAME}</td>
		<td data-title="理賠申請明細 ">
			<c:if test="${data.APPLY_DT != null}">
				<button type="button" class="btn btn-warning"  onClick = "polClaimRecord('${data.APPLY_NO}','${data.APPLY_DT}',
				'${data.NEW_PROC_NO}','${data.ACCIDENT_DT}','${data.ACCIDENT_TYPE_NAME}','${data.ACCIDENT_NOTE}','${data.SEND_ORG_NAME}',
				'${data.PROCESS_ENO}','${data.PROCESS_ENAME}','${data.APPLY_STAFF}','${data.APPLY_ENAME}','${data.PAYMENT_TYPE_NAME}',
				'${data.CHK_NO}','${data.APPLY_DOC}','${data.CLAIM_POLICY}')">明細資料</button>　
			</c:if>
		</td>
		<td data-title="賠付資料  ">
			<c:if test="${data.APPLY_DT != null}">
				<button type="button" class="btn btn-warning" onClick = "applyNoClaimPolicies('${data.APPLY_NO}')">賠付資料</button>　
			</c:if>	
		</td>
		<td data-title="聯繫資料  ">
			<c:if test="${data.APPLY_DT != null}">
				<button type="button" class="btn btn-warning" onClick = "claimContacts('${data.APPLY_NO}')">聯繫資料</button>　
			</c:if>
		</td>
		<td data-title="理賠記錄  ">
			<c:if test="${data.APPLY_DT != null}">
				<button type="button" onClick="previewClaimRecord('${data.APPLY_NO}')" class="btn btn-primary" >預覽</button>
			</c:if>
		</td>
	</tr>
</c:forEach>