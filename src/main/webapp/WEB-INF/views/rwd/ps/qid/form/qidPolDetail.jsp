<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<c:forEach var="footer" items="${footer}" varStatus="loopStatus">
 	<div style="color:#000000;font-weight:bold;">保單明細(繳款保費總計:${footer.INVEST}   繳款FYC總計:${footer.DFYC}  )</div>
 </c:forEach>
 <table  class="demo-table col-lg-12 break-table table-hover"  > 
	<thead>
		<tr>
			<th>主附約</th>
			<th>險種名稱</th>
			<th>險種代碼</th>
			<th>錠嵂險種代碼</th>
			<th>保額</th>
			<th>幣別代碼 </th>
			<th>幣別名</th>
			<th>保額單位</th>
			<th>外幣保費</th>
			<th>年期</th>
			<th>繳款保費</th>
			<th>繳款FYC</th>
		</tr>
	</thead>
	<tbody>
		<c:forEach var="data" items="${rows}" varStatus="loopStatus">
			<tr>
				<td data-title="主附約  ">${data.ISMASTER}</td>
				<td data-title="險種名稱 ">${data.KIND_NAME}</td>
				<td data-title="險種代碼 ">${data.COKIND}</td>
				<td data-title="錠嵂險種代碼 ">${data.KIND}</td>
				<td data-title="保額">${data.LIFE}</td>
				<td data-title="幣別代碼 ">${data.COIN}</td>
				<td data-title="幣別名">${data.COIN_NAME}</td>
				<td data-title="保額單位">${data.UNIT}</td>
				<td data-title="外幣保費 ">${data.COIN_INV}</td>
				<td data-title="年期 ">${data.MESSAGE_TYPE}</td>
				<td data-title="繳款保費">${data.INVEST}</td>
				<td data-title="繳款FYC">${data.DFYC}</td>
			</tr>
		</c:forEach>
	</tbody>
</table>
