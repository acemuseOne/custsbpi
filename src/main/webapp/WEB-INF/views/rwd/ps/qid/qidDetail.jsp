<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">

<a href="javascript:previewPol()" class="fa fa-search btn  btn-primary" type="button">要保書影像檔</a><br><br>
<iframe id="polAttachment" name="frameAttachment" frameborder="0" scrolling="no" style="display:none;width: 99%; height: 700px"></iframe>
<div id="qidDetail">
	<div style="color:#000000;font-weight:bold;">被保人資料</div>		
	<table id="polInfo" class="demo-table break-table table-hover "  >
		<thead>
			<tr>
				<th>身分證號</th>
				<th>姓名 </th>
				<th>生日</th>
				<th>性別 </th>
				<th>住家電話</th>
				<th>公司電話</th>
				<th>行動電話</th>
				<th>戶籍地址</th>
				<th>電子郵件</th>
				<th>收費地址</th>
				<th>服務機關</th>
				<th>職稱</th>
				<th>工作性質</th>
			</tr>
		</thead>
		<tbody>
			<tr>
				<td data-title="身分證號 "></td>
				<td data-title="姓名"></td>
				<td data-title="生日"></td>
				<td data-title="性別 "></td>
				<td data-title="住家電話"></td>
				<td data-title="公司電話 "></td>
				<td data-title="行動電話"></td>
				<td data-title="戶籍地址"></td>
				<td data-title="電子郵件"></td>
				<td data-title="收費地址"></td>
				<td data-title="服務機關"></td>
				<td data-title="職稱 "></td>
				<td data-title="工作性質 "></td>
			</tr>
		</tbody>
	</table>
	
	<div style="color:#000000;font-weight:bold;">要保人資料</div>							
	<table id="payPolInfo" class="demo-table break-table table-hover "  >
		<thead>
			<tr>
				<th>身分證號</th>
				<th>姓名 </th>
				<th>生日</th>
				<th>性別 </th>
				<th>住家電話</th>
				<th>公司電話</th>
				<th>行動電話</th>
				<th>戶籍地址</th>
				<th>電子郵件</th>
				<th>收費地址</th>
				<th>服務機關</th>
				<th>職稱</th>
				<th>工作性質</th>
			</tr>
		</thead>
		<tbody>
			<tr>
				<td data-title="身分證號 "></td>
				<td data-title="姓名"></td>
				<td data-title="生日"></td>
				<td data-title="性別 "></td>
				<td data-title="住家電話"></td>
				<td data-title="公司電話 "></td>
				<td data-title="行動電話"></td>
				<td data-title="戶籍地址"></td>
				<td data-title="電子郵件"></td>
				<td data-title="收費地址"></td>
				<td data-title="服務機關"></td>
				<td data-title="職稱 "></td>
				<td data-title="工作性質 "></td>
			</tr>
		</tbody>
	</table>
	
	<div style="color:#000000;font-weight:bold;">保單資訊</div>
	<table id="" class="demo-table bow-table" >
		<tbody>
		<tr>
		<td>		
			 <div class="col-sm-3 col-lg-2  col-xs-6"  style="color:#000000;font-weight:bold;">	保險公司  		</div>   <div id = "pol1" class="col-sm-3 col-lg-2  col-xs-6">　</div> 	
			 <div class="col-sm-3 col-lg-2  col-xs-6"  style="color:#000000;font-weight:bold;">	保單號碼  		</div>   <div id = "pol2" class="col-sm-3 col-lg-2  col-xs-6">　</div>  
			 <div class="col-sm-3 col-lg-2  col-xs-6"  style="color:#000000;font-weight:bold;">	保單狀態  		</div>   <div id = "pol3" class="col-sm-3 col-lg-2  col-xs-6">　</div> 
			 <div class="col-sm-3 col-lg-2  col-xs-6"  style="color:#000000;font-weight:bold;">	繳別	    	</div>   <div id = "pol4" class="col-sm-3 col-lg-2  col-xs-6">　</div> 
			 <div class="col-sm-3 col-lg-2  col-xs-6"  style="color:#000000;font-weight:bold;">	險種     		</div>   <div id = "pol5" class="col-sm-3 col-lg-2  col-xs-6">　</div> 
			 <div class="col-sm-3 col-lg-2  col-xs-6"  style="color:#000000;font-weight:bold;">	投保日期  		</div>   <div id = "pol6" class="col-sm-3 col-lg-2  col-xs-6">　</div> 
			 <div class="col-sm-3 col-lg-2  col-xs-6"  style="color:#000000;font-weight:bold;">	核保日期  		</div>   <div id = "pol7" class="col-sm-3 col-lg-2  col-xs-6">　</div> 
			 <div class="col-sm-3 col-lg-2  col-xs-6"  style="color:#000000;font-weight:bold;">	投保年齡  		</div>   <div id = "pol8" class="col-sm-3 col-lg-2  col-xs-6">　</div> 
			 <div class="col-sm-3 col-lg-2  col-xs-6"  style="color:#000000;font-weight:bold;">	年限      		</div>   <div id = "pol9" class="col-sm-3 col-lg-2  col-xs-6">　</div> 
			 <div class="col-sm-3 col-lg-2  col-xs-6"  style="color:#000000;font-weight:bold;">	歲期滿 		</div>   <div id = "pol10" class="col-sm-3 col-lg-2  col-xs-6">　</div> 
			 <div class="col-sm-3 col-lg-2  col-xs-6"  style="color:#000000;font-weight:bold;">	出單日期  		</div>   <div id = "pol11" class="col-sm-3 col-lg-2  col-xs-6">　</div> 
			 <div class="col-sm-3 col-lg-2  col-xs-6"  style="color:#000000;font-weight:bold;">	收費方式 		</div>   <div id = "pol12" class="col-sm-3 col-lg-2  col-xs-6">　</div> 
			 <div class="col-sm-3 col-lg-2  col-xs-6"  style="color:#000000;font-weight:bold;"> 最後計佣日期  	</div>	 <div id = "pol13" class="col-sm-3 col-lg-2  col-xs-6">　</div> 
	         <div class="col-sm-3 col-lg-2  col-xs-6"  style="color:#000000;font-weight:bold;"> 保單簽收遞回日  </div>	 <div id = "pol14" class="col-sm-3 col-lg-2  col-xs-6">　</div> 
	         <div class="col-sm-3 col-lg-2  col-xs-6"  style="color:#000000;font-weight:bold;"> 服務聲明回函日  </div>	 <div id = "pol15" class="col-sm-3 col-lg-2  col-xs-6">　</div> 
	    	 <div class="col-sm-3 col-lg-2  col-xs-6"  style="color:#000000;font-weight:bold;"> 預給保單號   	</div>	 <div id = "pol16" class="col-sm-3 col-lg-2  col-xs-6">　</div> 
			 <div class="col-sm-3 col-lg-2  col-xs-6"  style="color:#000000;font-weight:bold;"> 送金單號   	    </div>   <div id = "pol17" class="col-sm-3 col-lg-2  col-xs-6">　</div> 
		     <div class="col-sm-3 col-lg-2  col-xs-6"  style="color:#000000;font-weight:bold;"> 新契約受理碼   	</div>   <div id = "pol18" class="col-sm-3 col-lg-2  col-xs-6">　</div> 
		     <div class="col-sm-3 col-lg-2  col-xs-6"  style="color:#000000;font-weight:bold;"> 申請案號   		</div>   <div id = "pol19" class="col-sm-3 col-lg-2  col-xs-6">　</div> 
			 <div class="col-sm-3 col-lg-2  col-xs-6"  style="color:#000000;font-weight:bold;"> 經紀人  		</div>   <div id = "pol20" class="col-sm-3 col-lg-2  col-xs-6">　</div>   
			 <div class="col-sm-3 col-lg-2  col-xs-6"  style="color:#000000;font-weight:bold;"> 共件經紀人   	</div>   <div id = "pol21" class="col-sm-3 col-lg-2  col-xs-6">　</div>   
			 <div class="col-sm-3 col-lg-2  col-xs-6"  style="color:#000000;font-weight:bold;"> 保單經手人   	</div>   <div id = "pol22" class="col-sm-3 col-lg-2  col-xs-6">　</div>   
		</td>
		</tr>
		</tbody>
	</table>
	
	<div id="pol">
	</div>
</div>