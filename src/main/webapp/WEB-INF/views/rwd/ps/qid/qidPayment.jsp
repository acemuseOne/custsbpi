<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">

<a href="javascript:previewPayment()" class="fa fa-search btn  btn-primary" type="button">預覽</a><br><br>
<iframe id="paymentAttachment" name="frameAttachment" frameborder="0" scrolling="no" style="display:none;width: 99%; height: 700px"></iframe>
<table id="firstPayment" class="demo-table break-table table-hover">
	<tr><b>首期付款內容</b></tr>
	<thead>
		<tr>
			<th>現金$</th>
			<th>匯款</th>
			<th>保險公司</th>
			<th>信用卡$</th>
			<th>信用卡卡號 </th>
			<th>信用卡卡類別</th>
			<th>信用卡有效期限</th>
			<th>支票$</th>
			<th>票面$</th>
			<th>到期日</th>
			<th>票號</th>
			<th>帳號 </th>
			<th>付款行庫</th>
			<th>轉帳$</th>
			<th>轉帳號</th>
			<th>合計金額$</th>
			<th>溢繳金額$</th>
			<th>與被保人關係</th>
			<th>姓名</th>
			<th>身分證號</th>
		</tr>
	</thead>
	<tbody>
		<tr>
			<td data-title="現金$">　</td>	
			<td data-title="匯款">　</td>
			<td data-title="保險公司">　</td>
			<td data-title="信用卡$"></td>
			<td data-title="信用卡卡號 "></td>
			<td data-title="信用卡卡類別">　</td>
			<td data-title="信用卡有效期限"></td>
			<td data-title="支票$">　</td>
			<td data-title="票面$">　</td>
			<td data-title="到期日">　</td>
			<td data-title="票號">　</td>
			<td data-title="帳號 ">　</td>
			<td data-title="付款行庫">　</td>
			<td data-title="轉帳$">　</td>
			<td data-title="轉帳號">　</td>
			<td data-title="合計金額$"></td>
			<td data-title="溢繳金額$">　</td>
			<td data-title="與被保人關係"></td>
			<td data-title="姓名"></td>
			<td data-title="身分證號"></td>
		</tr>
	</tbody>
</table>

<table id="renewalPayment" class="demo-table break-table table-hover ">
<tr><b>續期代繳記錄</b></tr>
	<thead>
		<tr>
			<th>應繳日期</th>
			<th>繳費原因</th>
			<th>續期送金單</th>
			<th>繳別</th>
			<th>應繳保費</th>
			<th>匯款/現金金額</th>
			<th>支票明細</th>

		</tr>
	</thead>
	<tbody id="renewalPaymentData">
		
	</tbody>
</table>
    