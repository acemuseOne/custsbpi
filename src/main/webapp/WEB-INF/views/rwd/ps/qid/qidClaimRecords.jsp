<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">

<iframe id="claimRecordAttachment" name="frameAttachment" frameborder="0" scrolling="no" style="display:none;width: 99%; height: 700px"></iframe>
<table id="claimRecordsTable" class="demo-table break-table table-hover ">
	<tr><b>理賠記錄列表</b></tr>
	<thead>
		<tr>
			<th>申請案號</th>
			<th>申請日期</th>
			<th>事故類別</th>
			<th>事故說明</th>
			<th>保險金給付方式</th>
			<th>理賠申請明細</th>
			<th>賠付資料</th>
			<th>聯繫資料</th>
			<th>理賠記錄</th>
		</tr>
	</thead>
	<tbody id="claimRecordsData">
		
	</tbody>
</table>

<div id="claimRecordsDetailArea" style="display:none;">
	<div style="color:#000000;font-weight:bold;">理賠申請明細</div>		
	<table id="claimRecordsDetail" class="demo-table break-table table-hover "  >
		<thead>
			<tr>
				<th>申請案號</th>
				<th>申請日期</th>
				<th>新理賠受理碼</th>
				<th>事故日期</th>
				<th>事故類別</th>
				<th>事故說明</th>
				<th>送件單位</th>
				<th>送件人員</th>
				<th>受理人員</th>
				<th>保險金給付方式</th>
				<th>檢送文件</th>
				<th>理賠保單</th>		
			</tr>
		</thead>
		<tbody>
			<tr>
				<td data-title="申請案號"></td>
				<td data-title="申請日期"></td>
				<td data-title="新理賠受理碼"></td>
				<td data-title="事故日期"></td>
				<td data-title="事故類別"></td>
				<td data-title="事故說明 "></td>
				<td data-title="送件單位"></td>
				<td data-title="送件人員"></td>
				<td data-title="受理人員"></td>
				<td data-title="保險金給付方式"></td>
				<td data-title="服務機關"></td>
				<td data-title="理賠保單"></td>
			</tr>
		</tbody>
	</table>
</div>

<div  id="claimPaymentsArea" style="display:none;">
	<div style="color:#000000;font-weight:bold;">賠付資料</div>
	<table id="claimPaymentsTable" class="demo-table break-table table-hover ">
		<thead>
			<tr>
				<th>通知日期</th>
				<th>賠付類別</th>
				<th>賠付金額</th>
				<th>保單號碼 </th>
				<th>檢附文件</th>
				<th>備註</th>
				<th>支票抬頭</th>
				<th>支票面額</th>
				<th>處理單位</th>
				<th>處理人員</th>
			</tr>
		</thead>
		<tbody id="claimPaymentsData">
			
		</tbody>
	</table>
</div>

<div  id="claimContactArea" style="display:none;">
	<div style="color:#000000;font-weight:bold;">聯繫資料</div>
	<table id="claimContactsTable" class="demo-table break-table table-hover ">
	<thead>
		<tr>
			<th>照會日期</th>
			<th>照會內容 </th>
			<th>回覆內容 </th>
		</tr>
	</thead>
	<tbody id="claimContactsData">
		
	</tbody>
</table>
</div>