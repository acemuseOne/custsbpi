<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<c:forEach var="data" items="${rows}" >
	<tr>
		<td data-title="通知日期">${data.CLOSE_DT}</td>
		<td data-title="賠付類別">${data.CLOSE_NAME}</td>
		<td data-title="賠付金額 ">${data.PAYMENT}</td>
		<td data-title="保單號碼">${data.POLNO}</td>
		<td data-title="檢附文件 ">${data.PAYMENT_DOC}</td>
		<td data-title="備註">${data.MAIN_NOTE}</td>
		<td data-title="支票抬頭 ">${data.CHK_NAME}</td>
		<td data-title="支票面額 ">${data.CHK_AMT}</td>
		<td data-title="處理單位 ">${data.CLOSE_ORG_NAME}</td>
		<td data-title="處理人員 ">${data.PROCESS_EMP}</td>
	</tr>
</c:forEach>

