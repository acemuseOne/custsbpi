<?xml version="1.0" encoding="UTF-8" ?>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<!-- Page Heading -->
<div class="row">
    <div class="col-lg-12">
        <h3 class="page-header  ">
            <div class="fa fa-pencil-square-o">訊息快遞管理查詢</div>
        </h3>
         <ol class="breadcrumb">
           <li>
           	   <i class="fa fa-search btn btn-success" onclick="showRow('tab1') ">歷史資料查詢</i>
               <i class="fa fa-arrow-right" onclick="showRow()"></i><a href="javascript:showRow('tab3')">查詢結果</a> 
           </li>
       </ol>
    </div>
</div>

<div id = "queryRow"  class="uiRow row" style="display:none">
	<div class="col-md-12">
		<div class="panel panel-info">
		    <div class="panel-heading">
				<span class="glyphicon glyphicon-search"></span>歷史資料查詢 
		    </div>
		    <div class="panel-body">
				<form role="form" id="subMenuQueryForm" >
					<div class="col-sm-3 col-lg-3" >
						<label for="sendMsg">關鍵字查詢:</label>
				      	<input type='text' class="form-control" id="sendMsg" />
					</div>
					<div class="col-sm-3 col-lg-3">
						<label for="sendStartDate">訊息發送起始日:</label>
						 <div  class="datepicker input-group date " >
						    <input id="sendStartDate"  class="form-control" type="text" readonly />  
				 			<span class="input-group-addon"><i class="glyphicon glyphicon-calendar"></i></span>
						</div>
					</div>
					<div class="col-sm-3 col-lg-3">
						<label for="sendEndDate">訊息發送截止日:</label>
						 <div  class="datepicker input-group date " >
						    <input id="sendEndDate"  class="form-control" type="text" readonly />  
				 			<span class="input-group-addon"><i class="glyphicon glyphicon-calendar"></i></span>
						</div>
					</div>
					<div class="col-sm-3 col-lg-3">
				      	<label for="sendDepart">發送部門:</label>
						<select class="form-control" id="sendDepart"></select>
				    </div>
				    <div class=" col-sm-3 col-lg-3">
				      	<label for="readStatus">訊息讀取狀態:</label>
						<select class="form-control" id="readStatus">
							<option value=""></option>
				   			<option value="01"selected>未讀取</option>
				   			<option value="02" >已讀取</option>
						</select>
				    </div>
					<div class=" col-sm-3 col-lg-3">
				    	<label for="kind">發送平台:</label>
						<select class="form-control" id="kind">
							<option value=""></option>
							<option value="1">電信簡訊</option>
							<option value="2">訊息快遞</option>
						</select>
				    </div>
				    <div class="col-sm-3 col-lg-3">
						<br>
						<input type="reset"  value = "清空"    onclick="" class="btn btn-warning">
						<input type="button"  value = "查詢"   onclick="btnSumint();" class="btn btn-info">
					</div>
				</form>
		    </div>
		</div>
	</div>
</div>

<div id = "result"  class="row">
	<div class="col-md-12">
		<div class="panel panel-yellow">
		    <div class="panel-heading">
		        <span class="glyphicon glyphicon-th-list"></span>查詢結果 <span id ="queryCondition"></span>
		    </div>
		    <div id = "myTab" class="panel-body">
            	<ul class="nav nav-pills">
                	<li class="active"><a data-toggle="tab" class="active fa fa-file-text-o" onClick="$('#h_page').val(1);" href="#resultNotRead">未讀取</a>
                    </li>
                    <li class=""><a data-toggle="tab" class=" fa fa-file-text-o"  onClick="$('#h_page').val(2);" href="#resultIsRead">已讀取(7天內)</a>
                    </li>
                    <li class=""><a data-toggle="tab" class=" fa fa-file-text-o"  onClick="$('#h_page').val(3);" href="#queryResultTotal">查詢結果</a>
                    </li>
                </ul>
				
				<div class="tab-content">
<!-- 				<div id="owl-tbl" class="owl-carousel"> -->
					<div id = "resultNotRead" class=" tab-pane table-responsive active in">
						<table id="queryResultNotRead" class="break-table demo-table"  style="height:auto;" >
							<thead style="border-radius:5px;">
			 					<tr>                 
			 						<th></th>        
			 						<th>訊息分類</th>
			 						<th>訊息內容</th>
			 						<th>訊息讀取</th>
			 						<th>發送平台</th>
			 						<th>發送時間</th>
			 						<th>傳送結果</th>
			 						<th>發送人</th>  
			 					</tr>                
				 			 </thead>                   
							<tbody id = "rowNotRead">
							</tbody>
						</table>
						<div   align="center">
							<div id="notReadData" style="display:none"><span>End</span></div>		
							<div id="notReadload"  class="glyphicon glyphicon-refresh" style="display:none"></div>
						</div>
					</div>
					
					<div id = "resultIsRead" class="tab-pane table-responsive">
						<table id="queryResultIsRead" class="break-table demo-table"  style="height:auto;" >
							<thead style="border-radius:5px;">
			 					<tr>                 
			 						<th></th>        
			 						<th>訊息分類</th>
			 						<th>訊息內容</th>
			 						<th>訊息讀取</th>
			 						<th>發送平台</th>
			 						<th>發送時間</th>
			 						<th>傳送結果</th>
			 						<th>發送人</th>  
			 					</tr>                
				 			 </thead>                   
							<tbody id = "rowIsRead">
							</tbody>
						</table>
						<div    align="center" >
							<div id="isReadData" style="display:none"><span>End</span></div>
							<div id="isReadload" class="glyphicon glyphicon-refresh" style="display:none"></div>
						</div>
					</div>
					
					<div id="queryResultTotal" class="tab-pane table-responsive">
						<table id = "queryResultTable" class="break-table demo-table"  style="height:auto;" >
							<thead style="border-radius:5px;">
			 					<tr>                 
			 						<th></th>        
			 						<th>訊息分類</th>
			 						<th>訊息內容</th>
			 						<th>訊息讀取</th>
			 						<th>發送平台</th>
			 						<th>發送時間</th>
			 						<th>傳送結果</th>
			 						<th>發送人</th>  
			 					</tr>                
				 			 </thead>                   
							<tbody id = "rowQueryResult">
							</tbody>
						</table>
						<div align="center" >
							<div id="queryResultData" style="display:none"><span>End</span></div>
							<div id="queryResultload" class="glyphicon glyphicon-refresh" style="display:none"></div>
						</div>
					</div>
<!-- 				</div> -->
				</div>
			</div>
		</div>
	</div>
</div>

<form id="QueryStatusForm" listenTo="subMenuQueryForm subMenuResultTree queryResult sessionDataForm">
	<input type="hidden" id="h_sendMsg"  		name="sendMsg" 		  value=""/>
	<input type="hidden" id="h_sendStartDate"   name="sendStartDate"  value=""/>
	<input type="hidden" id="h_sendEndDate"   	name="sendEndDate"    value=""/>
	<input type="hidden" id="h_sendDepart"   	name="sendDepart"     value=""/>
	<input type="hidden" id="h_readStatus"    	name="readStatus"     value=""/>
	<input type="hidden" id="h_kind"    		name="kind"   	 	  value=""/>
	<input type="hidden" id="h_page"    		name="page"   	 	  value="1"/>
	<input type="hidden" id="h_notReadPage" 	name="notReadPage"    value="1"/> 
	<input type="hidden" id="h_isReadPage"  	name="isReadPage"     value="1"/> 
	<input type="hidden" id="h_queryResultPage" name="queryResultPage" value="1"/> 
	<input type="hidden" id="h_firstLoad"       name="firstLoad"	  value="1"/> 
</form>
<!-- /中間要加入的table or form -->


<script type="text/javascript">
	$(document).ready(function(){ 
		initDeptCode();
		initDate();
		btnSumint();
		
		setTimeout(function(){
			initScroll();
		}, 200);
	});
	
	function initScroll(){
		  $(window).scroll(function() {
		    	if($(window).scrollTop() + $(window).height() == $(document).height()) {
		    		if($('#h_page').val()==1){
		    			
			    		$('#notReadload').show();
			    		$('#h_notReadPage').val( parseInt($('#h_notReadPage').val())+1);
			    		queryResultNotRead();
			    		$('#notReadload').hide();
			  
		    		}else if($('#h_page').val()==2){
		    			$('#isReadload').show();
			    		$('#h_isReadPage').val( parseInt($('#h_isReadPage').val())+1);
			    		queryResultIsRead();
			    		$('#isReadload').hide();
		    		}else{
		    			$('#queryResultload').show();
			    		$('#h_queryResultPage').val( parseInt($('#h_queryResultPage').val())+1);
			    		queryResultTotal();
			    		$('#queryResultload').hide();
		    		}
		    	}
		  });
	}

	
	function showRow(tab){
		if(tab=='tab1'){
			$("#queryRow").show();
			$("#result").hide();
		}else if(tab=='tab3'){
			$("#queryRow").hide();
			$("#result").show();
		}
	}
	
	function initDate() {
	
		today = moment().format("YYYY-MM-DD"); 
		$('#sendEndDate').val(today);
		lastMonth = moment().subtract('years', 1).format("YYYY-MM-DD"); 
		$('#sendStartDate').val(lastMonth);
	};
	
	function queryResultNotRead(){	
		debugger;
		$.ajax({
			type: 'POST',
			url: '${_contextPath}/RWMSMSG/queryResult',
			data: { sendMsg: $('#h_sendMsg').val(),
					sendStartDate: $('#h_sendStartDate').val(),
					sendEndDate:$('#h_sendEndDate').val(),
					sendDepart:$('#h_sendDepart').val(),
					readStatus:"01",
					kind:$('#h_kind').val(),
					page:$('#h_notReadPage').val()},
			success: function(data) {
				if(!data.trim()){
					$('#notReadData').show();
				}
				$('#queryResultNotRead > tbody').append(data);

				$('[data-toggle="popover"]').popover();  
			},
			dataType: 'html',
		});
	};
	
	function queryResultIsRead(){	
		debugger;
		$.ajax({
			type: 'POST',
			url: '${_contextPath}/RWMSMSG/queryResult',
			data: { sendMsg: $('#h_sendMsg').val(),
					sendStartDate: moment().format("YYYY-MM-DD"),
					sendEndDate:moment().subtract('days', 7).format("YYYY-MM-DD"),
					sendDepart:$('#h_sendDepart').val(),
					readStatus:"02",
					kind:$('#h_kind').val(),
					page:$('#h_isReadPage').val()},
			success: function(data) {
				if(!data.trim()){
					$('#isReadData').show();
				}
				$('#queryResultIsRead > tbody').append(data);
				$('[data-toggle="popover"]').popover();  
			},
			dataType: 'html',
		});
	};
	
	function queryResultTotal(){	
		$.ajax({
			type: 'POST',
			url: '${_contextPath}/RWMSMSG/queryResult',
			data: { sendMsg: $('#h_sendMsg').val(),
					sendStartDate: $('#h_sendStartDate').val(),
					sendEndDate:$('#h_sendEndDate').val(),
					sendDepart:$('#h_sendDepart').val(),
					readStatus:$('#h_readStatus').val(),
					kind:$('#h_kind').val(),
					page:$('#h_queryResultPage').val()},
			success: function(data) {
				if(!data.trim()){
					$('#queryResultData').show();
				}
				debugger;
				$('#queryResultTable > tbody').append(data);
				$('[data-toggle="popover"]').popover();  
			},
			dataType: 'html',
		});
	};
	
	function btnSumint(){
		if($('#h_firstLoad').val()==3){
			$('#myTab li:eq(2) a').tab('show');
		}else{
			$('#h_firstLoad').val(3);
		}
		debugger;
		$('#queryCondition').empty();
		$("#rowNotRead").children().remove();
		$("#rowIsRead").children().remove();
		$("#rowQueryResult").children().remove();
		var queryCondition ='';
		$('#h_sendMsg').val($('#sendMsg').val().trim());
		$('#h_sendStartDate').val($('#sendStartDate').val().trim());
		$('#h_sendEndDate').val($('#sendEndDate').val().trim());
		$('#h_sendDepart').val($('#sendDepart').val());
		$('#h_readStatus').val($('#readStatus').val());
		$('#h_kind').val($('#kind').val());
		$('#h_notReadPage').val(1);
		$('#h_isReadPage').val(1);
		$('#h_queryResultPage').val(1);
		
		if($('#h_sendMsg').val()){
			queryCondition = queryCondition + '關鍵字查詢:' + $('#h_sendMsg').val() + '; ';
		}
		
		if($('#h_sendEndDate').val()){
			queryCondition = queryCondition + '訊息發送起始日:' + $('#h_sendStartDate').val() + '; ';
		}
		if($('#h_sendEndDate').val()){
			queryCondition = queryCondition + '訊息發送截止日:' + $('#h_sendEndDate').val() + '; ';
		}
		if($('#h_sendDepart').val()){
			queryCondition = queryCondition + '發送部門:' + $('#sendDepart :selected').text() + '; ';
		}
		if($('#h_readStatus').val()){
			queryCondition = queryCondition + '訊息讀取狀態:' + $('#readStatus :selected').text() + '; ';
		}
		if($('#h_kind').val()){
			queryCondition = queryCondition + '發送平台:' + $('#kind :selected').text() + '; ';
		}
		$('#queryCondition').append(' ('+queryCondition+')');
		queryResultNotRead();
		queryResultIsRead();
		debugger;
		queryResultTotal();
		showRow('tab3');
	}
	
	function initDeptCode(){
		
		$.ajax({
			type: 'POST',
			url: '${_contextPath}/RWMSMSG/getAdm',
			success: function(result) {
				_makeCombobox("sendDepart", result, "NAME1", "CODE");
			},
			dataType: 'json',
			error: function(jqXHR, textStatus, errorThrown){
				console.info(errorThrown);
			}
		});
		
	}
	
	$(".datepicker").datepicker({ 
	    autoclose: true, 
	    format:'yyyy-mm-dd'
	});
</script>


