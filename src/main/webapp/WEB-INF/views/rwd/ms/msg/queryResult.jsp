<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ page import="tw.com.lawbroker.model.enumeration.tr.*" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
	<c:forEach var="data" items="${rows}" varStatus="loopStatus">
		<tr>
			<td data-title="項次">${loopStatus.index+data.INDEX}</td>
			<td data-title="訊息分類">${data.MESSAGE_TYPE}</td>
			<td data-title="訊息內容">${data.SUB_SENDMSG}
				<a tabindex="0" data-toggle="popover" data-trigger="focus" data-placement="bottom" data-content="${data.SENDMSG}" data-original-title="" title=""><i class="fa fa-info-circle"></i></a>
			</td>                                           
			<td data-title="訊息讀取">${data.ISREAD}</td>
			<td data-title="發送平台">${data.KIND}</td>
			<td data-title="發送時間">${data.CREATEDDATE}</td>
			<td data-title="傳送結果">${data.ISSEND}</td>
			<td data-title="發送人">${data.CREATEDENO}</td>
		</tr>
	</c:forEach>
