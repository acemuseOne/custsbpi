<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="st" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="sf" uri="http://www.springframework.org/tags/form"%>

<script type="text/javascript" src="${pageContext.request.contextPath}/resources/js/jqueryFormat/jquery.format-1.2.min.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/resources/js/common/util/EasyuiUtil.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/resources/js/common/util/StrUtil.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/resources/js/common/util/datetime/DateTimeUtil.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/resources/bundle/jquery-easyui-exts/validatebox-ext.js"></script>

<script type="text/javascript">
	
	$(document).ready(function()
	{		
		_addExportButton(function() {
			if(validate())
			{							
				$('#frmPLCRRCZ').submit();
			}	
		});
		
		$('#frmPLCRRCZ').form({url: '${pageContext.request.contextPath}/PLCRRCZ/export'});				
		
		//報表格式
		$('#selReportFormat').combobox
		({   
			url: '${pageContext.request.contextPath}/PLCRRCZ/getReportFormatList',   
		    valueField:'VALUE',   
		    textField:'ITEM',
		    validType: 'combobox["#selReportFormat"]',
		    
		    onLoadSuccess: function(objData)
		    {
		    	if(objData != null)
		    	{		    		
					$.each(objData, function(iIndex, objField)
					{			
						var strValue = objField.VALUE;
						
						if(strValue == 'pdf')
						{
							$('#selReportFormat').combobox('select', strValue);
						}
					});		    		
		    		
		    	}			    				    				    	
		    }
		});	
		
		//繼續率指標
		$('#cntTarget').combobox
		({  
			valueField: 'VALUE',
			textField: 'ITEM',
			validType: 'combobox["#cntTarget"]',
			data: [{
				VALUE: '13',
				ITEM: '13'
			},{
				VALUE: '25',
				ITEM: '25'
			}]			
		});		
		
		//單位		
		$('#cntOrg').combogrid
		({							
			url:'${pageContext.request.contextPath}/PLCRRCZ/getUnitInfo',
			panelWidth:150,
			fitColumns:true,
			idField:'VALUE',
			textField:'ITEM',
			validType: 'combogrid["#cntOrg"]',
			
			columns:
			[[
				{field:'VALUE',title:'代碼', width:50},
				{field:'ITEM',title:'單位',width:150}
			]],				
			
			onHidePanel:function()
			{												
				var objDataGrid = $(this).combogrid('grid');
				var objSelectedItem = objDataGrid.datagrid('getSelected');					
				
				var strTextValue = $(this).combogrid('getText');
				strTextValue = $.trim(strTextValue);				
				
				if(strTextValue != '' && objSelectedItem != null)
				{
					var strText = StrUtil.bindValueItem(objSelectedItem.VALUE, objSelectedItem.ITEM);
					
					$(this).combobox('setText', strText);
				}				
			},
			
			onShowPanel:function()
			{								
				var strTextValue = $(this).combogrid('getText');
				strTextValue = $.trim(strTextValue);
				
				if(strTextValue == '')
				{
					var objDataGrid = $(this).combogrid('grid');
					objDataGrid.datagrid('clearSelections');
				}
				else
				{
					var objDataGrid = $(this).combogrid('grid');
					var objSelectedItem = objDataGrid.datagrid('getSelected');
					
					if(objSelectedItem != null)
					{
						var strText = StrUtil.bindValueItem(objSelectedItem.VALUE, objSelectedItem.ITEM);						
						var iSearchedIndex = strText.indexOf(strTextValue);
						
						if(iSearchedIndex < 0)
						{
							objDataGrid.datagrid('clearSelections');
						}
					}
				}
			},
			
			onLoadSuccess: function(objData)
			{								
				//To set default value
				if(objData != null)
				{
					if(objData.total != undefined && objData.total > 0)
					{
						var strValue = objData.rows[0].VALUE;						
							
						EasyuiUtil.combogrid.setValue('cntOrg', strValue);						
					}					
				}								
			}
			
		});		
		
		
	});	
	
	function sysExport()
	{		
		if(validate())
		{
			var strOrgItem = EasyuiUtil.combogrid.getSelectedValueByItemField('selOrg');							
			
			$('#hidOrgName').val(strOrgItem);			
			$('#frmPcRsMbt').submit();
		}					
	}
	

	
	function validate()
	{
		var bIsPassed = true;
		
		if($('#frmPLCRRCZ').form('validate'))
		{
			
		}
		else
		{
			bIsPassed = false;
		}
			
		return bIsPassed;
	}
	
</script>

<div>
	<form id="frmPLCRRCZ" name="frmPLCRRCZ" method="post">			
		<div class="easyui-panel" title="報表匯出條件" data-options="collapsible:true" style="width: 1680px;">		
			<table border="0" style="font-size: 12px;">						
				<tr>
					<td style="text-align: right; width: 130px;"><label>報表格式</label></td>
					<td style="text-align: left; width: 175px;">
						<input type="text" id="selReportFormat" name="selReportFormat" data-options="required:true" style="width: 145px;"/>
					</td>
					
					<td style="text-align: right; width: 130px;"><label>繼續率指標</label></td>
					<td style="text-align: left; width: 80px;">	
						<input type="text" id="cntTarget" name="cntTarget" data-options="required:true"  style="width: 70px;"/>
					</td>
					
					<td style="text-align: right; width: 130px;"><label>繼續率起始月份</label></td>
					<td style="text-align: left; width: 290px;">
						<input type="text" id="cntStartDate" name="cntStartDate" maxlength="6" class="easyui-validatebox" data-options="required:true" style="width: 70px"></input>
						<label>【格式為：YYYYMM(201306)】</label>
					</td>
					
					<td style="text-align: right; width: 130px;"><label>繼續率結束月份</label></td>
					<td style="text-align: left; width: 290px;">
						<input type="text" id="cntEndDate" name="cntEndDate" maxlength="6" class="easyui-validatebox" data-options="required:true" style="width: 70px"></input>
						<label>【格式為：YYYYMM(201306)】</label>
					</td>
									
					<td style="text-align: right; width: 130px;"><label>單位</label></td>
					<td style="text-align: left; width: 290px;">
						<input type="text" id="cntOrg" name="cntOrg" data-options="required:true" style="width: 70px"></input>				
					</td>
					
				</tr>
			</table>		
		</div>
	</form>
</div>	