<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<link rel="stylesheet" type="text/css" href="../custom/pl/cr/qry/style.css"></link>

<div id="queryTabs" data-options="fit:true,border:false,plain:true">
	<div id="tabIndiv" title="個人" style="padding: 5px" data-options="href:'indivMenu',iconCls:'indivTab'"></div>
	<div id="tabDept" title="組織" style="padding: 5px" data-options="href:'deptMenu',iconCls:'deptTab'"></div>
</div>

<script type="text/javascript">
	var curTabIndex = 0;

	$(document).ready(function() {
		var tabsId = "queryTabs";
		var queryTabs = $("#" + tabsId);

		_triggerChangeSubMenuItemEvent([ "個人" ]);

		// 加入"全部展開" tool bar
		_addSubMenuTool("icon-expandAll", function() {
			// 根據目前所選 tab 觸發 expand event
			var tab = queryTabs.tabs("getSelected");
			var index = queryTabs.tabs("getTabIndex", tab);

			_triggerEvent("subMenu", index == 0 ? "subMenu:expandIndivMenu" : "subMenu:expandDeptMenu");
		});

		// 加入"全部縮合" tool bar
		_addSubMenuTool("icon-collapseAll", function() {
			// 根據目前所選 tab 觸發 collapse event
			var tab = queryTabs.tabs("getSelected");
			var index = queryTabs.tabs("getTabIndex", tab);

			_triggerEvent("subMenu", index == 0 ? "subMenu:collapseIndivMenu" : "subMenu:collapseDeptMenu");
		});

		// 頁籤 initialization
		queryTabs.tabs({
			onSelect : function(title, index) {
				if (curTabIndex != 0 && index == 0) { // 切換"個人"頁籤
					
					curTabIndex = index; // 要先在此指定 curTabIndex, 因接收 queryTabs::selectXXXMenu 的對象會判斷目前頁籤 
					_triggerChangeSubMenuItemEvent([ "個人" ]);
					_triggerEvent("queryTabs", "queryTabs:selectIndivTab");
					_hideMenu();
					_showOrgMenu();
				} else if (curTabIndex != 1 && index == 1) { // 切換"組織"頁籤
					
					curTabIndex = index; // 要先在此指定 curTabIndex, 因接收 queryTabs::selectXXXMenu 的對象會判斷目前頁籤
					_triggerChangeSubMenuItemEvent([ "組織" ]);
					_triggerEvent("queryTabs", "queryTabs:selectDeptTab");
					_hideMenu();
					_hideOrgMenu();
				}
			}
		});

		if (parseInt("${_user.RANK}") < 4 && "${_user.IS_G_STAFF}" != "true") {
			queryTabs.tabs("disableTab", 1);
		}
	});
</script>