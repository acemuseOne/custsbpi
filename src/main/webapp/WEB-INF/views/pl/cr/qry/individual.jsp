<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="st" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="sf" uri="http://www.springframework.org/tags/form"%>

<script type="text/javascript" src="${pageContext.request.contextPath}/resources/js/jqueryFormat/jquery.format-1.2.min.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/resources/js/common/util/EasyuiUtil.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/resources/js/common/util/StrUtil.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/resources/js/common/util/datetime/DateTimeUtil.js"></script>

<!-- 未有跨年情形，通常顯示此組表格 -->
<!-- 主表格 -->
<table id="persistencyRelActProcess1-1" listenTo="orgMenu"></table>
<div style="height: 2px; font-size: 2px"></div>
<!-- 子表格 -->
<div id="persistencyRelActProcess1-2"></div>
<span style="color: red; font-weight: bold"> ※上述資料為截至目前已發佣完成列入計算之繼續率，最終結果仍以 1/1 ~ 12/31 的總結算為準。 </span>

<div style="height: 10px; font-size: 10px"></div>

<!-- 有跨年情形，會顯示此組表格 -->
<!-- 主表格 -->
<!-- <div id="persistencyRelActProcess2-1"></div>
<div style="height: 2px; font-size: 2px"></div> -->
<!-- 子表格 -->
<!-- <div id="persistencyRelActProcess2-2"></div> -->

<script type="text/javascript">
	/*
	 * 繼續率相關活動達成進度，共分四個表格，兩個一組完成
	 */

	$(document).ready(function() {

		buildTables();

		// 接收"組織層級"點選人員事件
		/*$('#persistencyRelActProcess1-1').on("orgMenu:select", function(event) {
			buildTables();
		});*/

	});

	function buildTables() {
		// 宣告傳參
		var postData = {};
		
		// 取得indivMenu.jsp Tree 上所點擊的node資訊
		var node = $('#persistencyTree').tree("getSelected");
		postData.year = node.attributes.year;
		
		var employee = _getSelectedEmployee();
		var eno = "";
		var ename = "";

		if (employee) {
			eno = employee.ENO;
			ename = employee.ENAME;

			postData.eno = eno;
			postData.ename = ename;

			relActProcessHeadGrid('persistencyRelActProcess1-1', "relActProcess", loadFilterFun, postData);
			relActProcessBodyGrid('persistencyRelActProcess1-2');
		}
	}

	/**
	 * 處理每組主表格的函式
	 *
	 * @param tagIdStr 只給id名，不加'#' (字串)
	 * @param urlStr 資料來源網址 (字串)
	 * @param loadFilterFun 過濾函式 (函式)
	 * @param params 查詢參數集合 (物件)
	 */
	function relActProcessHeadGrid(tagIdStr, urlStr, loadFilterFun, params) {

		var tagId = '#' + tagIdStr;

		$(tagId).datagrid({
			border : true,
			width : 805,
			//url : "datagrid_data3.json",
			url : urlStr,
			queryParams : params,
			selectOnCheck : false,
			checkOnSelect : false,
			singleSelect : true,
			fitColumns : false,
			maximized : false,
			remoteSort : false,
			showFooter : false,
			nowrap : false,
			columns : [ [ {
				field : "rangefrom",
				title : "計算起月",
				sortable : false,
				width : 90,
				halign : "center",
				align : "center"
			}, {
				field : "rangeto",
				title : "計算迄月",
				sortable : false,
				width : 80,
				halign : "center",
				align : "center"
			}, {
				field : "eno",
				title : "經紀人業代",
				sortable : false,
				width : 100,
				halign : "center",
				align : "center"
			}, {
				field : "ename",
				title : "經紀人姓名",
				sortable : false,
				width : 85,
				halign : "center",
				align : "center"
			}, {
				field : "13month",
				title : "13個月繼續率",
				sortable : false,
				width : 110,
				halign : "center",
				align : "center",
				formatter : function(value, row, index) {
					return persistencyFormatter(value);
				}
			}, {
				field : "25month",
				title : "25個月繼續率",
				sortable : false,
				width : 110,
				halign : "center",
				align : "center",
				formatter : function(value, row, index) {
					return persistencyFormatter(value);
				}
			}, {
				field : "37month",
				title : "37個月繼續率",
				sortable : false,
				width : 110,
				halign : "center",
				align : "center",
				formatter : function(value, row, index) {
					return persistencyFormatter(value);
				}
			}, {
				field : "49month",
				title : "49個月繼續率",
				sortable : false,
				width : 110,
				halign : "center",
				align : "center",
				formatter : function(value, row, index) {
					return persistencyFormatter(value);
				}
			} ] ], // 以Filter的方式，將資料丟向子表格
			loadFilter : loadFilterFun
		});
	} // End of relActProcessHeadGrid()

	/**
	 * 處理每組子表格的函式
	 *
	 * @param tagIdStr 只給id名，不加'#' (字串)
	 */
	function relActProcessBodyGrid(tagIdStr) {

		var tagId = '#' + tagIdStr;

		// 加入類似 datagrid header的sytle
		var styleStr = 'background-color: #efefef;' + 'background: -webkit-linear-gradient(top,#F9F9F9 0,#efefef 100%);' + 'background: -moz-linear-gradient(top,#F9F9F9 0,#efefef 100%);'
				+ 'background: -o-linear-gradient(top,#F9F9F9 0,#efefef 100%);' + 'background: linear-gradient(to bottom,#F9F9F9 0,#efefef 100%);' + 'background-repeat: repeat-x;'
				+ 'filter: progid:DXImageTransform.Microsoft.gradient(startColorstr=#F9F9F9,endColorstr=#efefef,GradientType=0);';

		var comparisonStyle = "background-color: #eaf2ff";
		var blankStyle = "background-color: white";

		$(tagId).datagrid({
			border : true,
			width : 805,
			//url : 'p_down.json',
			selectOnCheck : false,
			checkOnSelect : false,
			singleSelect : true,
			fitColumns : false,
			maximized : false,
			remoteSort : false,
			showFooter : false,
			nowrap : false,
			columns : [ [ {
				field : "point",
				title : "評選指標",
				sortable : false,
				width : 90,
				halign : "center",
				align : "center",
				styler : function(value, row, index) {
					return styleStr;
				}
			}, {
				field : "daterange",
				title : "評選時間",
				sortable : false,
				width : 80,
				halign : "center",
				align : "center",
				styler : function(value, row, index) {
					return styleStr;
				}
			}, {
				field : "standard",
				title : "評選標準",
				sortable : false,
				width : 100,
				halign : "center",
				align : "center",
				styler : function(value, row, index) {
					return styleStr;
				}
			}, {
				field : "document",
				title : "辦法公文",
				sortable : false,
				width : 85,
				halign : "center",
				align : "center",
				styler : function(value, row, index) {
					return styleStr;
				}
			}, {
				field : "13monthgoal",
				title : "13個月繼續率達標",
				sortable : false,
				width : 110,
				halign : "center",
				align : "center",
				styler : function(value, row, index) {
					if (value != "")
						return comparisonStyle;
					else
						return blankStyle;
				}
			}, {
				field : "25monthgoal",
				title : "25個月繼續率達標",
				sortable : false,
				width : 110,
				halign : "center",
				align : "center",
				styler : function(value, row, index) {
					if (value != "")
						return comparisonStyle;
					else
						return blankStyle;
				}
			}, {
				field : "37monthgoal",
				title : "37個月繼續率達標",
				sortable : false,
				width : 110,
				halign : "center",
				align : "center",
				styler : function(value, row, index) {
					if (value != "")
						return comparisonStyle;
					else
						return blankStyle;
				}
			}, {
				field : "49monthgoal",
				title : "49個月繼續率達標",
				sortable : false,
				width : 110,
				halign : "center",
				align : "center",
				styler : function(value, row, index) {
					if (value != "")
						return comparisonStyle;
					else
						return blankStyle;
				}
			} ] ]
		});
	} // End of relActProcessBodyGrid()

	/**
	 * 處理要丟給子表格資料的函式，以JSON表示及處理
	 *
	 * @param headGridData 主表格的資料串
	 */
	function getBodyGridData(headGridData) {

		var headData = $(headGridData.rows).get(0);

		var bodyData = {
			"total" : "5",
			"rows" : [ {
				"point" : "金續獎",
				"daterange" : "nowYY/01/01<br/>｜<br/>nowYY/12/31<br/>",
				"standard" : "13 個月 >= 95%<br/>25 個月 >= 90%<br/>37 個月 >= 80%<br/>49 個月 >= 70%<br/>",
				"document" : "(100) 嵂業字<br/>第00141號文",
				"13monthgoal" : $.isNumeric(headData['13month']) && headData['13month'] >= 0 ? comparePersistency(headData['13month'], 0.95) : "─",
				"25monthgoal" : $.isNumeric(headData['25month']) && headData['25month'] >= 0 ? comparePersistency(headData['25month'], 0.90) : "─",
				"37monthgoal" : $.isNumeric(headData['37month']) && headData['37month'] >= 0 ? comparePersistency(headData['37month'], 0.80) : "─",
				"49monthgoal" : $.isNumeric(headData['49month']) && headData['49month'] >= 0 ? comparePersistency(headData['49month'], 0.70) : "─"
			}, {
				"point" : "服務品質獎",
				"daterange" : "nowYY/01/01<br/>｜<br/>nowYY/12/31<br/>",
				"standard" : "13 個月 >= 88%<br/>25 個月 >= 85%<br/>",
				"document" : "(102) 嵂業字<br/>第00035號文",
				"13monthgoal" : $.isNumeric(headData['13month']) && headData['13month'] >= 0 ? comparePersistency(headData['13month'], 0.88) : "─",
				"25monthgoal" : $.isNumeric(headData['25month']) && headData['25month'] >= 0 ? comparePersistency(headData['25month'], 0.85) : "─",
				"37monthgoal" : "",
				"49monthgoal" : ""
			}, {
				"point" : "執業獎金",
				"daterange" : "nowYY/01/01<br/>｜<br/>nowYY/12/31<br/>",
				"standard" : "13 個月 >= 60%",
				"document" : "(102) 嵂業字<br/>第00190號文",
				"13monthgoal" : $.isNumeric(headData['13month']) && headData['13month'] >= 0 ? comparePersistency(headData['13month'], 0.60) : "─",
				"25monthgoal" : "",
				"37monthgoal" : "",
				"49monthgoal" : ""
			}, {
				"point" : "承接資格",
				"daterange" : "任職期間",
				"standard" : "13 個月 >= 75%<br/>25 個月 >= 70%<br/>",
				"document" : "(102) 嵂業字<br/>第00193號文",
				"13monthgoal" : $.isNumeric(headData['13month']) && headData['13month'] >= 0 ? comparePersistency(headData['13month'], 0.75) : "─",
				"25monthgoal" : $.isNumeric(headData['25month']) && headData['25month'] >= 0 ? comparePersistency(headData['25month'], 0.70) : "─",
				"37monthgoal" : "",
				"49monthgoal" : ""
			}, {
				"point" : "推薦辦法",
				"daterange" : "nextYY/01/01<br/>｜<br/>nextYY/12/31<br/>",
				"standard" : "13 個月 >= 85%<br/>",
				"document" : "請洽單位<br/>人委或行政<br/>",
				"13monthgoal" : $.isNumeric(headData['13month']) && headData['13month'] >= 0 ? comparePersistency(headData['13month'], 0.85) : "─",
				"25monthgoal" : "",
				"37monthgoal" : "",
				"49monthgoal" : ""
			} ]
		};

		if (headGridData != null) {
			var curYear = headData['fromYear'];

			// 推算時記得要由西元年轉為民國年
			var nowYear = curYear;
			nowYear = nowYear - 1911;
			var nextYear = curYear + 2;
			nextYear = nextYear - 1911;

			// 進行將識別符代換為民國年，今年跟明年要分開做，不然會發生取代不成功情況
			bodyDataArr = bodyData.rows;
			for ( var i = 0; i < 3; i++) {
				var lastDateRange = $(bodyDataArr).get(i).daterange;
				$(bodyDataArr).get(i).daterange = lastDateRange.replace(/nowYY/g, nowYear);
			};

			var nextDateRange = $(bodyDataArr).get(4).daterange;
			$(bodyDataArr).get(4).daterange = nextDateRange.replace(/nextYY/g, nextYear);

		}

		return bodyData;
	} // End of getBodyGridData()

	/**
	 * 給主表格用的過濾函式，主要用於資料分離，並決定產生第二組表格
	 *
	 * (鎮注釋掉的邏輯部份，是計算全範圍繼續率邏輯[會依需求產生跨年記錄]，
	 * 需要時可再加回。)
	 *
	 * @param data 主表格內部的資料串
	 */
	function loadFilterFun(data) {
		var splitData = data;

		// 第一筆資料，給第一組表格
		splitData = {
			total : 1,
			rows : [ $(data).get(0) ]
		};

		// 產生第一組的子表格
		$('#persistencyRelActProcess1-2').datagrid('loadData', getBodyGridData(splitData));

		/*if ($(data).length > 1) {
			// 有超過二筆資料，subData就是第二筆，給第二組表格使用
			var subData = {
				total : 1,
				rows : [ $(data).get(1) ]
			};

			// 第二組表格預先建立，並不使用過濾函式，傳入undefined
			relActProcessHeadGrid('persistencyRelActProcess2-1', '', undefined);
			relActProcessBodyGrid('persistencyRelActProcess2-2');

			// 使用 EasyUI datagrid 的 loadData 函式，向第二組表格傳入資料
			if (subData != null) {
				$('#persistencyRelActProcess2-1').datagrid('loadData', subData);
				$('#persistencyRelActProcess2-2').datagrid('loadData', getBodyGridData(subData));
			}
		}*/

		return splitData;
	}

	// 繼續率格式化
	function persistencyFormatter(persistency) {
		return !$.isNumeric(persistency) || persistency < 0 ? "-" : ($.format.number(persistency * 100, "##.00") + "%");
	}

	// 達標/不達標 icon
	function comparePersistency(pcy, target) {
		var icon = pcy >= target ? "smile.png" : "cry.png";

		return "<div style='margin:auto;width:32px;height:32px;background: url(../custom/pl/cr/qry/images/" + icon + ") no-repeat center;'></div>";
	}
</script>