<?xml version="1.0" encoding="UTF-8" ?>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>

<script type="text/javascript" src="${_contextPath}/resources/bundle/jquery-easyui-exts/datagrid-detailview.js"></script>
<script type="text/javascript" src="${_contextPath}/resources/bundle/jquery-easyui-exts/datagrid-filter.js"></script>

<input id="cc" name="dept">
<input id="customers">
<a id="dd" href="#" class="easyui-linkbutton" iconCls="icon-fillForm" data-options="plain:true"></a>

<table id="dgs" style="width: 650px; height: 250px; display:none " title="DataGrid - SubGrid" singleSelect="true" fitColumns="false">
	<thead>
		<tr>
			<th field="chk" width="30" data-options="checkbox:true"></th>
			<th field="itemid" width="80">Item ID</th>
			<th field="productid" width="100">Product ID</th>
			<th field="listprice" align="right" width="80">List Price</th>
			<th field="unitcost" align="right" width="80">Unit Cost</th>
			<th field="attr1" width="120">Attribute</th>
			<th field="status" width="60" align="center">Status</th>
			<th field="testA" width="50">test A</th>
		</tr>
	</thead>
</table>

<div id="tb" style="padding: 5px; height: auto">
	<div style="margin-bottom: 5px">
		<a href="#" class="easyui-linkbutton" iconCls="icon-add" plain="true"></a> <a href="#" class="easyui-linkbutton" iconCls="icon-edit" plain="true"></a> <a href="#" class="easyui-linkbutton"
			iconCls="icon-save" plain="true"></a> <a href="#" class="easyui-linkbutton" iconCls="icon-cut" plain="true"></a> <a href="#" class="easyui-linkbutton" iconCls="icon-remove" plain="true"></a>
	</div>
	<div>
		Date From: <input class="easyui-datebox" style="width: 80px"> To: <input class="easyui-datebox" style="width: 80px"> Language: <select class="easyui-combobox" panelHeight="auto"
			style="width: 100px">
			<option value="java">Java</option>
			<option value="c">C</option>
			<option value="basic">Basic</option>
			<option value="perl">Perl</option>
			<option value="python">Python</option>
		</select> <a href="#" class="easyui-linkbutton" iconCls="icon-search" onclick="searchForCombo();">Search</a>
	</div>
</div>

<script type="text/javascript">
	$(function() {
		$('#dd').tooltip({
			position: 'right',
			content : $('<div></div>'),
			showEvent : 'click',
			onUpdate : function(content) {

				content.panel({
					border: false,
					width : 274,
					height: 300,
					href: "${_contextPath}/customerChooser"
				});
			},
			onShow : function() {
				var t = $(this);
				t.tooltip('tip').unbind().bind('mouseenter', function() {
					t.tooltip('show');
				}).bind('mouseleave', function() {
					//t.tooltip('hide');
				});
			}
		});

		$('#dg').datagrid({
			//view : detailview,
			selectOnCheck : true,
			checkOnSelect : true,
			singleSelect : false,
			checkbox : true,

			detailFormatter : function(index, row) {
				return '<div style="padding:2px"><table class="ddv"></table></div>';
			},

			toolbar : '#tb',

			onExpandRow : function(index, row) {
				var ddv = $(this).datagrid('getRowDetail', index).find('table.ddv');
				ddv.datagrid({
					//url:'http://www.jeasyui.com/demo/main/datagrid22_getdetail.php?itemid='+row.itemid,                        
					//url: 'datagrid_data2.json',
					fitColumns : true,
					singleSelect : true,
					rownumbers : true,
					loadMsg : '',
					height : 'auto',
					columns : [ [ {
						field : 'orderid',
						title : 'Order ID',
						width : 200
					}, {
						field : 'quantity',
						title : 'Quantity',
						width : 100,
						align : 'right'
					}, {
						field : 'unitprice',
						title : 'Unit Price',
						width : 100,
						align : 'right'
					} ] ],
					onResize : function() {
						$('#dg').datagrid('fixDetailRowHeight', index);
					},
					onLoadSuccess : function() {
						setTimeout(function() {
							$('#dg').datagrid('fixDetailRowHeight', index);
						}, 0);
					}
				});

				$('#dg').datagrid('fixDetailRowHeight', index);

				var objData2 = {
					"total" : 1,
					"rows" : [ {
						"orderid" : "1004",
						"quantity" : 2,
						"unitprice" : 20
					} ]
				};

				ddv.datagrid('loadData', objData2);

			}
		});

		$('#dg').datagrid("enableFilter");

		var objData1 = {
			"total" : 28,
			"rows" : [ {
				"productid" : "FI-SW-01",
				"productname" : "Koi",
				"unitcost" : 10.00,
				"status" : "P",
				"listprice" : 36.50,
				"attr1" : "Large",
				"itemid" : "EST-1"
			}, {
				"productid" : "K9-DL-01",
				"productname" : "Dalmation",
				"unitcost" : 12.00,
				"status" : "P",
				"listprice" : 18.50,
				"attr1" : "Spotted Adult Female",
				"itemid" : "EST-10"
			}, {
				"productid" : "RP-SN-01",
				"productname" : "Rattlesnake",
				"unitcost" : 12.00,
				"status" : "P",
				"listprice" : 38.50,
				"attr1" : "Venomless",
				"itemid" : "EST-11"
			}, {
				"productid" : "RP-SN-01",
				"productname" : "Rattlesnake",
				"unitcost" : 12.00,
				"status" : "P",
				"listprice" : 26.50,
				"attr1" : "Rattleless",
				"itemid" : "EST-12"
			}, {
				"productid" : "RP-LI-02",
				"productname" : "Iguana",
				"unitcost" : 12.00,
				"status" : "P",
				"listprice" : 35.50,
				"attr1" : "Green Adult",
				"itemid" : "EST-13"
			}, {
				"productid" : "FL-DSH-01",
				"productname" : "Manx",
				"unitcost" : 12.00,
				"status" : "P",
				"listprice" : 158.50,
				"attr1" : "Tailless",
				"itemid" : "EST-14"
			}, {
				"productid" : "FL-DSH-01",
				"productname" : "Manx",
				"unitcost" : 12.00,
				"status" : "P",
				"listprice" : 83.50,
				"attr1" : "With tail",
				"itemid" : "EST-15"
			}, {
				"productid" : "FL-DLH-02",
				"productname" : "Persian",
				"unitcost" : 12.00,
				"status" : "P",
				"listprice" : 23.50,
				"attr1" : "Adult Female",
				"itemid" : "EST-16"
			}, {
				"productid" : "FL-DLH-02",
				"productname" : "Persian",
				"unitcost" : 12.00,
				"status" : "P",
				"listprice" : 89.50,
				"attr1" : "Adult Male",
				"itemid" : "EST-17"
			}, {
				"productid" : "AV-CB-01",
				"productname" : "Amazon Parrot",
				"unitcost" : 92.00,
				"status" : "P",
				"listprice" : 63.50,
				"attr1" : "Adult Male",
				"itemid" : "EST-18"
			} ]
		};

		$('#dg').datagrid('loadData', objData1);

		$('#cc').combogrid({
			required : true,
			width : 200,
			panelWidth : 600,
			panelHeight : 200,
			url : "result",
			toolbar : "#tb",
			idField : 'POLNO',
			textField : 'VALUE',
			multiple : true,
			pagination : false,
			pageSize : 10,
			pageList : [ 10, 25, 50 ],
			//url : 'datagrid_data.json',
			onBeforeLoad : function(param) {
				param = {
					year : 2012,
					type : 13,
					fromMonth : 1,
					toMonth : 12,
					eno : 'A00196',
					pcyAlgorithm : "GOOD"
				};

				param.year = 2012;
			},
			loadFilter : function(data) {
				if (data.total > 0) {
					for (var i = 0; i < data.rows.length; i++) {
						var row = data.rows[i];
						row.VALUE = row.CONAME + "_" + row.POLNO;
					}
				}

				return data;
			},
			columns : [ [ {
				field : "test",
				checkbox : true
			}, {
				field : "POLNO",
				title : "保單號碼",
				sortable : true,
				width : 90,
				halign : "center",
				align : "right",
				formatter : function(value, row, index) {
					if (value != undefined) {
						return '<a href="#" title="開啟綜合保單內容明細表" onclick="openPolicyDetailWindow(\'' + value + '\',\'' + row.APPLY_NO + '\');return false;" >' + value + '</a>';
					} else {
						return value;
					}
				}
			}, {
				field : "STATUSNAME",
				title : "保單狀態",
				sortable : true,
				width : 80,
				halign : "center",
				align : "center"
			}, {
				field : "CONAME",
				title : "保險公司",
				width : 70,
				halign : "center",
				align : "center",
				resizable : true,
				sortable : true
			}, {
				field : "VALUE",
				title : "VALUE",
				hidden : true
			} ] ]
		});

		$('#cc').combogrid('validate');
		var v = $('#cc').combogrid('isValid');
	});

	function searchForCombo() {
		var data = {
			total : 2,
			rows : [ {
				code : "L1",
				name : "Java"
			}, {
				code : "L2",
				name : "C++"
			} ]
		};

		var datagrid = $("#cc").combogrid("grid");

		datagrid.datagrid("load", {
			year : 2012,
			type : 13,
			fromMonth : 1,
			toMonth : 12,
			eno : 'A00196',
			pcyAlgorithm : "GOOD"
		});
	}
</script>