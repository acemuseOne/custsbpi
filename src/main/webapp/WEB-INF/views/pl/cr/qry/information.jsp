<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="st" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="sf" uri="http://www.springframework.org/tags/form"%>

<script type="text/javascript" src="${pageContext.request.contextPath}/resources/js/jqueryFormat/jquery.format-1.2.min.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/resources/js/common/util/EasyuiUtil.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/resources/js/common/util/StrUtil.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/resources/js/common/util/datetime/DateTimeUtil.js"></script>

<table id="persistencyInfo" listenTo="subMenu"></table>

<script type="text/javascript">
	$(document).ready(function() {
		$("#persistencyInfo").datagrid({
			url : "${_contextPath}/PLCRQRY/persistencyInfo",
			maximized : true,
			fitColumns : false,
			border : false,

			onBeforeLoad : function(param) {
				// 尚未結算月份或區間
				if (param.isAccounted != undefined && !param.isAccounted) {
					$(this).datagrid("loadData", [ {
						_pGOOD : param.persistency,
						_pSTD : param.persistency,
						_pSTDN1Y : param.persistency
					} ]);

					return false;
				}
			},

			columns : [ [ {
				field : "_pGOOD",
				title : "擇優",
				halign : "center",
				align : "center",
				width : 120,

				formatter : function(value, row, index) {
					return "<span style='color:blue;font-weight:bold'>" + value + "</span>";
				}
			}, {
				field : "_pSTD",
				title : "含承接一年內保單",
				halign : "center",
				align : "center",
				width : 120,

				formatter : function(value, row, index) {
					if (row.pSTDN1Y != -1 && row.pSTD > row.pSTDN1Y) // 排除承接一年內無保單情況下
						return highlightCell(value);

					return value;
				}
			}, {
				field : "_pSTDN1Y",
				title : "不含承接一年內保單",
				halign : "center",
				align : "center",
				width : 120,

				formatter : function(value, row, index) {
					if (row.pSTD != -1 && (row.pSTDN1Y == -1 || row.pSTDN1Y > row.pSTD))
						return highlightCell(value);

					return value;
				}
			} ] ]
		}).on("subMenu:select", function(event, node) {
			var level = node.attributes.level;

			if (typeof (node.attributes.persistency) == "undefined") {
				// 清空內容
				$(this).datagrid("loadData", {
					rows : []
				});
			} else {
				var from = (level == 2 ? node.attributes.from : node.attributes.month);
				var to = (level == 2 ? node.attributes.to : node.attributes.month);

				$(this).datagrid({
					queryParams : {
						year : node.attributes.year,
						type : node.attributes.type,
						fromMonth : from,
						toMonth : to,
						eno : node.eno,
						persistency : node.attributes.persistency,

						// 是否已結算
						isAccounted : node.attributes.isAccounted
					}
				});
			}
		});
	});

	// 擇優 highlight 樣式
	function highlightCell(value) {
		return "<span style='background:url(../custom/pl/cr/qry/images/award_16x16.png) no-repeat;padding-left:18px'>" + value + "</span>";
	}
</script>