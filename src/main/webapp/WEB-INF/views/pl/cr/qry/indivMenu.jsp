<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>

<%-- 繼續率「個人」查詢選項 --%>

<!-- 今年至目前 & 下半年繼續率 -->
<ul id="persistencyTree" listenTo="orgMenu subMenu queryTabs pcyRecordTable"></ul>

<script type="text/javascript">
	var nodeId = null; // 記錄目前所選擇 node id, 供 tree refresh 後自動選取該 node

	$(document).ready(function() {
		$("#persistencyTree").tree({
			url : "persistencyTree",
			lines : true,
			formatter : pcyTreeNodeFormatter,

			onBeforeLoad : function(node, param) {
				_maskSubMenu(true, "資料計算中...");
				param.pcyAlgorithm = "GOOD"; // 擇優繼續率

				if (!node) {
					param.year = 0;

					var employee = _getSelectedEmployee();

					if (employee)
						param.eno = employee.ENO; // 傳入要查詢的業代
				}
			},

			loadFilter : function(data, parent) {
				var rptTreeData = [
				/*				                   
				{
					id : "report-1",
					text : "繼續率相關活動達成進度",
					iconCls : "icon-excel",
					attributes : {
						level : 0,
						url : "individual",
						uiId : "persistencyRelActProcess2-1"
					}
				}, */
				{
					id : "report-2",
					text : "應繳日後 30-60 天內尚未繳費之保單明細",
					iconCls : "icon-report",
					attributes : {
						level : 0,
						url : "indivUnpaid",
						uiId : "unpaidRecords"
					}
				} ];

				return $.merge(rptTreeData, data);
			},

			onLoadSuccess : function(node, data) {
				// 載入完成後, 自動選取上次選取的 node
				if (nodeId) {
					var n = $(this).tree("find", nodeId);

					if (n) {
						$(this).tree("select", n.target);

						// 自動展開父節點
						var parent = $(this).tree("getParent", n.target);

						if (parent)
							$(this).tree("expand", parent.target);
					}
				}

				_maskSubMenu(false);
				_showTooltipForTreeNodeWhenExceedingContainer("tabIndiv", "persistencyTree");
			},

			onSelect : function(node) {
				// 目前不是在"個人"頁籤, 則不處理
				if (curTabIndex != 0) // curTabIndex 定義在 subMenu.jsp
					return;

				nodeId = node.id; // 記錄點選 node ID

				// tree node ID 為數字(年度)，Level為零者(父節點) 
				if ($.isNumeric(node.id) && node.attributes.level == 0) {

					// 檢查 html id "persistencyRelActProcess1-1" (在 individual.jsp)
					// 底下是否有html，若無代表該頁未開啟
					if ($('#persistencyRelActProcess1-1').length == 0) {
						_loadContent("individual");
					} else {
						// 此函式在 individual.jsp
						buildTables();
					}

				}

				if (!$.isNumeric(node.id) && node.id.indexOf("report-") == 0) { // 切換至"個人繼續率報表"
					// 該頁如未開啟, 則載入 
					if ($("#" + node.attributes.uiId).length == 0)
						_loadContent(node.attributes.url);

					_hideMenu();
					_triggerChangeSubMenuItemEvent([ "個人", node.text ]);
				} else {
					// 如非 main 頁面, 則載入 main
					if ($("#persistencyRecords").length == 0) {
						_loadContent("main"); // main 頁面載入後觸發 pcyRecordTable:load 由此 Tree 接收, 以避免丟出的 subMenu:select 事件因 main 尚未載入而接收不到 
					} else {
						var employee = _getSelectedEmployee();

						if (employee) {
							node.pcyAlgorithm = "GOOD"; // 擇優繼續率
							node.eno = employee.ENO; // 業代
							node.name = employee.ENAME; // 姓名

							// 觸發 subMenu:select 事件, 由 information.jsp & main.jsp 接收並處理
							_triggerEvent("subMenu", "subMenu:select", node);

							// 在 header 中顯示"功能選項"路徑: 觸發事件並傳遞所點選 tree nod 的階層陣列(呼叫 _getTreeNodeHierarchy() 轉換)
							_triggerChangeSubMenuItemEvent($.merge([ "個人" ], _getTreeNodeHierarchy("persistencyTree", node, pcyTreeNodeFormatter)));

							if (node.attributes.level > 1)
								_hideMenu(); // 縮合"功能選單"
						}
					}
				}
			}
		}).on("orgMenu:select", function() { // 接收"選擇人員"事件
			$(this).tree("reload", null); // refresh tree
		}).on("pcyRecordTable:load", function() { // 接收"查詢結果表格載入完成"事件(main.jsp)
			var node = $(this).tree("getSelected");

			if (node)
				$(this).tree("select", node.target);
		}).on("queryTabs:selectIndivTab", function() { // 接收選擇"個人"頁籤事件(subMenu.jsp)
			if ($("#persistencyRecords,#persistencyRelActProcess1-1,#unpaidRecords").length == 0) // 如果目前未載入兩個頁面其中一個, 則載入 main
				_loadContent("main");
			else {
				var node = $(this).tree("getSelected");

				if (node)
					$(this).tree("select", node.target);
			}
		}).on("subMenu:expandIndivMenu", function() { // 接收按"查詢選項全部展開"事件(subMenu.jsp)
			$(this).tree("expandAll");
		}).on("subMenu:collapseIndivMenu", function() { // 接收按"查詢選項全部縮合"事件(subMenu.jsp)
			$(this).tree("collapseAll");
		});

	});

	// tree node 格式化函式
	function pcyTreeNodeFormatter(node) {
		if (node.attributes.level == 0) {
			var color = $.isNumeric(node.id) || (!$.isNumeric(node.id) && node.id.indexOf("report-")) > -1 ? "blue" : "brown";
			return "<span style='color:" + color + ";font-weight:normal;text-decoration:underline'>" + ($.isNumeric(node.id) ? node.attributes.year + " 年度繼續率相關活動達成進度" : node.text) + "</span>";
		} else
			return node.text;
	}
</script>