<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<script type="text/javascript" src="../resources/js/views/util/treeChooser.js"></script>
<script type="text/javascript" src="../resources/js/views/util/unitChooser.js"></script>
<script type="text/javascript" src="../resources/js/views/util/customerChooser.js"></script>

<form id="frmPLCRRCZ" name="frmPLCRRCZ" method="post">
	<table style="font-size: 12px;">
		<tr>
			<td>保單號碼:</td>
			<td><input type="text" name="POLNO" class="easyui-validatebox" data-options="required:true" style="width: 145px;" /></td>

			<td>應繳日期:</td>
			<td><input type="text" id="enos" name="enos" class="lawui-treechooser" data-options="lines:true,multiple:true,width:200,panelHeight:300,multiline:false,url:'../system/orgMenu?eno=A00404'"></td>
			<td><input type="text" id="units" name="units" class="lawui-unitchooser"
				data-options="required:true,lines:true,multiple:true,width:300,panelHeight:300,multiline:false,treeTypes:'OrgLevel,OrgAdm,OrgTeam,Adm,OrgArea'"></td>
		</tr>
		<tr>
			<td>應繳保費年繳化:</td>
			<td><input type="text" name="RECBLE" data-options="required:true" style="width: 70px;" /></td>

			<td>實繳保費年繳化:</td>
			<td><input value="get checked" type="button" onclick="console.log($('#enos').treeChooser('getChecked'));"> <input value="get values" type="button"
				onclick="console.log($('#enos').treeChooser('getValues'));"> <input type="button" value="set values" onclick="$('#enos').treeChooser('setValues',['A00404'])"> <input type="button"
				value="get value" onclick="console.log($('#enos').treeChooser('getValue'))"></td>
			<td><input value="get checked" type="button" onclick="console.log($('#units').unitChooser('getChecked'));"> <input value="get values" type="button"
				onclick="console.log($('#units').unitChooser('getValues'));"> <input type="button" value="set values"
				onclick="$('#units').unitChooser('setValues',['O-23', 'O-21', 'O-22', 'OA-21', 'OA-22'])"> <input type="button" value="get value" onclick="console.log($('#units').unitChooser('getValue'))"></td>
		</tr>
		<tr>
			<td>續期利益:</td>
			<td><input type="text" name="TOT_DFYC" data-options="required:true" readonly></input></td>

			<td>保單狀態:</td>
			<td><select class="easyui-combobox" name="STATUS" style="width: 80px;">
					<option value="00">有效</option>
					<option value="01">停效</option>
					<option value="17">解約_不計績</option>
			</select></td>
			<td><span id="customers" class="lawui-customerchooser"></span></td>
		</tr>
	</table>
</form>

<script type="text/javascript">
	$(function() {

		$("#enos").treeChooser({
			width : 300,
			multiple : true,
			lines : true
		});

		$("#units").unitChooser({
			onBeforeSelect : function(node) {
				console.log("This is my form: " + node);
			},
			onCheck : function(record) {
				console.log(record)
			}
		});

		$("#customers").customerChooser({
			multiple : true,
			
			onBeforeLoad : function(params) {
				params.eno = "A00404"
			},
			
			onSelect : function(data) {
				console.log(data)
			}
		});

		var checkBtn = new LawButton({
			id : "checkBtn",
			iconCls : "icon-editTable",
			text : "文件確認",

			handler : function() {
				checkBtn.hide();
			}
		});

		var clearBtn = new LawButton({
			id : "clearBtn",
			iconCls : "icon-clear",
			text : "清空內容",

			handler : function() {
				$("#frmPLCRRCZ").form("clear");
			},

			shown : false
		});

		var undoBtn = new LawButton({
			id : "undoBtn",
			iconCls : "icon-undo",
			text : "回復內容",

			handler : function() {
				$("#frmPLCRRCZ").form("load", $("#persistencyRecords").datagrid("getSelected"));

				checkBtn.destroy();
				undoBtn.insertAfter(saveBtn);
			},

			shown : false
		});

		var newBtn = new LawButton({
			id : "newBtn",
			iconCls : "icon-add",
			text : "新增文件",

			handler : function() {
				_openEditor("新增");

				clearBtn.show();
				undoBtn.insertBefore(clearBtn).show();
			}
		});

		clearBtn.insertAfter(newBtn);
		undoBtn.insertAfter(clearBtn);

		var editBtn = new LawButton({
			id : "editBtn",
			iconCls : "icon-editDoc2",
			text : "修改文件",

			handler : function() {
				checkBtn.show();

				var row = $("#persistencyRecords").datagrid("getSelected");

				if (!row) {
					_showError("請選擇要修改的保單!");
					return false;
				}

				$("#frmPLCRRCZ").form("load", row); // 將選擇的表格資料列欄位值載入至 form 裡的欄位

				_openEditor("編輯");
				_maskHeader(true);
				_maskBody(true);
				_maskSubMenu(true);
				_maskOrgMenu(true);
				_maskMenu(true);
				_maskInformation(true);
			}
		});

		var saveBtn = new LawButton({
			id : "saveBtn",
			iconCls : "icon-save",
			text : "儲存文件",
			shown : false,

			handler : function() {
				if (!$('#frmPLCRRCZ').form('validate'))
					return false;

				// 根據 form 欄位建立新 row 
				var data = $("#frmPLCRRCZ").serializeArray();
				var newRow = {};

				for ( var i in data) {
					newRow[data[i].name] = data[i].value;
				}

				// 更新 datagrid row
				var row = $("#persistencyRecords").datagrid("getSelected");
				var index = $("#persistencyRecords").datagrid("getRowIndex", row);

				$("#persistencyRecords").datagrid("getRows")[index] = newRow;
				$("#persistencyRecords").datagrid("refreshRow", index).datagrid("selectRow", index);

				_maskButton(true);
				_maskEditor(true, "資料儲存中...");
				//_showProcessing("資料儲存中...");

				setTimeout(function() {
					_maskAll(false);

					_showInformation("資料儲存成功!");
					_closeEditor();
					_removeClearButton();
					_removeUndoButton();
				}, 2000);

			}
		});

		var cancelBtn = new LawButton({
			id : "cancelBtn",
			iconCls : "icon-cancel",
			text : "取消作業",
			shown : false,

			handler : function() {
				_closeEditor();
				_maskAll(false);

				clearBtn.hide();
				undoBtn.hide();
			}
		});

		newBtn.setHiddenButtons([ newBtn, editBtn ]);
		newBtn.setShownButtons([ saveBtn, cancelBtn ]);

		editBtn.setDisabledButtons([ newBtn, editBtn ]);
		editBtn.setEnabledButtons([ saveBtn, cancelBtn ]);

		cancelBtn.setHiddenButtons([ saveBtn, cancelBtn ]);
		cancelBtn.setShownButtons([ newBtn, editBtn ]);

		// 遮罩鈕
		new LawButton({
			id : "importBtn",
			iconCls : "icon-import",
			text : "遮罩",

			handler : function() {
				_setPreviewTitle("TEST...");
				_maskMenu(true);
				_maskSubMenu(true);
				_maskOrgMenu(true);
				_maskInformation(true);
				_maskPreview(true);
				_maskBody(true);
				//_maskButton(true);
				//_maskContent(true);
				_maskEditor(true);
				//_maskAll(true);
				_maskHeader(true);
			}
		});

		new LawButton({
			id : "unmaskBtn",
			iconCls : "icon-cancel",
			text : "解除遮罩",

			handler : function() {
				_maskAll(false);
			}
		});

		var menuBtn = new LawMenuButton({
			id : "menuBtn",
			iconCls : "icon-execute",
			text : "選單"
		});

		var law99Item = menuBtn.addMenuItem({
			text : "Law99",
			iconCls : "icon-transfer",
			handler : function() {
				console.log(this)
			}
		}).hide();

		menuBtn.addSeparator();
		
		var mate57Item = menuBtn.addMenuItem({
			text : "Mate57",
			iconCls : "icon-print"
		});

		mate57Item.addSubMenuItem({
			text : "Show Law99",
			iconCls : "icon-pdf",
			handler : function() {
				law99Item.show();
			}
		});
	});
</script>