<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="st" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="sf" uri="http://www.springframework.org/tags/form"%>

<script type="text/javascript" src="${pageContext.request.contextPath}/resources/js/jqueryFormat/jquery.format-1.2.min.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/resources/js/common/util/EasyuiUtil.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/resources/js/common/util/StrUtil.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/resources/js/common/util/datetime/DateTimeUtil.js"></script>

<table id="persistencyRecords" listenTo="orgMenu subMenu"></table>

<script type="text/javascript">
	$(document).ready(function() {
		// 查詢結果表格
		$("#persistencyRecords").datagrid({
			border : false,
			url : "result",
			title : '保單明細',
			rownumbers : true,
			pagination : true,
			striped : true,
			selectOnCheck : true,
			checkOnSelect : true,
			singleSelect : true,
			checkbox : true,
			fitColumns : false,
			maximized : true,
			remoteSort : true,
			showFooter : true,
			nowrap : false,
			sortName : 'PREPAYDATE',
			sortOrder : "desc",
			pageSize : 25,
			pageList : [ 25, 50, 100 ],

			onLoadSuccess: function(data){
				$("a.easyui-linkbutton").linkbutton();
			},
			
			columns : [ [ {
				field : "chk",
				checkbox : true
			}, {
				field : "POLNO",
				title : "保單號碼",
				sortable : true,
				width : 120,
				halign : "center",
				align : "right",
				formatter : function(value, row, index) {
					if (value != undefined) {
						//return '<a href="#" title="開啟綜合保單內容明細表" onclick="openPolicyDetailWindow(\'' + value + '\',\'' + row.APPLY_NO + '\');return false;" >' + value + '</a>';
						var html = "<span>" + value + "</span>";
						html +="<a href='#' class='easyui-linkbutton' data-options='iconCls:\"icon-brokerPage\",plain:true' onclick='_openBrokerPage(\"" + row.ENO + "\",\"" + row.CORP + "\",\"" + row.POLNO + "\");'></a>";
						
						return html;
					} else {
						return value;
					}
				}
			}, {
				field : "STATUSNAME",
				title : "保單狀態",
				sortable : true,
				width : 80,
				halign : "center",
				align : "center"
			}, {
				field : "PREPAYDATE",
				title : "應繳日期",
				sortable : true,
				width : 70,
				halign : "center",
				align : "center",
				formatter : function(value, row, index) {
					if (row.PREPAYYM) { // 修正應繳日期: prepayYM 年份 + prepayDate 月日, Z00741, 2013-10-04
						var prepaydate = row.PREPAYYM.substring(0, 4) + value.substring(4);
						return DateTimeUtil.transferIsoDate(prepaydate);
					} else
						return value;
				},
				styler : function(value, row, index) {
					if (row.PREPAYDATE == '總計:') { // footer
						return 'font-weight: 900; color: #CC00CC';
					}
				}
			}, {
				field : "RECBLE",
				title : "應繳保費年繳化",
				sortable : true,
				width : 103,
				halign : "center",
				align : "right",
				formatter : function(value, row, index) {
					return $.isNumeric(value) ? $.format.number(value, '##,###,###') : "";
				},
				styler : function(value, row, index) {
					if (row.PREPAYDATE == '總計:') { // footer
						return 'font-weight: 900; color: #CC00CC';
					}
				}
			}, {
				field : "RECD",
				title : "實繳保費年繳化",
				sortable : true,
				width : 103,
				halign : "center",
				align : "right",
				formatter : function(value, row, index) {
					return $.isNumeric(value) ? $.format.number(value, '##,###,###') : "";
				},
				styler : function(value, row, index) {
					if (row.PREPAYDATE == '總計:') { // footer
						return 'font-weight: 900; color: #CC00CC';
					}
				}
			}, {
				field : "TOT_DFYC",
				title : "續期利益",
				sortable : true,
				width : 70,
				halign : "center",
				align : "right",
				formatter : function(value, row, index) {
					return $.isNumeric(value) ? $.format.number(value, '##,###,###') : (row.PREPAYDATE == '總計:' ? "" : "-");
				}
			}, {
				field : "CONAME",
				title : "保險公司",
				width : 70,
				halign : "center",
				align : "center",
				resizable : true,
				sortable : true
			}, {
				field : "PAYIDCNAME",
				title : "要保人",
				sortable : true,
				width : 65,
				halign : "center",
				align : "center",
				formatter : function(value, row, index) {
					return value;
				}
			}, {
				field : "IDCNAME",
				title : "被保人",
				sortable : true,
				width : 65,
				halign : "center",
				align : "center",
				formatter : function(value, row, index) {
					return value;
					// return StrUtil.bindValueItem(value, row.CNAME);
				}
			}, {
				field : "DUE_DT2",
				title : "生效日期",
				sortable : true,
				width : 70,
				halign : "center",
				align : "center",
				formatter : function(value, row, index) {
					return DateTimeUtil.transferIsoDate(value);
				}
			}, {
				field : "PAY_TYPE_NAME",
				title : "繳別",
				sortable : true,
				width : 42,
				halign : "center",
				align : "center"
			}, {
				field : "MCOKIND",
				title : "險種代碼",
				sortable : true,
				width : 68,
				halign : "center",
				align : "center",
				formatter : function(value, row, index) {
					return StrUtil.bindValueItem(value, row.KIND_NAME);
				}
			}, {
				field : "MKINDNAME",
				title : "主約險種",
				sortable : true,
				width : 200,
				halign : "center",
				align : "left"
			}, {
				field : "MLIFE",
				title : "保額",
				sortable : true,
				width : 50,
				halign : "center",
				align : "right",
				formatter : function(value, row, index) {
					return $.isNumeric(row.MLIFE) ? $.format.number(row.MLIFE, '##,###,###') : "";
				}
			}, {
				field : "MLIFE_UNIT",
				title : "保額單位",
				sortable : true,
				width : 68,
				halign : "center",
				align : "center"
			}, {
				field : "ORG_ENAME",
				title : "原經手人",
				sortable : true,
				width : 68,
				halign : "center",
				align : "center"
			}, {
				field : "WITHINYEAR",
				title : "承接一年內",
				sortable : true,
				width : 75,
				halign : "center",
				align : "center",

				formatter : function(value) {
					return "";
				},

				styler : function(value, row, index) {
					if (value == "Y") { // footer
						return "background: url(../custom/pl/cr/qry/images/check_16x16.png) no-repeat center;";
					}
				}
			} ] ]
		}).on("subMenu:select", function(event, node) { // 接收 subMenu onSelect 的觸發
			var level = node.attributes.level;

			if (level > 1) {
				var from = (level == 2 ? node.attributes.from : node.attributes.month);
				var to = (level == 2 ? node.attributes.to : node.attributes.month);

				$(this).datagrid("load", {
					year : node.attributes.year,
					type : node.attributes.type,
					fromMonth : from,
					toMonth : "H2" == node.attributes.scope ? 12 : to, // 如 scope="H2", 為下半年 tree, 要顯示 7-12 月的保單
					eno : node.eno,
					pcyAlgorithm : node.pcyAlgorithm
				});
			} else {
				$(this).datagrid("loadData", {
					total : 0,
					rows : [],
					footer : [ {
						ORG_ENAME : "",
						RECD : "",
						RECBLE : ""
					} ]
				});
			}
		});

		// 觸發此事件, 通知 indivMenu.jsp Tree 此頁面已載入
		_triggerEvent("pcyRecordTable", "pcyRecordTable:load");
	});

	function openPolicyDetailWindow(strPolNo, strApplyNo) {
		var strUrl = 'https://www.law888.com.tw/j2ee/pol_policyquery_v2_main.jsp?seltab=all&seltype=2&m=' + strPolNo + '&applyno=' + strApplyNo;

		var win = window.open(strUrl, '_policy', 'toolbar=0,scrollbars=1,location=0,statusbar=0,menubar=0,resizable=1');
		win.focus();
	}
</script>