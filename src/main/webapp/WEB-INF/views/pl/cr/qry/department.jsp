<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<script type="text/javascript" src="${pageContext.request.contextPath}/resources/js/jqueryFormat/jquery.format-1.2.min.js"></script>

<div id="deptLevel0" class="easyui-layout" data-options="fit:true">
	<div id="panelSelfAndSpousePcy" data-options="region:'north',split:true,border:true,title:'個人及配偶繼續率'" style="height: 110px">
		<table id="selfAndSpousePcy" listenTo="subMenu orgMenu"></table>
	</div>

	<div data-options="region:'center',split:true,border:false">
		<div id="deptLevel1" class="easyui-layout" data-options="fit:true">
			<div id="panelDeptPcy" data-options="region:'north',split:true,border:true,title:'單位組織繼續率'" style="height: 85px">
				<table id="deptPcy" listenTo="subMenu orgMenu"></table>
			</div>

			<div id="panelDeptPcyByEno" data-options="region:'center',split:true,border:true,title:'組織成員繼續率'">
				<table id="deptPcyByEno" listenTo="subMenu orgMenu"></table>
			</div>
		</div>
	</div>
</div>

<script type="text/javascript">
	var toggleFlag = false;

	$(document).ready(function() {
		// 表格縮放
		_addPanelTool("panelDeptPcyByEno", {
			iconCls : "icon-toggleSize",
			handler : function() {
				if (!toggleFlag) {
					$("#deptLevel0").layout("collapse", "north");
					$("#deptLevel1").layout("collapse", "north");

					toggleFlag = true;
				} else {
					$("#deptLevel0").layout("expand", "north");
					$("#deptLevel1").layout("expand", "north");

					toggleFlag = false;
				}
			}
		});

		// 個人及配偶繼續率
		$("#selfAndSpousePcy").datagrid({
			url : "selfAndSpousePcy",
			border : false,
			maximized : true,
			fitColumns : false,
			striped : true,
			singleSelect : true,

			columns : [ [ {
				field : "DUMMY",
				title : "",
				width : 25,
				halign : "center",
				align : "center"
			}, {
				field : "isSpouse",
				title : "身份",
				width : 110,
				halign : "center",
				align : "center",
				formatter : function(value, row, index) {
					if (!value)
						return "個人";
					else
						return "配偶";
				}
			}, {
				field : "NAME",
				title : "姓名",
				width : 90,
				halign : "center",
				align : "center"
			}, {
				field : "TYPE13",
				title : "13 個月繼續率",
				width : 100,
				halign : "center",
				align : "center",
				formatter : function(value) {
					return persistencyFormatter(value);
				}
			}, {
				field : "TYPE25",
				title : "25 個月繼續率",
				width : 100,
				halign : "center",
				align : "center",
				formatter : function(value) {
					return persistencyFormatter(value);
				}
			}, {
				field : "TYPE37",
				title : "37 個月繼續率",
				width : 100,
				halign : "center",
				align : "center",
				formatter : function(value) {
					return persistencyFormatter(value);
				}
			}, {
				field : "TYPE49",
				title : "49 個月繼續率",
				width : 100,
				halign : "center",
				align : "center",
				formatter : function(value) {
					return persistencyFormatter(value);
				}
			} ] ],

			onBeforeLoad : function(param) {
				if ("${_user.IS_G_STAFF}" == "true") {
					var employee = _getSelectedEmployee();

					if (!employee)
						return false;

					param.eno = employee.ENO; // 傳入要查詢的業代
				} else
					param.eno = "${_user.ENO}";
			},

			onLoadSuccess : function(data) {
				var title;

				if (data.year)
					title = data.year + " 年 " + data.from + " 月 - " + data.to + " 月「個人及配偶繼續率」";
				else
					title = "個人及配偶繼續率";

				$("#panelSelfAndSpousePcy").panel("setTitle", title);
			}

		}).on("orgMenu:select", function() {
			if ("${_user.IS_G_STAFF}" == "true")
				$(this).datagrid("load"); // 總體行政人員才可查詢
		});

		// 單位組織繼續率
		$("#deptPcy").datagrid({
			url : "deptPcy",
			border : false,
			maximized : true,
			fitColumns : false,
			striped : true,
			singleSelect : true,

			columns : [ [ {
				field : "DUMMY",
				title : "",
				width : 25,
				halign : "center",
				align : "center"
			}, {
				field : "DUMMY2",
				title : "-",
				width : 100,
				halign : "center",
				align : "center"
			}, {
				field : "DUMMY3",
				title : "-",
				width : 100,
				halign : "center",
				align : "center"
			}, {
				field : "TYPE13",
				title : "13 個月繼續率",
				width : 100,
				halign : "center",
				align : "center",
				formatter : function(value) {
					return persistencyFormatter(value);
				}
			}, {
				field : "TYPE25",
				title : "25 個月繼續率",
				width : 100,
				halign : "center",
				align : "center",
				formatter : function(value) {
					return persistencyFormatter(value);
				}
			}, {
				field : "TYPE37",
				title : "37 個月繼續率",
				width : 100,
				halign : "center",
				align : "center",
				formatter : function(value) {
					return persistencyFormatter(value);
				}
			}, {
				field : "TYPE49",
				title : "49 個月繼續率",
				width : 100,
				halign : "center",
				align : "center",
				formatter : function(value) {
					return persistencyFormatter(value);
				}
			} ] ],

			onBeforeLoad : function(param) {
				if ("${_user.IS_G_STAFF}" == "true") {
					var employee = _getSelectedEmployee();

					if (!employee)
						return false;

					param.eno = employee.ENO; // 傳入要查詢的業代
				} else
					param.eno = "${_user.ENO}";
			},

			onLoadSuccess : function(data) {
				var title;

				if (data.year)
					title = data.year + " 年 " + data.from + " 月 - " + data.to + " 月「單位組織繼續率」";
				else
					title = "單位組織繼續率";

				$("#panelDeptPcy").panel("setTitle", title);
			},

			onLoadError : function() {
				$(this).datagrid("loadData", []);
			}
		}).on("orgMenu:select", function() {
			if ("${_user.IS_G_STAFF}" == "true")
				$(this).datagrid("load"); // 總體行政人員才可查詢
		});

		// 組織成員繼續率
		$("#deptPcyByEno").datagrid({
			url : "deptPcyByEno",
			border : false,
			maximized : true,
			fitColumns : false,
			singleSelect : true,
			rownumbers : true,
			pagination : true,
			striped : true,
			remoteSort : true,
			sortName : "TYPE13",
			sortOrder : "desc",
			multiSort : false,
			pageSize : 25,
			pageList : [ 25, 50, 100 ],

			columns : [ [ {
				field : "ENO",
				title : "組織成員",
				width : 110,
				halign : "center",
				align : "center",
				sortable : true,
				formatter : function(value, row, index) {
					return value + " " + row.NAME;
				}
			}, {
				field : "RANK_NAME",
				title : "職階",
				width : 90,
				halign : "center",
				align : "center",
				sortable : true
			}, {
				field : "TYPE13",
				title : "13 個月繼續率",
				width : 100,
				halign : "center",
				align : "center",
				sortable : true,
				formatter : function(value) {
					return persistencyFormatter(value);
				}
			}, {
				field : "TYPE25",
				title : "25 個月繼續率",
				width : 100,
				halign : "center",
				align : "center",
				sortable : true,
				formatter : function(value) {
					return persistencyFormatter(value);
				}
			}, {
				field : "TYPE37",
				title : "37 個月繼續率",
				width : 100,
				halign : "center",
				align : "center",
				sortable : true,
				formatter : function(value) {
					return persistencyFormatter(value);
				}
			}, {
				field : "TYPE49",
				title : "49 個月繼續率",
				width : 100,
				halign : "center",
				align : "center",
				sortable : true,
				formatter : function(value) {
					return persistencyFormatter(value);
				}
			} ] ],

			onBeforeLoad : function(param) {
				if ("${_user.IS_G_STAFF}" == "true") {
					var employee = _getSelectedEmployee();

					if (!employee)
						return false;

					param.eno = employee.ENO; // 傳入要查詢的業代
				} else
					param.eno = "${_user.ENO}";
			},

			onLoadSuccess : function(data) {
				var title;

				if (data.year)
					title = data.year + " 年 " + data.from + " 月 - " + data.to + " 月「組織成員繼續率」";
				else
					title = "組織成員繼續率";

				$("#panelDeptPcyByEno").panel("setTitle", title);
			},

			onLoadError : function() {
				$(this).datagrid("loadData", []);
			}
		}).on("orgMenu:select", function() {
			if ("${_user.IS_G_STAFF}" == "true")
				$(this).datagrid("load"); // 總體行政人員才可查詢
		});
	});

	// 繼續率格式化
	function persistencyFormatter(persistency) {
		return !$.isNumeric(persistency) || persistency < 0 ? "-" : ($.format.number(persistency * 100, "##.00") + "%");
	}
</script>