<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<script type="text/javascript" src="${pageContext.request.contextPath}/resources/js/jqueryFormat/jquery.format-1.2.min.js"></script>

<div id="accordionReport" class="easyui-accordion" data-options="fit:true,onSelect:function(title,index){if(index>0)$('#'+tables[index].id).datagrid('fixColumnSize','SBENO');}">
	<div id="panel-13" title="13 個月繼續率" style="height: 300px">
		<table id="unpaidReport-13" listenTo="subMenu orgMenu"></table>
	</div>

	<div id="panel-25" title="25 個月繼續率">
		<table id="unpaidReport-25" listenTo="subMenu orgMenu"></table>
	</div>

	<div id="panel-37" title="37 個月繼續率">
		<table id="unpaidReport-37" listenTo="subMenu orgMenu"></table>
	</div>

	<div id="panel-49" title="49 個月繼續率">
		<table id="unpaidReport-49" listenTo="subMenu orgMenu"></table>
	</div>
</div>

<script type="text/javascript">
	$(document).ready(function() {
		$("#accordionReport").accordion(); // 先 instantiation, 因 table 會使用

		var frozenStyle = "background-color: #eaf2ff";

		tables = [ {
			panelId : "panel-13",
			id : "unpaidReport-13",
			type : "13"
		}, {
			panelId : "panel-25",
			id : "unpaidReport-25",
			type : "25"
		}, {
			panelId : "panel-37",
			id : "unpaidReport-37",
			type : "37"
		}, {
			panelId : "panel-49",
			id : "unpaidReport-49",
			type : "49"
		} ];

		for ( var i in tables) {
			// 實繳為 0 分析報表
			$("#" + tables[i].id).datagrid({
				url : "unpaidReport",
				//title : "繼續率分析報表",
				border : false,
				maximized : true,
				fitColumns : false,
				rownumbers : true,
				singleSelect : true,
				striped : true,
				nowrap : false,
				remoteSort : false,
				customOption : tables[i], // 自訂 option

				frozenColumns : [ [ {
					field : "SBENO",
					title : "區經理",
					width : 100,
					halign : "center",
					align : "center",
					sortable : true,
					//rowspan : 2, // easyui 1.4 加此參數會錯誤 		
					formatter : function(value, row, index) {
						return value == null ? "全處" : (value + " " + row.SBENAME);
					},
					styler : function() {
						return frozenStyle;
					}
				}, {
					field : "RECD",
					title : "實收保費",
					width : 70,
					halign : "center",
					align : "right",
					sortable : true,
					//rowspan : 2,
					formatter : function(value) {
						return moneyFormatter(value);
					},
					styler : function() {
						return frozenStyle;
					}
				}, {
					field : "RECBLE",
					title : "應繳保費",
					width : 70,
					halign : "center",
					align : "right",
					sortable : true,
					//rowspan : 2,
					formatter : function(value) {
						return moneyFormatter(value);
					},
					styler : function() {
						return frozenStyle;
					}
				}, {
					field : "CNT",
					title : "繼續率",
					width : 60,
					halign : "center",
					align : "right",
					sortable : true,
					//rowspan : 2,
					formatter : function(value) {
						return persistencyFormatter(value);
					},
					styler : function() {
						return frozenStyle;
					}
				}, {
					field : "RECBLE00",
					title : "實繳為 0 應繳(A)",
					width : 110,
					halign : "center",
					align : "right",
					sortable : true,
					//rowspan : 2,
					formatter : function(value) {
						return moneyFormatter(value);
					},
					styler : function() {
						return frozenStyle;
					}
				} ] ],

				columns : [ [ {
					title : "<span style='color:red;font-weight:bold'>※ 準停效</span>",
					colspan : 2
				}, {
					title : "該狀況前三名險種",
					colspan : 3
				}, {
					title : "<b>停效</b>",
					colspan : 2
				}, {
					title : "該狀況前三名險種",
					colspan : 3
				}, {
					title : "<b>繳清</b>",
					colspan : 2
				}, {
					title : "該狀況前三名險種",
					colspan : 3
				}, {
					title : "<b>失效</b>",
					colspan : 2
				}, {
					title : "該狀況前三名險種",
					colspan : 3
				}, {
					title : "<b>解約</b>",
					colspan : 2
				}, {
					title : "該狀況前三名險種",
					colspan : 3
				} ], [ {
					field : "A_RECBLE",
					title : "保費(B)",
					width : 70,
					halign : "center",
					align : "right",
					sortable : true,
					formatter : function(value) {
						return moneyFormatter(value);
					}
				}, {
					field : "A_CNT",
					title : "佔比(B/A)",
					width : 70,
					halign : "center",
					align : "right",
					sortable : true,
					formatter : function(value) {
						return "<span style='color:blue;font-weight:bold'>" + persistencyFormatter(value) + "</span>";
					}
				}, {
					field : "A_MK1",
					title : "1",
					width : 100,
					halign : "center",
					align : "left",
					sortable : true,
					formatter : function(value) {
						return value;
					}
				}, {
					field : "A_MK2",
					title : "2",
					width : 100,
					halign : "center",
					align : "left",
					sortable : true,
					formatter : function(value) {
						return value;
					}
				}, {
					field : "A_MK3",
					title : "3",
					width : 100,
					halign : "center",
					align : "left",
					sortable : true,
					formatter : function(value) {
						return value;
					}
				}, {
					field : "B_RECBLE",
					title : "保費(C)",
					width : 70,
					halign : "center",
					align : "right",
					sortable : true,
					formatter : function(value) {
						return moneyFormatter(value);
					}
				}, {
					field : "B_CNT",
					title : "佔比(C/A)",
					width : 70,
					halign : "center",
					align : "right",
					sortable : true,
					formatter : function(value) {
						return "<span style='color:blue;font-weight:bold'>" + persistencyFormatter(value) + "</span>";
					}
				}, {
					field : "B_MK1",
					title : "1",
					width : 100,
					halign : "center",
					align : "left",
					sortable : true,
					formatter : function(value) {
						return value;
					}
				}, {
					field : "B_MK2",
					title : "2",
					width : 100,
					halign : "center",
					align : "left",
					sortable : true,
					formatter : function(value) {
						return value;
					}
				}, {
					field : "B_MK3",
					title : "3",
					width : 100,
					halign : "center",
					align : "left",
					sortable : true,
					formatter : function(value) {
						return value;
					}
				}, {
					field : "C_RECBLE",
					title : "保費(D)",
					width : 70,
					halign : "center",
					align : "right",
					sortable : true,
					formatter : function(value) {
						return moneyFormatter(value);
					}
				}, {
					field : "C_CNT",
					title : "佔比(D/A)",
					width : 70,
					halign : "center",
					align : "right",
					sortable : true,
					formatter : function(value) {
						return "<span style='color:blue;font-weight:bold'>" + persistencyFormatter(value) + "</span>";
					}
				}, {
					field : "C_MK1",
					title : "1",
					width : 100,
					halign : "center",
					align : "left",
					sortable : true,
					formatter : function(value) {
						return value;
					}
				}, {
					field : "C_MK2",
					title : "2",
					width : 100,
					halign : "center",
					align : "left",
					sortable : true,
					formatter : function(value) {
						return value;
					}
				}, {
					field : "C_MK3",
					title : "3",
					width : 100,
					halign : "center",
					align : "left",
					sortable : true,
					formatter : function(value) {
						return value;
					}
				}, {
					field : "D_RECBLE",
					title : "保費(E)",
					width : 70,
					halign : "center",
					align : "right",
					sortable : true,
					formatter : function(value) {
						return moneyFormatter(value);
					}
				}, {
					field : "D_CNT",
					title : "佔比(E/A)",
					width : 70,
					halign : "center",
					align : "right",
					sortable : true,
					formatter : function(value) {
						return "<span style='color:blue;font-weight:bold'>" + persistencyFormatter(value) + "</span>";
					}
				}, {
					field : "D_MK1",
					title : "1",
					width : 100,
					halign : "center",
					align : "left",
					sortable : true,
					formatter : function(value) {
						return value;
					}
				}, {
					field : "D_MK2",
					title : "2",
					width : 100,
					halign : "center",
					align : "left",
					sortable : true,
					formatter : function(value) {
						return value;
					}
				}, {
					field : "D_MK3",
					title : "3",
					width : 100,
					halign : "center",
					align : "left",
					sortable : true,
					formatter : function(value) {
						return value;
					}
				}, {
					field : "E_RECBLE",
					title : "保費(F)",
					width : 70,
					halign : "center",
					align : "right",
					sortable : true,
					formatter : function(value) {
						return moneyFormatter(value);
					}
				}, {
					field : "E_CNT",
					title : "佔比(F/A)",
					width : 70,
					halign : "center",
					align : "right",
					sortable : true,
					formatter : function(value) {
						return "<span style='color:blue;font-weight:bold'>" + persistencyFormatter(value) + "</span>";
					}
				}, {
					field : "E_MK1",
					title : "1",
					width : 100,
					halign : "center",
					align : "left",
					sortable : true,
					formatter : function(value) {
						return value;
					}
				}, {
					field : "E_MK2",
					title : "2",
					width : 100,
					halign : "center",
					align : "left",
					sortable : true,
					formatter : function(value) {
						return value;
					}
				}, {
					field : "E_MK3",
					title : "3",
					width : 100,
					halign : "center",
					align : "left",
					sortable : true,
					formatter : function(value) {
						return value;
					}
				} ] ],

				onBeforeLoad : function(param) {
					if ("${_user.IS_G_STAFF}" == "true") {
						var employee = _getSelectedEmployee();

						if (!employee || employee.RANK < 4) { // 經理級以上才查詢
							$(this).datagrid("loadData", []);
							return false;
						}

						param.eno = employee.ENO; // 傳入要查詢的業代
					} else
						param.eno = "${_user.ENO}";

					param.type = $(this).datagrid("options").customOption.type;
				},

				onLoadSuccess : function(data) {
					var title;

					if (data.year)
						title = data.year + " 年 " + data.from + " 月 - " + data.to + " 月「" + data.type + " 個月繼續率」實繳為 0 元分析報表";
					else
						title = "「" + $(this).datagrid("options").customOption.type + " 個月繼續率」實繳為 0 元分析報表";

					$("#" + $(this).datagrid("options").customOption.panelId).panel("setTitle", title);
				},

				onLoadError : function() {
					$(this).datagrid("loadData", []);
				}
			}).on("orgMenu:select", function() {
				if ("${_user.IS_G_STAFF}" == "true")
					$(this).datagrid("load");
			});
		}
	});

	// 格式化繼續率 OR 佔比
	function persistencyFormatter(persistency) {
		return !$.isNumeric(persistency) || persistency < 0 ? "-" : ($.format.number(persistency * 100, "##.00") + "%");
	}

	// 格式化金額
	function moneyFormatter(money) {
		return !$.isNumeric(money) ? "-" : $.format.number(money, "###,###");
	}
</script>
