<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<script type="text/javascript" src="${pageContext.request.contextPath}/resources/js/jqueryFormat/jquery.format-1.2.min.js"></script>

<div id="deptLevel0" class="easyui-layout" data-options="fit:true">
	<div id="panelOrgPcyByDept" data-options="region:'north',split:true,border:true,title:'各區繼續率'" style="height: 185px">
		<table id="orgPcyByDept" listenTo="subMenu orgMenu"></table>
	</div>

	<div id="panelOrgPcyByDeptEno" data-options="region:'center',split:true,border:true,title:'各區組織成員繼續率'">
		<table id="orgPcyByDeptEno" listenTo="subMenu orgMenu"></table>
	</div>
</div>

<script type="text/javascript">
	var toggleFlag = false;

	$(document).ready(function() {
		// 表格縮放
		_addPanelTool("panelOrgPcyByDeptEno", {
			iconCls : "icon-toggleSize",
			handler : function() {
				if (!toggleFlag) {
					_hideInfoPanel();
					$("#deptLevel0").layout("collapse", "north");

					toggleFlag = true;
				} else {
					$("#deptLevel0").layout("expand", "north");
					_hideInfoPanel();

					toggleFlag = false;
				}
			}
		});

		// 各區繼續率
		$("#orgPcyByDept").datagrid({
			url : "orgPcyByDept",
			border : false,
			lines : true,
			maximized : true,
			rownumbers : true,
			fitColumns : false,
			singleSelect : true,
			striped : true,
			remoteSort : false,
			sortName : "TYPE13",
			sortOrder : "desc",

			columns : [ [ {
				field : "MANAGER_ENO",
				title : "區經理",
				width : 100,
				halign : "center",
				align : "center",
				sortable : true,
				formatter : function(value, row, index) {
					return value + " " + row.MANAGER_NAME;
				}
			}, {
				field : "TYPE13",
				title : "13 個月繼續率",
				width : 100,
				halign : "center",
				align : "center",
				sortable : true,
				formatter : function(value) {
					return persistencyFormatter(value);
				}
			}, {
				field : "TYPE25",
				title : "25 個月繼續率",
				width : 100,
				halign : "center",
				align : "center",
				sortable : true,
				formatter : function(value) {
					return persistencyFormatter(value);
				}
			}, {
				field : "TYPE37",
				title : "37 個月繼續率",
				width : 100,
				halign : "center",
				align : "center",
				sortable : true,
				formatter : function(value) {
					return persistencyFormatter(value);
				}
			}, {
				field : "TYPE49",
				title : "49 個月繼續率",
				width : 100,
				halign : "center",
				align : "center",
				sortable : true,
				formatter : function(value) {
					return persistencyFormatter(value);
				}
			} ] ],

			onBeforeLoad : function(param) {
				if ("${_user.IS_G_STAFF}" == "true") {
					var employee = _getSelectedEmployee();

					if (!employee)
						return false;

					param.eno = employee.ENO; // 傳入要查詢的業代
				} else
					param.eno = "${_user.ENO}";
			},

			onLoadSuccess : function(data) {
				var title = "各區繼續率";

				if (data.year)
					title = data.year + " 年 " + data.from + " 月 - " + data.to + " 月「各區繼續率」";

				$("#panelOrgPcyByDept").panel("setTitle", title);
			},

			onLoadError : function() {
				$(this).datagrid("loadData", []);
			}
		}).on("orgMenu:select", function() {
			if ("${_user.IS_G_STAFF}" == "true")
				$(this).datagrid("load"); // 總體行政人員才可查詢
		});

		// 各區組織成員繼續率
		$("#orgPcyByDeptEno").datagrid({
			url : "orgPcyByDeptEno",
			border : false,
			maximized : true,
			fitColumns : false,
			rownumbers : true,
			singleSelect : true,
			pagination : true,
			striped : true,
			remoteSort : false,
			sortName : "MANAGER_NAME",
			pageSize : 300,
			pageList : [ 300 ],

			columns : [ [ {
				field : "MANAGER_NAME",
				title : "區經理",
				width : 100,
				halign : "center",
				align : "center",
				sortable : true
			}, {
				field : "ENO",
				title : "組織成員",
				width : 100,
				halign : "center",
				align : "center",
				sortable : true,
				formatter : function(value, row, index) {
					return value + " " + row.NAME;
				}
			}, {
				field : "RANK_NAME",
				title : "職階",
				width : 100,
				halign : "center",
				align : "center",
				sortable : true
			}, {
				field : "TYPE13",
				title : "13 個月繼續率",
				width : 100,
				halign : "center",
				align : "center",
				sortable : true,
				formatter : function(value) {
					return persistencyFormatter(value);
				}
			}, {
				field : "TYPE25",
				title : "25 個月繼續率",
				width : 100,
				halign : "center",
				align : "center",
				sortable : true,
				formatter : function(value) {
					return persistencyFormatter(value);
				}
			}, {
				field : "TYPE37",
				title : "37 個月繼續率",
				width : 100,
				halign : "center",
				align : "center",
				sortable : true,
				formatter : function(value) {
					return persistencyFormatter(value);
				}
			}, {
				field : "TYPE49",
				title : "49 個月繼續率",
				width : 100,
				halign : "center",
				align : "center",
				sortable : true,
				formatter : function(value) {
					return persistencyFormatter(value);
				}
			} ] ],

			onBeforeLoad : function(param) {
				if ("${_user.IS_G_STAFF}" == "true") {
					var employee = _getSelectedEmployee();

					if (!employee)
						return false;

					param.eno = employee.ENO; // 傳入要查詢的業代
				} else
					param.eno = "${_user.ENO}";
			},

			onLoadSuccess : function(data) {
				var title;

				if (data.year)
					title = data.year + " 年 " + data.from + " 月 - " + data.to + " 月「組織成員繼續率」";
				else
					title = "組織成員繼續率";

				$("#panelOrgPcyByDeptEno").panel("setTitle", title);

				var curManager = "";
				var fromIndex = 0;
				var rowCount = 0;

				// 合併同區經理的列
				for ( var i in data.rows) {
					var row = data.rows[i];

					if (curManager != row.MANAGER_ENO) {
						if (rowCount > 1) {
							$(this).datagrid("mergeCells", {
								index : fromIndex,
								field : "MANAGER_NAME",
								rowspan : rowCount
							});
						}

						rowCount = 0;
						fromIndex = i;
						curManager = row.MANAGER_ENO;
					} else if (i == data.rows.length - 1 && rowCount > 0) {
						$(this).datagrid("mergeCells", {
							index : fromIndex,
							field : "MANAGER_NAME",
							rowspan : rowCount + 1
						});

						break;
					}

					++rowCount;
				}
			},

			onLoadError : function() {
				$(this).datagrid("loadData", []);
			}
		}).on("orgMenu:select", function() {
			if ("${_user.IS_G_STAFF}" == "true")
				$(this).datagrid("load"); // 總體行政人員才可查詢
		});
	});

	function persistencyFormatter(persistency) {
		return !$.isNumeric(persistency) || persistency < 0 ? "-" : ($.format.number(persistency * 100, "##.00") + "%");
	}
</script>
