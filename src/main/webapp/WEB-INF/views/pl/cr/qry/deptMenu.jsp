<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>

<!-- 繼續率「組織」查詢選項 -->
<ul id="deptPersistencyTree" listenTo="orgMenu subMenu queryTabs"></ul>

<script type="text/javascript">
	var deptNodeId = null;

	$(document).ready(function() {
		$("#deptPersistencyTree").tree({
			url : "deptPcyTree",
			lines : true,
			formatter : pcyTreeNodeFormatter,

			onBeforeLoad : function(node, param) {
				param.pcyAlgorithm = "GOOD"; // 擇優繼續率

				if (!node) {
					param.year = 0;

					if ("${_user.IS_G_STAFF}" == "true") {
						var employee = _getSelectedEmployee();

						if (!employee || employee.RANK < 4)
							param.eno = "dummy"; // 非經理級以上人員, 不顯示其繼續率
						else
							param.eno = employee.ENO; // 傳入要查詢的業代
					} else
						param.eno = "${_user.ENO}";
				}
			},

			loadFilter : function(data, parent) {
				var rptTreeData = [ {
					id : "report-1",
					text : "組織 & 配偶繼續率",
					iconCls : "icon-excel",
					
					attributes : {
						level : 0,
						url : "department",
						uiId : "panelSelfAndSpousePcy"
					}
				} ];

				if ("${_user.IS_ORG_MANAGER || _user.IS_G_STAFF}" == "true")
					rptTreeData = $.merge(rptTreeData, [ {
						id : "report-2",
						text : "全單位繼續率",
						iconCls : "icon-excel",
						selected: true,
						attributes : {
							level : 0,
							url : "organization",
							uiId : "panelOrgPcyByDept"
						}
					} ]);

				rptTreeData = $.merge(rptTreeData, [ {
					id : "report-3",
					text : "實繳為 0 元分析報表",
					iconCls : "icon-report",
					attributes : {
						level : 0,
						url : "plcrrcz",
						uiId : "unpaidReport-13"
					}
				} ]);

				return $.merge(rptTreeData, data);
			},

			onLoadSuccess : function(node, data) {
				// 載入完成後, 自動選取上次選取的 node
				if (!deptNodeId)
					;//deptNodeId = "report-1"; // 預設開啟頁面

				var n = $(this).tree("find", deptNodeId);

				if (n) {
					$(this).tree("select", n.target);

					// 自動展開父節點
					var parent = $(this).tree("getParent", n.target);

					if (parent)
						$(this).tree("expand", parent.target);
				}
			},

			onBeforeSelect : function(node) {
				if ($.isNumeric(node.id) || !node.id.indexOf("report-") == 0)
					return false;
			},

			onSelect : function(node) {
				// 目前不是在"組織"頁籤, 則不處理
				if (curTabIndex != 1) // curTabIndex 定義在 subMenu.jsp
					return;

				deptNodeId = node.id;

				// 該頁如未開啟, 則載入 
				if ($("#" + node.attributes.uiId).length == 0)
					_loadContent(node.attributes.url);

				_triggerChangeSubMenuItemEvent([ "組織", node.text ]);
			}
		}).on("queryTabs:selectDeptTab", function() { // 接收選擇"組織"頁籤事件(subMenu.jsp)
			var node = $(this).tree("getSelected");

			if (node)
				$(this).tree("select", node.target);
			else
				_loadContent("department");
		}).on("orgMenu:select", function(event, node) {
			if ("${_user.IS_G_STAFF}" == "true")
				$(this).tree("reload", null); // 總體行政人員才可查詢
		}).on("subMenu:expandDeptMenu", function() {
			$(this).tree("expandAll");
		}).on("subMenu:collapseDeptMenu", function() {
			$(this).tree("collapseAll");
		});
	});

	// tree node 格式化函式
	function pcyTreeNodeFormatter(node) {
		if (node.attributes.level == 0) {
			var color = !$.isNumeric(node.id) && node.id.indexOf("report-") > -1 ? "blue" : "brown";
			return "<span style='color:" + color + ";font-weight:normal;text-decoration:underline'>" + node.text + "</span>";
		} else
			return node.text;
	}
</script>