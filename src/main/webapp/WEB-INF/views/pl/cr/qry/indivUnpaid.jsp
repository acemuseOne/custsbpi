<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="st" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="sf" uri="http://www.springframework.org/tags/form"%>

<script type="text/javascript" src="${pageContext.request.contextPath}/resources/js/jqueryFormat/jquery.format-1.2.min.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/resources/js/common/util/EasyuiUtil.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/resources/js/common/util/StrUtil.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/resources/js/common/util/datetime/DateTimeUtil.js"></script>

<table id="unpaidRecords" listenTo="orgMenu subMenu"></table>

<script type="text/javascript">
	$(document).ready(function() {
		// 查詢結果表格
		$("#unpaidRecords").datagrid({
			border : true,
			url : "unpaidRecords",
			title : "應繳日後 30-60 天內尚未繳費之保單明細",
			rownumbers : true,
			pagination : true,
			striped : true,
			selectOnCheck : false,
			checkOnSelect : false,
			singleSelect : true,
			fitColumns : false,
			maximized : true,
			remoteSort : true,
			nowrap : false,
			sortName : 'PREPAYDATE',
			sortOrder : "desc",
			pageSize : 25,
			pageList : [ 25, 50, 100 ],

			columns : [ [ {
				field : "C_ENO",
				title : "業代",
				sortable : true,
				width : 65,
				halign : "center",
				align : "center",
				formatter : function(value, row, index) {
					return value;
				}
			}, {
				field : "ENAME",
				title : "姓名",
				sortable : true,
				width : 65,
				halign : "center",
				align : "center",
				formatter : function(value, row, index) {
					return value;
				}
			}, {
				field : "CONAME",
				title : "保險公司",
				width : 70,
				halign : "center",
				align : "center",
				resizable : true,
				sortable : true
			}, {
				field : "POLNO",
				title : "保單號碼",
				sortable : true,
				width : 90,
				halign : "center",
				align : "right",
				formatter : function(value, row, index) {
					if (value != undefined) {
						return '<a href="#" title="開啟綜合保單內容明細表" onclick="openPolicyDetailWindow(\'' + value + '\',\'' + row.APPLY_NO + '\');return false;" >' + value + '</a>';
					} else {
						return value;
					}
				}
			}, {
				field : "PAYIDCNAME",
				title : "要保人",
				sortable : true,
				width : 65,
				halign : "center",
				align : "center",
				formatter : function(value, row, index) {
					return value;
				}
			}, {
				field : "IDCNAME",
				title : "被保人",
				sortable : true,
				width : 65,
				halign : "center",
				align : "center",
				formatter : function(value, row, index) {
					return value;
				}
			}, {
				field : "STATUSNAME",
				title : "保單狀態",
				sortable : true,
				width : 80,
				halign : "center",
				align : "center"
			}, {
				field : "PREPAYDATE",
				title : "應繳日期",
				sortable : true,
				width : 70,
				halign : "center",
				align : "center",
				formatter : function(value, row, index) {
					if (row.PREPAYYM) { // 修正應繳日期: prepayYM 年份 + prepayDate 月日, Z00741, 2013-10-04
						var prepaydate = row.PREPAYYM.substring(0, 4) + value.substring(4);
						return DateTimeUtil.transferIsoDate(prepaydate);
					}
				}
			}, {
				field : "RECBLE",
				title : "應繳保費年繳化",
				sortable : true,
				width : 103,
				halign : "center",
				align : "right",
				formatter : function(value, row, index) {
					return $.isNumeric(value) ? $.format.number(value, '##,###,###') : "";
				}
			}, {
				field : "CNTNUTYPE",
				title : "繼續率別",
				sortable : true,
				width : 70,
				halign : "center",
				align : "center",
				formatter : function(value, row, index) {
					return value + " 個月";
				}
			}, {
				field : "YEARS",
				title : "年期",
				sortable : true,
				width : 42,
				halign : "center",
				align : "right"
			}, {
				field : "PAY_TYPE_NAME",
				title : "繳別",
				sortable : true,
				width : 42,
				halign : "center",
				align : "center"
			}, {
				field : "WITHINYEAR",
				title : "承接一年內",
				sortable : true,
				width : 75,
				halign : "center",
				align : "center",

				formatter : function(value) {
					return "";
				},

				styler : function(value, row, index) {
					if (value == "Y") { // footer
						return 'background: url(../custom/pl/cr/qry/images/check_16x16.png) no-repeat center;';
					}
				}
			} ] ],

			onBeforeLoad : function(param) {
				var employee = _getSelectedEmployee();

				if (employee)
					param.eno = employee.ENO;
				else
					return false;
			},

			onLoadSuccess : function(data) {
				/* title 固定為 "應繳日後.......", 2013-10-01
				var title = "前兩個月實繳為 0 保單明細";

				if (data)
					title = data.fromYM + " ~ " + data.toYM + " 實繳為 0 保單明細";

				$(this).datagrid("getPanel").panel("setTitle", title);
				 */
			}

		}).on("orgMenu:select", function(event, node) { // 接收 subMenu onSelect 的觸發
			$(this).datagrid("reload");
		});
	});

	function openPolicyDetailWindow(strPolNo, strApplyNo) {
		var strUrl = 'https://www.law888.com.tw/j2ee/pol_policyquery_v2_main.jsp?seltab=all&seltype=2&m=' + strPolNo + '&applyno=' + strApplyNo;

		var win = window.open(strUrl, '_policy', 'toolbar=0,scrollbars=1,location=0,statusbar=0,menubar=0,resizable=1');
		win.focus();
	}
</script>