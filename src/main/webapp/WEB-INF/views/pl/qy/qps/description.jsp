<?xml version="1.0" encoding="UTF-8" ?>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>

<div style="font-weight: bold">說明：</div>

<div id="polMsgKindDescription" listenTo="informTree" style="margin-left: 40px; font-size: 14px"></div>

<script type="text/javascript">
	$(document).ready(function() {
		$("#polMsgKindDescription").on("informTree:select", function(event, data) {
			$("#polMsgKindDescription").html(data ? data.showMsg : ""); // 顯示通知項目說明
		}).on("queryForm:submit", function(event, params) {
			$("#polMsgKindDescription").html(""); // 歷史資料查詢時將"說明"清空
		});;
	});
</script>