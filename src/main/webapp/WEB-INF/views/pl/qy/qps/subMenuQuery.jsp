<?xml version="1.0" encoding="UTF-8" ?>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<link rel="stylesheet" type="text/css" href="${_contextPath}/custom/pl/qy/qps/style.css"></link>

<!-- 查詢條件 -->
<form id="frmQuery" style="margin: 0 auto" listenTo="orgMenu">
	<table style="width: 300px; font-size: 13px; white-space: nowrap;">
		<tr>
			<td>
				<!-- 為使 combobox 與 input 可以置中對齊 --> <input id="selQueryColumn" value="prompt" data-options="editable:false" style="width: 100px; vertical-align: middle; display: inline-block"> = <input
				id="columnValue" type="text" style="width: 180px; vertical-align: middle; display: inline-block">
			</td>
		</tr>
		<tr>
			<td style="padding-top: 5px; text-align: center"><a id="btnReset" href="#" class="easyui-linkbutton" data-options="iconCls:'icon-clear'" onclick="$('#frmQuery').form('reset');">清空</a><a
				id="btnQuery" href="#" class="easyui-linkbutton" data-options="iconCls:'icon-search'" onclick="$('#frmQuery').submit();" style="margin-left: 10px">查詢</a></td>
		</tr>
	</table>
</form>

<!-- Initialization & Definition -->
<script type="text/javascript">
	$(document).ready(function() {
		// 查詢條件
		var queryParams = {};

		// 查詢欄位下拉選單
		$("#selQueryColumn").combobox({
			valueField : "id",
			textField : "text",

			data : [ {
				id : "prompt",
				text : "請選擇"
			}, {
				id : "PID",
				text : "要保人ID"
			}, {
				id : "P_CNAME",
				text : "要保人姓名"
			}, {
				id : "CID",
				text : "被保人ID"
			}, {
				id : "C_CNAME",
				text : "被保人姓名"
			}, {
				id : "CONAME",
				text : "保險公司"
			}, {
				id : "POLNO",
				text : "保單號碼"
			} ]
		});

		// "查詢條件" Form
		$("#frmQuery").form({
			// 查詢
			onSubmit : function(params) {
				var conditions = []; // 查詢條件字串

				// "業代"參數
				var employee = _getSelectedEmployee();

				if (!employee) {
					_showError("請選擇要查詢的業代!");
					_triggerEvent("queryForm", "queryForm:submit", null); // 觸發事件, 傳遞 null 參數以清空細項資料
					return false;
				}

				params.eno = employee.ENO;

				// "查詢欄位"參數
				var queryColumn = $("#selQueryColumn").combobox("getValue");
				var columnValue = $("#columnValue").val();

				if (queryColumn != "prompt") {
					var data = $("#selQueryColumn").combobox("getData");
					var columnName = "";

					for ( var i in data) {
						if (data[i].id == queryColumn) {
							columnName = data[i].text;
							break;
						}
					}

					if (columnValue == "") {
						_showError("請填寫查詢條件「" + columnName + "」值!");
						$("#columnValue").focus();

						return false;
					} else {
						params.queryColumn = queryColumn;
						params.columnValue = columnValue;

						conditions = conditions.concat([ columnName + " = " + columnValue ]); // 階層路徑加上查詢條件
					}
				} else if (columnValue != "") {
					$("#selQueryColumn").combobox("showPanel");
					$("#selQueryColumn").combobox("textbox").focus();

					return false;
				}

				queryParams = params;

				// 發出查詢請求				
				_triggerEvent("queryForm", "queryForm:submit"); // 觸發 queryForm:submit 事件
				displayConditions(conditions); // 顯示查詢條件

				return false; // 不送出 submit
			}
		}).on("reset", function() { // 接收 reset 事件 

		}).on("keypress", function(event) {
			if (event.keyCode == 13)
				$(this).submit();
		}).on("orgMenu:select", function(event, node) { // 接收"組織層級"點選人員事件
			if ($("#displayFrom").val() == "history") {
				$(this).submit();
			}
		});
	});

	// 查詢條件格式化函式
	function displayConditions(cond) {
		var displayString = "";

		for (var i = 0; i < cond.length; i++) {
			displayString += (i == 0 ? "" : " / ") + cond[i]; // 將任何 html tag 的部份移除, 只取 text
		}

		$("#displayConditions").text(displayString);
	}
</script>
