<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="st" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="sf" uri="http://www.springframework.org/tags/form"%>

<script type="text/javascript" src="${pageContext.request.contextPath}/resources/js/jqueryFormat/jquery.format-1.2.min.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/resources/js/common/util/EasyuiUtil.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/resources/js/common/util/StrUtil.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/resources/js/common/util/datetime/DateTimeUtil.js"></script>

<!-- 查詢結果細項 -->
<table id="informRecords" listenTo="queryForm informTree"></table>

<!-- 記錄系統(DB)日期 -->
<input type="hidden" id="sysDate"></input>

<script type="text/javascript">
	$(document).ready(function() {
		// 取得系統(DB)日期
		$.get("${_contextPath}/system/sysDate", function(data) {
			// 計算前一天日期
			var strToday = DateTimeUtil.transferIsoDate(data);
			var targetDate = DateTimeUtil.parseDate(strToday);
			targetDate.setDate(targetDate.getDate() - 1);
			strTargetDate = DateTimeUtil.convertDateToISOString(targetDate);
		});

		// 查詢結果表格
		$("#informRecords").datagrid({
			border : false,
			title : '查詢結果',
			rownumbers : true,
			pagination : true,
			striped : true,
			selectOnCheck : false,
			checkOnSelect : false,
			singleSelect : true,
			fitColumns : false,
			maximized : true,
			remoteSort : true,
			nowrap : false,
			pageSize : 25,
			pageList : [ 25, 50, 100 ],
			rowStyler : function(index, row) {
				if (row.CREATEDDATE == strTargetDate) {
					return "color:red;";
				}
			},

			columns : [ [ {
				field : "CREATEDDATE",
				title : "通知日期",
				width : 70,
				halign : "center",
				align : "center",
				resizable : true,
				sortable : true
			}, {
				field : "MSGLOG",
				title : "備註",
				width : 220,
				halign : "center",
				align : "center",
				resizable : true,
				sortable : true,
				formatter : function(value, row, index) {
					// 警示性通知每筆
					var showWarnPcyNotes = row.NOPAYKIND == "1" || row.NOPAYKIND == "5" || row.NOPAYKIND == "9" || row.NOPAYKIND == "3";
					// 信用卡扣款失敗每筆
					var showCreditFailPcyNotes = row.NOPAYKIND == "0";
					
					var msg = (value ? value : "");
					if (showWarnPcyNotes) { // 繼續率註記
						msg = msg + (row.PCYNOTES && showWarnPcyNotes ? ("<span style='display:block;color:blue'>" + row.PCYNOTES + "</span>") : "");
					}

					if (showCreditFailPcyNotes) {
						var isMonthQuarter = row.PAYTYPE_NAME == '月' || row.PAYTYPE_NAME == '季';
						if (isMonthQuarter) {
							msg = msg + "<br/><span style='display:block;color:blue'>月/季繳件-預定停效日 " + row.LAPSEDATE + "</span>";
						}
					}
					
					return msg;
				}
			}, {
				field : "CONAME",
				title : "保險公司 / 保單號碼",
				width : 130,
				halign : "center",
				align : "left",
				resizable : true,
				sortable : true,
				formatter : function(value, row, index) {
					return value + (row.POLNO ? "<br>" + row.POLNO : "");
				}
			}, {
				field : "PID",
				title : "要保人ID / 姓名",
				width : 100,
				halign : "center",
				align : "center",
				resizable : true,
				sortable : true,
				formatter : function(value, row, index) {
					return value + "<br>" + row.P_CNAME;
				}
			}, {
				field : "CID",
				title : "被保人ID / 姓名",
				width : 100,
				halign : "center",
				align : "center",
				resizable : true,
				sortable : true,
				formatter : function(value, row, index) {
					return value + "<br>" + row.C_CNAME;
				}
			}, {
				field : "PAYDUEDATE",
				title : "應繳日",
				width : 70,
				halign : "center",
				align : "center",
				resizable : true,
				sortable : true
			}, {
				field : "STATUS_NAME",
				title : "保單狀況",
				width : 75,
				halign : "center",
				align : "center",
				resizable : true,
				sortable : true
			}, {
				field : "BANK",
				title : "代扣行庫 / 發卡銀行",
				width : 120,
				halign : "center",
				align : "center",
				resizable : true,
				sortable : true,
				formatter : function(value, row, index) {
					return (value ? value : "") + "<br>" + (row.CARD_BANK ? row.CARD_BANK : "");
				}
			}, {
				field : "DUE_DT2",
				title : "契約生效日",
				width : 70,
				halign : "center",
				align : "center",
				resizable : true,
				sortable : true
			}, {
				field : "TOTINV",
				title : "應繳金額",
				width : 70,
				halign : "center",
				align : "right",
				resizable : true,
				sortable : true,
				formatter : function(value, row, index) {
					return $.isNumeric(row.TOTINV) ? $.format.number(row.TOTINV, '##,###,###') : "";
				}
			}, {
				field : "PAYTYPE_NAME",
				title : "繳別",
				width : 45,
				halign : "center",
				align : "center",
				resizable : true,
				sortable : true
			}, {
				field : "KIND",
				title : "主約險種代碼 / 險種",
				width : 220,
				halign : "center",
				align : "center",
				resizable : true,
				sortable : true,
				formatter : function(value, row, index) {
					return value + "<br>" + row.KIND_NAME;
				}
			}, {
				field : "LIFE",
				title : "主約保額 /	單位",
				width : 90,
				halign : "center",
				align : "right",
				resizable : true,
				sortable : true,
				formatter : function(value, row, index) {
					return StrUtil.bindValueItem(value, row.UNIT_NAME);
				}
			}, {
				field : "UUID",
				title : "UUID",
				width : 50,
				halign : "center",
				align : "right",
				resizable : true,
				sortable : true
			} ] ]
		}).on("informTree:select", function(event, data) { // 接收 informTree onSelect 的觸發

			if (data.polMsgKind != "0") {
				$(this).datagrid("hideColumn", 'BANK');
			} else {
				$(this).datagrid("showColumn", 'BANK');
			}
			
			if (!data)
				$(this).datagrid("loadData", []);
			else {
				var employee = _getSelectedEmployee();

				if (!employee)
					$(this).datagrid("loadData", []);
				else {
					data.eno = employee.ENO;

					$(this).datagrid({
						url : "${_contextPath}/PLQYQPS/queryHistory",
						pageNumber : 1,
						queryParams : data
					});
				}

				_hideMenu(); // 縮合"功能選單"
			}
		});
	});
	
</script>