<?xml version="1.0" encoding="UTF-8" ?>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<link rel="stylesheet" type="text/css" href="${_contextPath}/custom/pl/qy/qps/style.css"></link>

<div style="margin-bottom: 10px; font-size: 13px">
	<span style="font-weight: bold">查詢條件: </span><span id="displayConditions" style="color: blue"></span>
</div>

<!-- 選項 -->
<ul id="informTree" listenTo="orgMenu queryForm"></ul>

<!-- 記錄目前點選 #informTree Node ID -->
<input type="hidden" id="informTreeNodeId"></input>

<!-- Initialization & Definition -->
<script type="text/javascript">
	$(document).ready(function() {
		// 查詢條件
		var queryParams = {};

		// 查詢欄位下拉選單
		$("#selQueryColumn").combobox({
			valueField : "id",
			textField : "text",

			data : [ {
				id : "prompt",
				text : "請選擇"
			}, {
				id : "PID",
				text : "要保人ID"
			}, {
				id : "P_CNAME",
				text : "要保人姓名"
			}, {
				id : "CID",
				text : "被保人ID"
			}, {
				id : "C_CNAME",
				text : "被保人姓名"
			}, {
				id : "CONAME",
				text : "保險公司"
			}, {
				id : "POLNO",
				text : "保單號碼"
			} ]
		});

		// "通知提示" Tree
		$("#informTree").tree({
			url : "${_contextPath}/PLQYQPS/inform",
			lines : true,
			// formatter() 在 1.3.3 以前版本無法使用
			formatter : informTreeFormatter,

			onBeforeLoad : function(node, param) {
				if (!$.isEmptyObject(queryParams))
					$.extend(param, queryParams);

				// "業代"參數
				var employee = _getSelectedEmployee();

				if (!employee) {
					return false;
				}

				param.eno = employee.ENO;
			},

			onLoadSuccess : function(node, data) {
				// 載入完成後, 自動選取
				var id = $("#informTreeNodeId").val();

				if (id != "") {
					var node = $(this).tree("find", id);

					if (node) {
						$(this).tree("select", node.target);
					}
				} else {
					if (!$.isEmptyObject(queryParams))
						_triggerEvent("informTree", "informTree:select", queryParams); // 觸發 informTree:select 事件
				}
			},

			onSelect : function(node) {
				// 在 header 中顯示"功能選項"路徑
				_triggerChangeSubMenuItemEvent(_getTreeNodeHierarchy("informTree", node, informTreeFormatter));
				var data = null;

				if (node.attributes.level > 0) {
					$("#informTreeNodeId").val(node.id); // 儲存點選 node id

					data = {
						polMsgKind : node.attributes.polMsgKind,
						polMsgKindName : node.text,
						showMsg : node.attributes.showMsg
					};

					$.extend(data, queryParams);
				}

				_triggerEvent("informTree", "informTree:select", data); // 觸發 informTree:select 事件
			}
		}).on("orgMenu:select", function(event) { // 接收"組織層級"點選人員事件
			$(this).tree("reload", null); // 更新 tree nodes
		}).on("queryForm:submit", function(event) {
			$(this).tree("reload", null);
		});

		// 加入"全部展開" tool bar
		_addSubMenuTool("icon-expandAll", function() {
			$("#informTree").tree("expandAll");
		});

		// 加入"全部縮合" tool bar
		_addSubMenuTool("icon-collapseAll", function() {
			$("#informTree").tree("collapseAll");
		});
	});

	function informTreeFormatter(node) {
		if (node.attributes.level == 0) {
			if (node.attributes.docType == "02")
				return "<span class='warning'>" + node.text + "</span>";
			else
				return node.text;
		} else {
			var html = "";

			if (node.attributes.docType == "02")
				html = "<span class='warning'>" + node.text + "</span>";
			else
				html = node.text;

			var newCount = node.attributes.newCount;
			var totalCount = node.attributes.totalCount;

			return html + " (新增 <span class='" + (newCount == 0 ? "normal" : "highlight") + "'>" + $.format.number(newCount, "##,###,###") + "</span> 筆 / 共 <span class='"
					+ (totalCount == 0 ? "normal" : "highlight") + "'>" + $.format.number(totalCount, "##,###,###") + "</span> 筆)";
		}
	}
</script>
