<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<script type="text/javascript" src="../resources/js/views/util/customerChooser.js"></script>

<form id="frmPLCRRCZ" name="frmPLCRRCZ" method="post">
	
	<table border="0" style="font-size: 12px;">
		<tr>
			<td>ENO</td>
			<td>
				<input type="text" id="ENO"/>
				<!-- 百家姓選取器 -->
				<span id="customerChooser"></span>
			</td>
			<td>NAME</td>
			<td><input type="text" id="NAME"/></td>
		</tr>
		<tr>
			<td style="text-align: right; width: 50px;"><label>報表格式: </label></td>
			<td style="text-align: left; width: 80px;"><input type="text" id="selReportFormat" name="selReportFormat" data-options="required:true" style="width: 80px;" /></td>

			<td style="text-align: right; width: 100px;"><label>繼續率起始月份: </label></td>
			<td style="text-align: left; width: 70px;"><input type="text" id="cntStartDate" name="cntStartDate" maxlength="6" class="easyui-validatebox" data-options="required:true" style="width: 70px"></input></td>
		</tr>
		<tr>
			<td style="text-align: right;"><label>繼續率指標: </label></td>
			<td style="text-align: left;"><input type="text" id="cntTarget" name="cntTarget" data-options="required:true" style="width: 70px;" /></td>

			<td style="text-align: right;"><label>繼續率結束月份: </label></td>
			<td style="text-align: left;"><input type="text" id="cntEndDate" name="cntEndDate" maxlength="6" class="easyui-validatebox" data-options="required:true" style="width: 70px"></input></td>
		</tr>
		<tr>
			<td style="text-align: right;"><label>單位: </label></td>
			<td style="text-align: left;"><input type="text" id="cntOrg" name="cntOrg" data-options="required:true" style="width: 70px"></input></td>
		</tr>
	</table>
</form>

<script type="text/javascript">
	$(document).ready(function() {
		// 新增鈕
		_addNewButton(function() {
			_openEditor("新增資料");
			_enableNewButton(false);
			_enableSaveButton(true);
			_enableCancelButton(true);
			_enableExportButton(true);
		}, true, "新增記錄");

		// 儲存鈕
		_addSaveButton(function() {
			_showInformation("記錄儲存成功!");
			_closeEditor();
			_enableNewButton(true);
			_enableSaveButton(false);
			_enableCancelButton(false);
			_enableExportButton(false);
		}, false, "儲存記錄");

		// 取消鈕
		_addCancelButton(function() {
			_closeEditor();
			_enableNewButton(true);
			_enableSaveButton(false);
			_enableCancelButton(false);
			_enableExportButton(false);
		}, false, "取消編輯");

		// 匯出鈕
		_addExportButton(function() {
			if ($('#frmPLCRRCZ').form('validate')) {
				$('#frmPLCRRCZ').submit();
			}
		}, false);

		_addPrintButton(function() {
			_maskSubMenu(true);
		});

		$('#frmPLCRRCZ').form({
			url : '${pageContext.request.contextPath}/PLCRRCZ/export'
		});

		// 百家姓
		$("#customerChooser").customerChooser({
			showTooltip : true,

			tooltip : {
				position : "right",
				content : "百家姓快速查詢"
			},

			onBeforeLoad : function(params) {
				if ($.trim($("#ENO").val()) == "") {
					_showError("請先輸入「諮詢業代」!");
					$("#ENO").focus();
					return false;
				}

				params.eno = $.trim($("#ENO").val()).toUpperCase();
			},

			onSelect : function(data) {
				//$("#ID").val(data.ID).validatebox("validate");
				$("#NAME").val(data.CNAME);
			}
		});

		//報表格式
		$('#selReportFormat').combobox({
			url : '${pageContext.request.contextPath}/PLCRRCZ/getReportFormatList',
			valueField : 'VALUE',
			textField : 'ITEM',
			validType : 'combobox["#selReportFormat"]',

			onLoadSuccess : function(objData) {
				if (objData != null) {
					$.each(objData, function(iIndex, objField) {
						var strValue = objField.VALUE;

						if (strValue == 'pdf') {
							$('#selReportFormat').combobox('select', strValue);
						}
					});

				}
			}
		});

		//繼續率指標
		$('#cntTarget').combobox({
			valueField : 'VALUE',
			textField : 'ITEM',
			validType : 'combobox["#cntTarget"]',
			data : [ {
				VALUE : '13',
				ITEM : '13'
			}, {
				VALUE : '25',
				ITEM : '25'
			} ]
		});

		//單位		
		$('#cntOrg').combogrid({
			url : '${pageContext.request.contextPath}/PLCRRCZ/getUnitInfo',
			panelWidth : 150,
			fitColumns : true,
			idField : 'VALUE',
			textField : 'ITEM',
			validType : 'combogrid["#cntOrg"]',

			columns : [ [ {
				field : 'VALUE',
				title : '代碼',
				width : 50
			}, {
				field : 'ITEM',
				title : '單位',
				width : 150
			} ] ],

			onHidePanel : function() {
				var objDataGrid = $(this).combogrid('grid');
				var objSelectedItem = objDataGrid.datagrid('getSelected');

				var strTextValue = $(this).combogrid('getText');
				strTextValue = $.trim(strTextValue);

				if (strTextValue != '' && objSelectedItem != null) {
					var strText = StrUtil.bindValueItem(objSelectedItem.VALUE, objSelectedItem.ITEM);

					$(this).combobox('setText', strText);
				}
			},

			onShowPanel : function() {
				var strTextValue = $(this).combogrid('getText');
				strTextValue = $.trim(strTextValue);

				if (strTextValue == '') {
					var objDataGrid = $(this).combogrid('grid');
					objDataGrid.datagrid('clearSelections');
				} else {
					var objDataGrid = $(this).combogrid('grid');
					var objSelectedItem = objDataGrid.datagrid('getSelected');

					if (objSelectedItem != null) {
						var strText = StrUtil.bindValueItem(objSelectedItem.VALUE, objSelectedItem.ITEM);
						var iSearchedIndex = strText.indexOf(strTextValue);

						if (iSearchedIndex < 0) {
							objDataGrid.datagrid('clearSelections');
						}
					}
				}
			},

			onLoadSuccess : function(objData) {
				//To set default value
				if (objData != null) {
					if (objData.total != undefined && objData.total > 0) {
						var strValue = objData.rows[0].VALUE;

						EasyuiUtil.combogrid.setValue('cntOrg', strValue);
					}
				}
			}

		});
	});
</script>