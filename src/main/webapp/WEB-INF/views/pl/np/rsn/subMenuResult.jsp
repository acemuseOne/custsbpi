<?xml version="1.0" encoding="UTF-8" ?>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<script type="text/javascript" src="${pageContext.request.contextPath}/resources/js/jqueryFormat/jquery.format-1.2.min.js"></script>

<ul id="informTree" listenTo="orgMenu"></ul>

<!-- 記錄目前點選 #informTree Node ID -->
<input type="hidden" id="informTreeNodeId"></input>

<!-- 以下為 CODE -->
<script type="text/javascript">
	$(document).ready(function() {

		$("#informTree").tree({
			url : "${_contextPath}/PLNPRSN/inform",
			lines : true,
			// formatter() 在 1.3.3 以前版本無法使用
			formatter : informTreeFormatter,

			onBeforeLoad : function(node, param) {
				// "業代"參數
				var employee = _getSelectedEmployee();

				if (!employee) {
					return false;
				}

				param.eno = employee.ENO;
			},

			onLoadSuccess : function(node, data) {
				// 載入完成後, 自動選取
				var displayFrom = $("#displayFrom").val();

				if (displayFrom != "history") {
					var id = $("#informTreeNodeId").val();

					if (id != "") {
						var node = $(this).tree("find", id);

						if (node) {
							$(this).tree("select", node.target);
						}
					}
				}

				_showTooltipForTreeNodeWhenExceedingContainer("panelSubMenuResult", "informTree");
			},

			onSelect : function(node) {
				// 在 header 中顯示"功能選項"路徑
				_triggerChangeSubMenuItemEvent(_getTreeNodeHierarchy("informTree", node, informTreeFormatter));
				var data = null;

				if (node.attributes.level > 0) {
					$("#informTreeNodeId").val(node.id); // 儲存點選 node id
					$("#displayFrom").val("new"); // 記錄目前細項資料查詢條件來源

					data = {
						polMsgKind : node.attributes.polMsgKind,
						polMsgKindName : node.text,
						showMsg : node.attributes.showMsg
					};
				}

				_triggerEvent("informTree", "informTree:select", data); // 觸發 informTree:select 事件
			}
		}).on("orgMenu:select", function(event) { // 接收"組織層級"點選人員事件
			$(this).tree("reload", null); // 更新 tree nodes
		});

		// 加入"全部展開" tool bar
		_addSubMenuTool("icon-expandAll", function() {
			$("#informTree").tree("expandAll");
		});

		// 加入"全部縮合" tool bar
		_addSubMenuTool("icon-collapseAll", function() {
			$("#informTree").tree("collapseAll");
		});
	});

	// 通知提示 Tree node 格式化函式
	function informTreeFormatter(node) {
		if (node.attributes.level == 0) {
			if (node.attributes.docType == "02")
				return "<span class='warning'>" + node.text + "</span>";
			else
				return node.text;
		} else {
			var html = "";

			if (node.attributes.docType == "02")
				html = "<span class='warning'>" + node.text + "</span>";
			else
				html = node.text;

			var totalCount = node.attributes.total

			return html + " (共 <span class='" + (totalCount == 0 ? "normal" : "highlight") + "'>" + $.format.number(totalCount, "##,###,###") + "</span> 筆)";
		}
	}
</script>