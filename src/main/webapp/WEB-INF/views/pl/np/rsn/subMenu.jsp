<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>

<script type="text/javascript" src="${pageContext.request.contextPath}/resources/js/common/util/datetime/DateTimeUtil.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/resources/bundle/jquery-easyui-exts/validatebox-ext.js"></script>

<link rel="stylesheet" type="text/css" href="../custom/cs/cr/mdt/resources/style.css"></link>

<!-- 作業項目 Tree  -->
<ul id="subMenuTree"></ul>

<!-- code -->
<script type="text/javascript">
	$(document).ready(function() {

		// 作業項目 tree node data
		var treeData = [ {
			id : "root",
			text : "諮詢單建立作業",
			iconCls : "icon-inquiryForm",
			attributes : {
				level : 0
			},

			children : [ {
				id : "inquiry",
				text : "一般諮詢",
				iconCls : "icon-log",
				attributes : {
					level : 1,
					url : "inquiry/content"
				}
			}, {
				id : "batch",
				text : "保單進度/延照會/延簽收回條諮詢",
				iconCls : "icon-log",
				attributes : {
					level : 1,
					url : "batch/content"
				}
			}, {
				id : "batchQuery",
				text : "保單進度/延照會/延簽收回條查詢",
				iconCls : "icon-log",
				attributes : {
					level : 1,
					url : "batchQuery/content"
				}
			} ]
		} ];

		// 作業業目 Tree Initialization
		$("#subMenuTree").tree({
			data : treeData,
			lines : true,

			onBeforeSelect : function(node) {
				// 非項目節點不可選取
				if (node.attributes.level == 0)
					return false;
			},

			onLoadSuccess : function() {
				_showTooltipForTreeNodeWhenExceedingContainer("panelSubMenu", "subMenuTree");
			}
		});
	});
</script>