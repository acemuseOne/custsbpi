<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="st" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="sf" uri="http://www.springframework.org/tags/form"%>

<script type="text/javascript" src="${pageContext.request.contextPath}/resources/js/jqueryFormat/jquery.format-1.2.min.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/resources/js/common/util/StrUtil.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/resources/js/common/util/datetime/DateTimeUtil.js"></script>
<link rel="stylesheet" type="text/css" href="${_contextPath}/custom/pl/np/rsn/style.css"></link>

<table id="queryResult" listenTo="queryForm informTree"></table>

<script type="text/javascript">
	$(document).ready(function() {
		// 查詢結果表格
		$("#queryResult").datagrid({
			title : '查詢結果',
			rownumbers : true,
			border: false,
			pagination : true,
			striped : true,
			selectOnCheck : false,
			checkOnSelect : false,
			singleSelect : true,
			fitColumns : false,
			maximized : true,
			remoteSort : true,
			pageNumber : 1,
			pageSize : 25,
			pageList : [ 25, 50, 100 ],

			columns : [ [ {
				field : "F1",
				title : "要保文件",
				width : 55,
				halign : "center",
				align : "center",
				formatter : function(value, row, index) {
					var typeString = "7F5A1&7F5A";
					var paramString = row.CORP + row.CID.substring(0, 2) + "," + row.POLNO + "&" + row.NEW_PROC_NO;
					var previewTitle = "要保文件 - " + row.CONAME + " " + (row.POLNO ? row.POLNO : row.NEW_PROC_NO);

					var pdf = "<a title='要保文件' href='javascript:showPreview(\"" + typeString + "\", \"" + paramString + "\", \"" + previewTitle + "\")'><span class='icon-pdf attachment' /></a>";

					return pdf;
				}
			}, {
				field : "F2",
				title : "照會單",
				width : 42,
				halign : "center",
				align : "center",
				formatter : function(value, row, index) {
					if (row.PROC_REC_DT) {
						var paramString = row.PROC_REC_DT.substring(0, 6) + "," + row.NEW_PROC_NO + "," + row.PROC_REC_DT;
						var previewTitle = "照會單 - " + row.CONAME + " " + (row.POLNO ? row.POLNO : row.NEW_PROC_NO);

						return "<a title='照會單' href='javascript:showPreview(\"7R5A\", \"" + paramString + "\", \"" + previewTitle + "\")'><span class='icon-pdf attachment' /></a>";
					}
				}
			}, {
				field : "F3",
				title : "附件",
				width : 30,
				halign : "center",
				align : "center",
				formatter : function(value, row, index) {
					var previewTitle = "附件 - " + row.CONAME + " " + (row.POLNO ? row.POLNO : row.NEW_PROC_NO);

					return "<a title='附件' href='javascript:showPreview(\"7F5A2\", \"" + row.NEW_PROC_NO + "\", \"" + previewTitle + "\")'><span class='icon-pdf attachment' /></a>";
				}
			}, {
				field : "CONAME",
				title : "保險公司 / 保單號碼",
				width : 130,
				halign : "center",
				align : "left",
				resizable : true,
				sortable : true,
				formatter : function(value, row, index) {
					return value + "<br>" + (row.POLNO ? row.POLNO : row.NEW_PROC_NO);
				}
			}, {
				field : "PID",
				title : "要保人ID / 姓名",
				width : 100,
				halign : "center",
				align : "center",
				resizable : true,
				sortable : true,
				formatter : function(value, row, index) {
					return value + "<br>" + row.PNAME;
				}
			}, {
				field : "CID",
				title : "被保人ID / 姓名",
				width : 100,
				halign : "center",
				align : "center",
				resizable : true,
				sortable : true,
				formatter : function(value, row, index) {
					return value + "<br>" + row.CNAME;
					//return StrUtil.bindValueItem(value, row.CNAME);
				}
			}, {
				field : "DUE_DT",
				title : "投保日期",
				width : 80,
				halign : "center",
				align : "center",
				resizable : true,
				sortable : true,
				formatter : function(value, row, index) {
					return DateTimeUtil.transferIsoDate(value);
				}
			}, {
				field : "PROC_REC_DT",
				title : "照會日期",
				sortable : true,
				width : 80,
				halign : "center",
				align : "center",
				formatter : function(value, row, index) {
					return DateTimeUtil.transferIsoDate(value);
				}
			}, {
				field : "PROC_DUEDATE",
				title : "回覆期限",
				width : 80,
				halign : "center",
				align : "center",
				resizable : true,
				sortable : true,
				formatter : function(value, row, index) {
					return DateTimeUtil.transferIsoDate(value);
				}
			}, {
				field : "POLNO_DT",
				title : "出單日期",
				width : 80,
				halign : "center",
				align : "center",
				resizable : true,
				sortable : true,
				formatter : function(value, row, index) {
					return DateTimeUtil.transferIsoDate(value);
				}
			}, {
				field : "STATUS",
				title : "契約現況",
				width : 68,
				halign : "center",
				align : "center",
				resizable : true,
				sortable : true,
				formatter : function(value, row, index) {
					return value;
					//return StrUtil.bindValueItem(value, row.KIND_NAME);
				}
			}, {
				field : "NOTE",
				title : "備註",
				width : 200,
				halign : "center",
				align : "left",
				resizable : true,
				sortable : true
			}, {
				field : "UUID",
				title : "UUID",
				width : 60,
				halign : "center",
				align : "right",
				resizable : true,
				sortable : true
			} ] ]
		}).on("queryForm:submit", function(event, params) { // 接收 subMenu.jsp 中 #frmQuery 的 submit 事件
			if (!params)
				$(this).datagrid("loadData", []);
			else {
				// 顯示細項內容
				$(this).datagrid({
					url : "${_contextPath}/PLNPRSN/queryHistory",
					pageNumber : 1, // 重置頁數回第一頁
					queryParams : params
				});

				_hideMenu(); // 縮合"功能選單"
				_hidePreview();
			}
		}).on("informTree:select", function(event, data) { // 接收 subMenu.jsp 中 #informTree 的 select 事件
			if (!data)
				$(this).datagrid("loadData", []);
			else {
				var employee = _getSelectedEmployee();

				if (!employee)
					$(this).datagrid("loadData", []);
				else {
					$(this).datagrid({
						url : "${_contextPath}/PLNPRSN/query",
						pageNumber : 1, // 重置頁數回第一頁

						queryParams : {
							eno : employee.ENO,
							polMsgKind : data.polMsgKind
						}
					});

					_hideMenu(); // 縮合"功能選單"
					_hidePreview();
				}
			}
		});
	});

	// 開啟附件
	function showPreview(typeString, paramString, previewTitle) {

		_triggerEvent("queryResult", "queryResult:preview", {
			typeString : typeString,
			paramString : paramString,
			previewTitle : previewTitle
		});
	}
</script>
