<?xml version="1.0" encoding="UTF-8" ?>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>

<script type="text/javascript" src="${_contextPath}/resources/js/common/util/datetime/DateTimeUtil.js"></script>
<link rel="stylesheet" type="text/css" href="${_contextPath}/custom/pl/np/rsn/style.css"></link>

<form id="frmQuery" style="margin: 0 auto" listenTo="orgMenu">
	<table style="width: 300px; font-size: 13px; white-space: nowrap;">
		<tr>
			<td style="width: 55px;">查詢條件:</td>
			<td>
				<!-- 為使 combobox 與 input 可以置中對齊 --> <input id="selQueryColumn" value="prompt" data-options="editable:false" style="width: 90px; vertical-align: middle; display: inline-block"> = <input
				id="columnValue" type="text" style="width: 132px; vertical-align: middle; display: inline-block">
			</td>
		</tr>
		
		<!--
		<tr>
			<td>通知類別:</td>
			<td><input id="selDocType" value="prompt" data-options="editable:false" style="width: 90px; vertical-align: middle; display: inline-block"></td>
		</tr>
		<tr>
			<td>通知項目:</td>
			<td><input type="radio" name="radioPolMsgKind" value="selected" style="vertical-align: middle; display: inline-block" checked><input id="selPolMsgKind" data-options="editable:false"
				style="width: 165px; vertical-align: middle; display: inline-block"><input type="radio" name="radioPolMsgKind" value="all" style="vertical-align: middle; display: inline-block">全部</td>
		</tr>
		-->
		
		<tr>
			<td>查詢範圍:</td>
			<td><input id="dateFrom" name="dateFrom" class="easyui-datebox" data-options="formatter:DateTimeUtil.convertDateToISOString" style="width: 100px"></input> ~ <input id="dateTo" name="dateTo"
				class="easyui-datebox" data-options="formatter:DateTimeUtil.convertDateToISOString" style="width: 100px"></input></td>
		</tr>
		<tr>
			<td colspan="2" style="color: red; text-align: center">※查詢結果為近三個月內投保資料※</td>
		</tr>
		<tr>
			<td></td>
			<td style="padding-top: 5px;"><a id="btnReset" href="#" class="easyui-linkbutton" data-options="iconCls:'icon-clear'" onclick="$('#frmQuery').form('reset');">清空</a><a id="btnQuery" href="#"
				class="easyui-linkbutton" data-options="iconCls:'icon-search'" onclick="$('#frmQuery').submit();" style="margin-left: 10px">查詢</a></td>
		</tr>
	</table>
</form>

<!-- 記錄系統(DB)日期 -->
<input type="hidden" id="sysDate"></input>

<!-- 記錄目前右方內容細項顯示的是上方"歷史資料"的查詢結果(值 history)或點選下方 Tree 的查詢結果(值 new) -->
<input type="hidden" id="displayFrom"></input>

<!-- 以下為 CODE -->
<script type="text/javascript">
	$(document).ready(function() {
		// 取得系統(DB)日期
		$.get("${_contextPath}/system/sysDate", function(data) {
			$("#sysDate").val(data);
		});

		// 歷史資料查詢 Form
		$("#frmQuery").form({
			// 查詢
			onSubmit : function(param) {
				var hierarchy = [ "歷史資料查詢" ]; // 顯示在 header 上的路徑

				// "業代"參數
				var employee = _getSelectedEmployee();

				if (!employee) {
					_showError("請選擇要查詢的業代!");
					_triggerEvent("queryForm", "queryForm:submit", null); // 觸發事件, 傳遞 null 參數以清空細項資料
					return false;
				}

				param.eno = employee.ENO;

				// "查詢欄位"參數
				var queryColumn = $("#selQueryColumn").combobox("getValue");
				var columnValue = $("#columnValue").val();

				if (queryColumn != "prompt") {
					var data = $("#selQueryColumn").combobox("getData");
					var columnName = "";

					for ( var i in data) {
						if (data[i].id == queryColumn) {
							columnName = data[i].text;
							break;
						}
					}

					if (columnValue == "") {
						_showError("請填寫查詢條件「" + columnName + "」值!");
						$("#columnValue").focus();

						return false;
					} else {
						param.queryColumn = queryColumn;
						param.columnValue = columnValue;

						hierarchy = hierarchy.concat([ columnName + " = " + columnValue ]); // 階層路徑加上查詢條件
					}
				}

				// "查詢項目"參數
				var docType = $("#selDocType").combobox("getValue");

				if (docType != "prompt") {
					var data = $("#selDocType").combobox("getData");

					for ( var i in data) {
						if (data[i].id == docType) {
							hierarchy = hierarchy.concat([ data[i].text ]); // 階層路徑加上通知類別
							break;
						}
					}

					var radioPolMsgKind = $("[name='radioPolMsgKind']:checked").val();

					if (radioPolMsgKind == "selected") {
						var polMsgKind = $("#selPolMsgKind").combobox("getValue");

						if (polMsgKind == "prompt") {
							_showError("請選擇「通知項目」!");
							$("#selPolMsgKind").combobox("showPanel");

							return false;
						} else {
							param.polMsgKind = polMsgKind; // 如選擇"通知項目", 則帶"通知項目"參數

							var data = $("#selPolMsgKind").combobox("getData");

							for ( var i in data) {
								if (data[i].id == polMsgKind) {
									hierarchy = hierarchy.concat([ data[i].text ]); // 階層路徑加上查詢項目
									break;
								}
							}
						}
					} else if (radioPolMsgKind == "all") { // 如選擇"全部", 則只帶"通知類別"參數
						param.docType = docType;
					}
				}

				// "查詢日期範圍"參數
				var sysDate = $("#sysDate").val();
				var fromDate = $("#dateFrom").datebox("getValue");
				var toDate = $("#dateTo").datebox("getValue");

				var dateToday = DateTimeUtil.parseDate(DateTimeUtil.transferIsoDate(sysDate));

				var dateLatest = new Date(dateToday); // 範圍最早日期
				dateLatest.setDate(dateLatest.getDate() - 1);
				var latestDate = DateTimeUtil.convertDateToISOString(dateLatest);

				var dateEarliest = new Date(dateToday); // 範圍最晚日期
				dateEarliest.setMonth(dateEarliest.getMonth() - 3);
				var earliestDate = DateTimeUtil.convertDateToISOString(dateEarliest);

				if (fromDate != "" && toDate != "" && fromDate > toDate) {
					_showError("起始日期不得晚於結束日期");
					$("#dateFrom").focus();

					return false;
				}

				if (fromDate == "" || fromDate < earliestDate)
					fromDate = earliestDate;
				else if (fromDate > latestDate)
					fromDate = latestDate;

				if (toDate == "" || toDate > latestDate)
					toDate = latestDate;
				else if (toDate < earliestDate)
					toDate = earliestDate;

				param.dateFrom = fromDate;
				param.dateTo = toDate;

				$("#dateFrom").datebox("setValue", param.dateFrom);
				$("#dateTo").datebox("setValue", param.dateTo);

				hierarchy = hierarchy.concat([ fromDate + " ~ " + toDate ]); // 階層路徑加上查詢日期範圍

				// 發出查詢請求				
				_triggerEvent("queryForm", "queryForm:submit", param); // 觸發 queryForm:submit 事件
				_triggerChangeSubMenuItemEvent(hierarchy); // 顯示階層路徑

				$("#displayFrom").val("history"); // 記錄目前細項資料查詢條件來源

				// 取消 informTree 點選的 node 
				var selected = $("#informTree").tree("getSelected");
				if (selected)
					$("#informTree").tree("unselect", selected.target);

				return false; // 不送出 submit
			}
		}).on("reset", function() { // 接收 reset 事件 
			$("#selPolMsgKind").combobox("loadData", []); // 清空選項
			$("#selPolMsgKind").combobox("enable"); // enable 選單
		}).on("keypress", function(event) {
			if (event.keyCode == 13)
				$(this).submit();
		}).on("orgMenu:select", function(event, node) { // 接收"組織層級"點選人員事件
			if ($("#displayFrom").val() == "history") {
				$(this).submit();
			}
		});

		// "查詢條件"下拉選單
		$("#selQueryColumn").combobox({
			valueField : "id",
			textField : "text",

			data : [ {
				id : "prompt",
				text : "請選擇"
			}, {
				id : "CID",
				text : "被保人ID"
			}, {
				id : "CNAME",
				text : "被保人姓名"
			}, {
				id : "CONAME",
				text : "保險公司"
			}, {
				id : "POLNO",
				text : "保單號碼"
			} ]
		});

		// "通知類別"下拉選單
		$("#selDocType").combobox({
			valueField : "id",
			textField : "text",

			onSelect : function(item) {
				// 更新"通知項目"選項
				$("#selPolMsgKind").combobox("reload");
			},

			data : [ {
				id : "prompt",
				text : "請選擇",
				selected : true
			}, {
				id : "01",
				text : "一般性通知"
			}, {
				id : "02",
				text : "警示性通知"
			} ]
		});

		// "通知項目"下拉選單
		$("#selPolMsgKind").combobox({
			url : "${_contextPath}/PLNPRSN/getPolMsgKinds",
			valueField : "id",
			textField : "text",

			onBeforeLoad : function(param) {
				// 取得"通知類別"選項值, 根據選擇的"通知類別"向後端取得"通知項目"選項
				var docType = $("#selDocType").combobox("getValue");

				if (docType != "" && docType != "prompt") {
					param.docType = docType;
				} else {
					$(this).combobox("loadData", []); // 清空選項
					$(this).combobox("clear"); // 清空值

					return false;
				}
			},

			// loadFilter() 在 1.3.3 以後版本才可用
			loadFilter : function(data) {
				// 在取得的選項前加入"請選擇"選項
				if (data && data.length > 0) {
					data = [ {
						id : "prompt",
						text : "請選擇"
					} ].concat(data);
				}

				return data;
			},

			onLoadSuccess : function() {
				if ($(this).combobox("getData").length > 0) {
					// 預設值為"請選擇"選項
					$(this).combobox("select", "prompt");

					// 列示"通知項目"選項
					if ($("[name='radioPolMsgKind']:checked").val() == "selected")
						$(this).combobox("showPanel");

				}
			}
		});

		// "通知項目" radio button
		$("[name='radioPolMsgKind']").on("change", function() {
			// 選擇"全部"則將"通知項目" disable 選單
			$("#selPolMsgKind").combobox($(this).val() == "selected" ? "enable" : "disable");

			if ($(this).val() == "selected") {
				// 列示"通知項目"選項
				if ($("#selPolMsgKind").combobox("getData").length > 0 && $("#selPolMsgKind").combobox("getValue") == "prompt")
					$("#selPolMsgKind").combobox("showPanel");
			}
		});
	});
</script>