<?xml version="1.0" encoding="UTF-8" ?>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>

<form id="frmAttachment" action="${_contextPath}/PLNPRSN/attachment" style="display: none" method="post" target="frameAttachment" listenTo="queryResult">
	<input type="text" id="typeString" name="typeString"><input type="text" id="paramString" name="paramString">
</form>

<!-- 顯示附件, frameborder 一定要設為 0, 否則在 Chrome 會有 overflow 的現象 -->
<iframe id="frameAttachment" name="frameAttachment" frameborder="0" style="width: 100%; height: 100%"></iframe>

<script type="text/javascript">
	$(document).ready(function() {
		$("#frmAttachment").on("queryResult:preview", function(event, data) {
			$("#typeString").val(data.typeString);
			$("#paramString").val(data.paramString);
			$(this).submit();

			_hideMenu();
			_hideOrgMenu();
			_hideSubMenu();
			_showPreview();

			_setPreviewTitle(data.previewTitle);
		});
	});
</script>