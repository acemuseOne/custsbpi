<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="st" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="sf" uri="http://www.springframework.org/tags/form"%>

<script type="text/javascript" src="${pageContext.request.contextPath}/resources/js/jqueryFormat/jquery.format-1.2.min.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/resources/js/common/util/EasyuiUtil.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/resources/js/common/util/StrUtil.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/resources/js/common/util/datetime/DateTimeUtil.js"></script>

<div class="easyui-layout" data-options="fit:true">
	<div data-options="region:'north',border:false,split:true" style="height: 84px">
		<table id="persistencyInfo" listenTo="subMenu"></table>
	</div>
</div>

<script type="text/javascript">
	$(document).ready(function() {
		$("#persistencyInfo").datagrid({
			maximized : true,
			fitColumns : true,
			border : false,

			data : [ {
				_pSTD : '51.02%',
				pSTD : '0.51023',
				_pSTDN1Y : '98.13%',
				pSTDN1Y : '0.98135'
			} ],

			columns : [ [ {
				field : "_pSTD",
				title : "含承接一年內保單",
				halign : "center",
				align : "center",
				width : 100,

				formatter : function(value, row, index) {
					if (row.pSTD > row.pSTDN1Y)
						return highlightCell(value);

					return value;
				}
			}, {
				field : "_pSTDN1Y",
				title : "不含承接一年內保單",
				halign : "center",
				align : "center",
				width : 100,

				formatter : function(value, row, index) {
					if (row.pSTDN1Y > row.pSTD)
						return highlightCell(value);

					return value;
				}
			}, {
				field : "DUMMY",
				title : "擇優",
				halign : "center",
				align : "center",
				width : 100,

				formatter : function(value, row, index) {
					if ($.isNumeric(row.pSTD) && $.isNumeric(row.pSTDN1Y)) {
						value = row.pSTD >= row.pSTDN1Y ? row._pSTD : row._pSTDN1Y;
					} else if ($.isNumeric(row.pSTD) && !$.isNumeric(row.pSTDN1Y))
						value = row._pSTD;
					else if (!$.isNumeric(row.pSTD) && $.isNumeric(row.pSTDN1Y))
						value = row._pSTDN1Y;
					else
						value = row._pSTD;

					return "<span style='color:blue;font-weight:bold'>" + value + "</span>";
				}
			} ] ]
		}).on("subMenu:select", function(event, node) {
			var level = node.attributes.level;

			if (level > 1) {
				var from = (level == 2 ? node.attributes.from : node.attributes.month);
				var to = (level == 2 ? node.attributes.to : node.attributes.month);

				$(this).datagrid({
					title : node.attributes.year + " 年 " + from + " 月 " + (from != to ? "- " + to + " 月 " : "") + "繼續率比較",
					queryParams : {
						year : node.attributes.year,
						type : node.attributes.type,
						fromMonth : from,
						toMonth : to,
						eno : node.eno
					}
				});
			}
		});
	});

	// 今年或本月利益推估值 highlight 樣式
	function highlightCell(value) {
		return "<span style='background:url(${_contextPath}/custom/pl/np/rsn/award_16x16.png) no-repeat;padding-left:18px'>" + value + "</span>";
	}
</script>