/*
 * 執行初始化動作
 */
$(document).ready(function() {
//	// 設定 layout panel 縮合時的 Title
//	_setPanelCollapseTitle("body"); // 設定縮合後顯示 west/east/north panel 的 title
//
//	// 組織層級 panel resize 時發出 orgMenu:resize 的事件
//	if ($("#panelOrgMenu").length > 0) {
//		$("#panelOrgMenu").panel({
//			onResize : function(width, height) {
//				_triggerEvent("orgMenu", "orgMenu:resize", {
//					width : width,
//					height : height
//				});
//			}
//		});
//	}
//
//	// 預覽區塊
//	if ($("#panelPreview").length > 0) {
//		$("#panelPreview").data("width", $("#panelPreview").panel("options").width); // 記錄 Preview 區塊寬度
//
//		// Resize 處理
//		$("#panelPreview").panel({
//			onResize : function(width, height) {
//				var layoutWidth = $("#layoutLevel0").layout("panel", "center").panel("options").width; // Preview 最大寬度
//
//				if (width < layoutWidth) // 寬度小於最大寬度則記錄寬度
//					$("#panelPreview").data("width", width);
//				else if (width > layoutWidth) // 寬度不得超出最大寬度
//					_setPreviewWidth(layoutWidth);
//			}
//		});
//
//		// 加入切換區塊寬度 tool button
//		_addPreviewTool("icon-toggleSize", function() {
//			var layoutWidth = $("#layoutLevel0").layout("panel", "center").panel("options").width; // Preview 最大寬度
//			var previewWidth = $("#panelPreview").panel("options").width; // 目前寬度
//
//			if (previewWidth < layoutWidth)
//				_setPreviewWidth(layoutWidth);
//			else
//				_setPreviewWidth($("#panelPreview").data("width"));
//		});
//	}
});

// 處理 ajax error 時, 重新開啟原來頁面
$(document).ajaxError(function(event, jqXHR, ajaxSettings, thrownError) {
	if (jqXHR.status == 200) {
		try {
			var responseXML = $.parseXML(jqXHR.responseText);
			var $response = $(responseXML);

			if ($response.find("lawException").length > 0) {
				var level = $response.find("level").text();
				var message = $response.find("message").text();

				if (level == "warning")
					_showWarning("Warning: " + message);
				else
					_showError("Error: " + message);
			}
		} catch (e) {
			_showInformation("Redirecting to login page...");
			location.reload();
		}
	} else if (jqXHR.status == 401) { // 來自 CAS 登入畫面的重新認證要求 (for IE)
		_showInformation("Redirecting to login page...");
		location.reload();
	} else
		_showError("AJAX Request Error: " + jqXHR.status + " (" + jqXHR.statusText + ")");
});

// 擴展 EasyUI Tree 具 unselect 功能
//$.extend($.fn.tree.methods, {
//	unselect : function(jq, target) {
//		$(target).removeClass("tree-node-selected");
//	}
//});

// Override EasyUI datebox date formatter
//$.fn.datebox.defaults.formatter = function(date) {
//	var y = date.getFullYear();
//	var m = date.getMonth() + 1;
//	var d = date.getDate();
//	var dateString = y + "-" + (m < 10 ? "0" : "") + m + "-" + (d < 10 ? "0" : "") + d;
//	
//	return dateString;
//};

// Override EasyUI datebox date parser
//$.fn.datebox.defaults.parser = function(s) {
//	var t = Date.parse(s);
//	
//	if (!isNaN(t)) {
//		return new Date(t);
//	} else {
//		return new Date();
//	}
//};

/**
 * 設定縮合後顯示 west/east/north panel 的 title
 * 
 * @param selector
 */
//function _setPanelCollapseTitle(selector) {
//	$(selector + " .easyui-layout").layout(); // 強制 EasyUI 先產生完所有 Layout 的 DOM, 否則由於 EasyUI javascript 執行時間差的關係, setXXXCollapseTitle() 會找不到 panel
//
//	$(selector + " .easyui-layout").each(function() {
//		// 設定一開始就是縮合狀態的 panel title
//		var panel = $(this).layout("panel", "east");
//
//		if (panel.length > 0)
//			_setEastCollapseTitle(panel);
//
//		panel = $(this).layout("panel", "west");
//
//		if (panel.length > 0)
//			_setWestCollapseTitle(panel);
//
//		// 每次縮合時設定 panel title
//		$(this).layout("panel", "west").panel({
//			onCollapse : function() {
//				_setWestCollapseTitle($(this));
//			}
//		});
//
//		$(this).layout("panel", "east").panel({
//			onCollapse : function() {
//				_setEastCollapseTitle($(this));
//			}
//		});
//
//		$(this).layout("panel", "north").panel({
//			onCollapse : function() {
//				_setNorthCollapseTitle($(this));
//			}
//		});
//	});
//}

/**
 * 設定 east panel 縮合時的 title
 * 
 * @param eastPanel
 */
//function _setEastCollapseTitle(eastPanel) {
//	if (eastPanel.panel("options").collapsed) { // 目前是縮合狀態
//		var candidates = eastPanel.panel("panel").siblings().closest(".layout-expand");
//		var candidate = candidates[0];
//
//		if (candidates.length > 1) { // 同時有 west & east 存在
//			var left = $(candidates[1]).css("left");
//
//			if (left != "0px") // 取 left 值不為 0  的
//				candidate = candidates[1];
//		}
//
//		var body = $(candidate).find(".panel-body"); // div.panel-body
//		$(body).html("<div class='collapsedTitle_V'>" + eastPanel.panel("options").title + "</div>");
//	}
//}

/**
 * 設定 west panel 縮合時的 title
 * 
 * @param westPanel
 */
//function _setWestCollapseTitle(westPanel) {
//	if (westPanel.panel("options").collapsed) { // 目前是縮合狀態
//		var candidates = westPanel.panel("panel").siblings().closest(".layout-expand");
//		var candidate = candidates[0];
//
//		if (candidates.length > 1) { // 同時有 west & east 存在
//			var left = $(candidates[1]).css("left");
//
//			if (left == "0px" || left == "auto") // 取 left 值為 0 或 auto  的
//				candidate = candidates[1];
//		}
//
//		var body = $(candidate).find(".panel-body"); // div.panel-body
//		$(body).html("<div class='collapsedTitle_V'>" + westPanel.panel("options").title + "</div>"); // 只有在第一次縮合後才會產生
//	}
//}

/**
 * 設定 north panel 縮合時的 title
 * 
 * @param northPanel
 */
//function _setNorthCollapseTitle(northPanel) {
//	if (northPanel.panel("options").collapsed) { // 目前是縮合狀態
//		var candidates = northPanel.panel("panel").siblings().closest(".layout-expand");
//		var candidate = candidates[0];
//
//		var body = $(candidate).find(".panel-title"); // div.panel-title
//		$(body).html("<div class='collapsedTitle_H'>" + northPanel.panel("options").title + "</div>"); // 只有在第一次縮合後才會產生
//	}
//}

/**
 * 加入 tool button 至 panel
 * 
 * @param id: panel id
 * @param tool: object of tool
 */
//function _addPanelTool(id, tool) {
//	if ($("#" + id).length == 0)
//		return;
//
//	var tools = $("#" + id).panel("options").tools;
//
//	$("#" + id).panel({
//		tools : !tools ? new Array(tool) : tools.concat(tool)
//	});
//}

/**
 * 加入 tool button 至 "功能選單" panel
 * 
 * @param iconCls 圖示 css class
 * @param handler 執行函式
 */
function _addMenuTool(iconCls, handler) {
	_addPanelTool("panelMenu", {
		iconCls : iconCls,
		handler : handler
	});
}

/**
 * 加入 tool button 至 "組織層級" panel
 * 
 * @param iconCls 圖示 css class
 * @param handler 執行函式
 */
//function _addOrgMenuTool(iconCls, handler) {
//	_addPanelTool("panelOrgMenu", {
//		iconCls : iconCls,
//		handler : handler
//	});
//}

/**
 * 顯示訊息
 * 
 * @param message 訊息內容
 * @param type 訊息類型
 * @param toFadeOut 訊息顯示後是否淡出
 */
//function _showMessage(message, type, toFadeOut) {
//	$(".messageFrame").hide().stop(true, true); // 停止正在執行的 animation
//	var messageFrame;
//
//	switch (type) {
//	case "info":
//		messageFrame = $("#infoMessage");
//		break;
//	case "warn":
//		messageFrame = $("#warningMessage");
//		break;
//	case "error":
//		messageFrame = $("#errorMessage");
//		break;
//	case "processing":
//		messageFrame = $("#processingMessage");
//		break;
//	default:
//		return;
//	}
//
//	messageFrame.text(message).show("slow");
//
//	if (toFadeOut)
//		messageFrame.fadeOut(10000);
//};

/**
 * 顯示 (展開) panel
 * 
 * @param id 區塊所在 layout id
 * @param region 區塊位置
 */
//function _showPanel(id, region) {
//	var panel = $("#" + id).layout("panel", region);
//
//	if (panel.length > 0) {
//		var options = $(panel).panel("options");
//
//		if (options.collapsed) { // 狀態是縮合時才執行 expand
//			$("#" + id).layout("expand", region); // 欲展開未曾縮合過的 panel 會產生錯誤
//		}
//	}
//}

/**
 * 隱藏 (縮合) 區塊
 * 
 * @param id 區塊所在 layout id
 * @param region 區塊位置
 */
//function _hidePanel(id, region) {
//	var panel = $("#" + id).layout("panel", region);
//
//	if (panel.length > 0) {
//		var options = $(panel).panel("options");
//
//		if (!options.collapsed) { // 狀態是展開時才執行 collapse
//			$("#" + id).layout("collapse", region);
//		}
//	}
//}

/**
 * 設定區塊 Title
 * 
 * @param id 區塊 id
 * @param title
 */
//function _setPanelTitle(id, title) {
//	var panel = $("#" + id);
//
//	$("#" + id).panel("setTitle", title);
//}

/**
 * EasyUI Panel 遮罩
 * 
 * @param panelID panel ID
 * @param masked 遮罩/移除遮罩. true: 遮罩區塊; false: 移除區塊遮罩
 * @param msg 顯示訊息
 */
//function _maskPanel(panelID, masked, msg) {
//	if ($("#" + panelID).length == 0)
//		return;
//
//	if (masked)
//		$("#" + panelID).panel("body").mask(msg);
//	else
//		$("#" + panelID).panel("body").unmask();
//}

/*******************************************************************************
 * *************** ********** 以上 function 請勿呼叫使用 ******************************
 ******************************************************************************/

/**
 * 取得置換訊息
 * 
 * @param message 訊息
 * @param arguments 置換字串陣列
 * @returns 置換後訊息
 */
function _getMessage(message, arguments) {
	var msg = message;

	for (var i = 0; i < arguments.length; i++) {
		msg = msg.replace("{" + i + "}", arguments[i]);
	}

	return msg;
}

/**
 * 顯示一般訊息
 * 
 * @param message 訊息內容
 */
//function _showInformation(message) {
//	_showMessage(message, "info", true);
//}

/**
 * 顯示警告訊息
 * 
 * @param message 訊息內容
 */
//function _showWarning(message) {
//	_showMessage(message, "warn", true);
//}

/**
 * 顯示錯誤訊息
 * 
 * @param message 訊息內容
 */
//function _showError(message) {
//	_showMessage(message, "error", true);
//}

/**
 * 顯示進行中訊息
 * 
 * @param message 訊息內容
 */
//function _showProcessing(message) {
//	_showMessage(message, "processing", false);
//}

/**
 * 隱藏進行中訊息
 * 
 * @returns
 */
function _hideProcessing() {
	$(".messageFrame").hide().stop(true, true);
}
/**
 * 取得目前選擇的"功能選單"功能項目物件
 * 
 * @returns 功能項目物件
 */
//function _getSelectedFunction() {
//	if ($("#_functionMenuTree").length > 0) {
//		var node = $("#_functionMenuTree").tree("getSelected");
//
//		if (node)
//			return node.attributes;
//	} else
//		return null;
//}

/**
 * 取得目前選擇的"組織層級"人員物件
 * 
 * @returns 人員物件
 */
//function _getSelectedEmployee() {
//	if ($("#_orgMenuTree").length > 0) {
//		var node = $("#_orgMenuTree").tree("getSelected");
//
//		if (node)
//			return node.attributes;
//	} else
//		return null;
//}

/**
 * 加入 tool button 至 "查詢選項" panel
 * 
 * @param iconCls 圖示 css class
 * @param handler 執行函式
 */
//function _addSubMenuTool(iconCls, handler) {
//	_addPanelTool($("#panelSubMenuQuery").length > 0 ? "panelSubMenuResult" : "panelSubMenu", {
//		iconCls : iconCls,
//		handler : handler
//	});
//}

/**
 * 加入 tool button 至 "預覽" panel
 * 
 * @param iconCls 圖示 css class
 * @param handler 執行函式
 */
//function _addPreviewTool(iconCls, handler) {
//	_addPanelTool("panelPreview", {
//		iconCls : iconCls,
//		handler : handler
//	});
//}

/**
 * 隱藏功能選單
 */
//function _hideMenu() {
//	_hidePanel("layoutLevel1", "west");
//}

/**
 * 顯示功能選單
 */
//function _showMenu() {
//	_showPanel("layoutLevel1", "west");
//}

/**
 * 隱藏組織層級
 */
//function _hideOrgMenu() {
//	_hidePanel("layoutLevel2", "west");
//}

/**
 * 顯示組織層級
 */
//function _showOrgMenu() {
//	_showPanel("layoutLevel2", "west");
//}

/**
 * 隱藏查詢選項
 */
//function _hideSubMenu() {
//	_hidePanel("layoutLevel3", "west");
//}

/**
 * 顯示查詢選項
 */
//function _showSubMenu() {
//	_showPanel("layoutLevel3", "west");
//}

/**
 * 隱藏預覽區塊
 */
//function _hidePreview() {
//	_hidePanel("layoutLevel1", "east");
//}

/**
 * 顯示預覽區塊
 */
//function _showPreview() {
//	_showPanel("layoutLevel1", "east");
//}

/**
 * 隱藏訊息區塊
 */
//function _hideInfoPanel() {
//	_hidePanel("layoutLevel4", "north");
//
//	if ($("#panelBody").length > 0)
//		$("#panelBody").panel("restore"); // center panel 要 restore 才不會留白
//}

/**
 * 顯示訊息區塊
 */
//function _showInfoPanel() {
//	_showPanel("layoutLevel4", "north");
//}

/**
 * 觸發事件
 * 
 * @param ui 元件名稱(自訂)
 * @param event 事件名稱(命名規則為 ui:event, eg. orgMenu:select)
 * @param data 傳給接收此事件的元件的資料
 */
function _triggerEvent(ui, event, data) {
	$("[listenTo~='" + ui + "']").trigger(event, [ data ]); // 觸發事件 ui:event, 注意: 要傳 [data], 只傳 data 會被接收事件函數當成個別參數而非陣列!
}

/**
 * 取得指定 Tree Node 階層陣列
 * 
 * @param treeId Tree id
 * @param node Tree node
 * @param formatter Tree Node 的格式化函式, 未指定則以 node.text 顯示
 * @returns {Array} 階層陣列: [0] 階層一, [1] 階層二, ...
 */
//function _getTreeNodeHierarchy(treeId, node, formatter) {
//	var array = [];
//	var parent = node;
//
//	while (parent) {
//		var text = formatter ? formatter(parent) : parent.text;
//		array = [ text ].concat(array);
//		parent = $("#" + treeId).tree("getParent", parent.target);
//	}
//
//	return array;
//}

function _getTreeNodeHierarchy(treeId, node, formatter) {
	var array = [];
	var parent = node;

	while (parent.parentId != undefined ||parent.parentId != null) {
		var text = formatter ? formatter(parent) : parent.text;
		array = [ text ].concat(array);
		debugger
		parent = $("#" + treeId).treeview("getParent", parent.nodeId);
	}

	return array;
}
/**
 * 觸發 subMenu:changeMenuItem 事件, 由事件處理函式載入傳入的 data 階層陣列, 並顯示其階層路徑於 header.jsp 中
 * 
 * @param data 階層陣列
 */
function _triggerChangeSubMenuItemEvent(data) {
	_triggerEvent("subMenu", "subMenu:changeMenuItem", data);
}

/**
 * 替換 body 區塊內容
 * 
 * @param url 網址 (僅限同網域內網址)
 */
//function _loadBody(url) {
//	// 先偵測 session 是否 timeout, 防止因 timeout 而將登入畫面在 panel 中開啟
//	$.ajax({
//		url : $("#_contextPath").text() + "/system/dummy",
//		dataType : "xml",
//		success : function() {
//			$("#panelBody").panel("refresh", url);
//		}
//	});
//}

/**
 * 替換 Pagebody 區塊內容
 * 
 * @param url 網址 (僅限同網域內網址)
 */
function _loadPagebody(url, data) {
	// 先偵測 session 是否 timeout, 防止因 timeout 而將登入畫面在 panel 中開啟
	$.ajax({
		url : $("#_contextPath").text() + "/system/dummy",
		dataType : "xml",
		success : function() {
			if(data){
				 $("#page_body").load(url,data);
				 debugger
			}
			else{
				$("#page_body").load(url);
			}
		}
	});
}

/**
 * 替換 preview 區塊內容
 * 
 * @param url 網址 (僅限同網域內網址)
 */
//function _loadPreview(url) {
//	// 先偵測 session 是否 timeout, 防止因 timeout 而將登入畫面在 panel 中開啟
//	$.ajax({
//		url : $("#_contextPath").text() + "/system/dummy",
//		dataType : "xml",
//		success : function() {
//			$("#panelPreview").panel("refresh", url);
//		}
//	});
//}

/**
 * 替換 subMenu 右方區塊內容(包含 information, body, description)
 * 
 * @param url 網址 (僅限同網域內網址)
 */
//function _loadContent(url) {
//	// 先偵測 session 是否 timeout, 防止因 timeout 而將登入畫面在 panel 中開啟
//	$.ajax({
//		url : $("#_contextPath").text() + "/system/dummy",
//		dataType : "xml",
//		success : function() {
//			$("#layoutLevel3").layout("panel", "center").panel("refresh", url);
//		}
//	});
//}

/**
 * 設定"預覽"區塊 Title
 * 
 * @param title
 */
//function _setPreviewTitle(title) {
//	_setPanelTitle("panelPreview", title);
//	_setEastCollapseTitle($("#panelPreview"));
//}

/**
 * 設定預覽區塊寬度
 * 
 * @param id
 * @param width
 */
//function _setPreviewWidth(width) {
//	$("#panelPreview").panel("resize", {
//		width : width
//	});
//
//	$("#layoutLevel1").layout("resize"); // 區塊所在 layout 要 resize
//}

/**
 * 設定訊息區塊 Title
 * 
 * @param title
 */
//function _setInformationTitle(title) {
//	$("#panelInformation").panel("setTitle", title);
//}

/**
 * 當樹節點 title 長度超出其容器邊界範圍時顯示tooltip
 * 
 * @param containerId 包含 Tree UI 之容器 ID
 * @param treeId tree ID
 */
//function _showTooltipForTreeNodeWhenExceedingContainer(containerId, treeId) {
//	$("#" + treeId + " span.tree-title").mouseover(function() {
//		var containerRightPos = $("#" + containerId)[0].clientWidth + $("#" + containerId).offset().left;
//		var titleRightPos = $(this).offset().left + $(this).width();
//
//		if (titleRightPos > containerRightPos) { // 如文字區塊最右 X 座標超出容器最右 X 座標
//
//			$(this).tooltip({
//				position : 'right',
//				content : $(this).text(),
//				onShow : function() {
//					$(this).tooltip('tip').css({
//						backgroundColor : 'black',
//						borderColor : 'white',
//						color : 'white',
//						borderRadius : '5px',
//						left : containerRightPos + 5
//					});
//				},
//				onHide : function() {
//					$(this).tooltip("destroy"); // 不顯示時將 tooltip 移除
//				}
//			}).tooltip("show");
//		}
//	});
//}

/**
 * 偵測目前 session 是否 timeout. 是則發出 ajaxError 事件, 然後導向登入畫面
 */
function _testConnection() {
	$.ajax({
		url : $("#_contextPath").text() + "/system/dummy",
		dataType : "xml" // 回傳資料非 xml 格式會發出 ajaxError
	});
}

/**
 * 開啟編輯區塊
 * 
 * @param title 編輯區塊標題. 如未指定, 則為 formTitle 設定值
 */
//function _openEditor(title) {
//	if ($("#_form").lenght == 0) {
//		alert("form attribute is not defined in functions.xml!");
//		return;
//	}
//
//	if (!title)
//		title = $("#_form").attr("title");
//
//	$("#layoutLevel6").layout("add", {
//		id : "panelEditor",
//		region : "north",
//		noheader : false,
//		height : $("#_form").attr("height"),
//		split : true,
//		collapsible : true,
//		bodyCls : "editorBody",
//		title : title != "" ? title : "<span></span>" // 防止因 title 為空時造成 header 區塊不顯示
//	});
//
//	//設定 layout panel 縮合時的 Title
//	_setPanelCollapseTitle("#layoutLevel5"); // 設定縮合後顯示 west/east/north panel 的 title	
//	$("#layoutLevel5").layout("resize"); // layout 要 resize 以防止在 IE 發生畫面錯亂
//
//	$("#panelEditor").append($("#_form >"));
//}

/**
 * 關閉編輯區塊
 */
//function _closeEditor() {
//	if ($("#_form").lenght == 0) {
//		alert("form attribute is not defined in functions.xml!");
//		return;
//	}
//
//	$("#_form").append($("#panelEditor >"));
//	$("#layoutLevel6").layout("remove", "north");
//}
//
//function _setEditorHeight(height) {
//	$("#panelEditor").panel("resize", {
//		height : height
//	});
//
//	$("#layoutLevel6").layout("resize"); // 區塊所在 layout 要 resize
//}

/**
 * 「功能選單」區塊遮罩
 * 
 * @param masked 遮罩/移除遮罩. true: 遮罩區塊; false: 移除區塊遮罩
 * @param msg 顯示訊息
 */
function _maskMenu(masked, msg, msg) {
	_maskPanel("panelMenu", masked, msg, msg);
}

/**
 * 「組織層級」遮罩
 * 
 * @param masked 遮罩/移除遮罩. true: 遮罩區塊; false: 移除區塊遮罩
 * @param msg 顯示訊息
 */
function _maskOrgMenu(masked, msg) {
	_maskPanel("panelOrgMenu", masked, msg);
}

/**
 * 「查詢選項/作業項目」區塊遮罩
 * 
 * @param masked 遮罩/移除遮罩. true: 遮罩區塊; false: 移除區塊遮罩
 * @param msg 顯示訊息
 */
function _maskSubMenu(masked, msg) {
	_maskPanel("panelSubMenu", masked, msg);
}

/**
 * 右半部區塊遮罩
 * 
 * @param masked 遮罩/移除遮罩. true: 遮罩區塊; false: 移除區塊遮罩
 * @param msg 顯示訊息
 */
function _maskContent(masked, msg) {
	_maskPanel("panelContent", masked, msg);
}

/**
 * 「按鈕」區塊遮罩
 * 
 * @param masked 遮罩/移除遮罩. true: 遮罩區塊; false: 移除區塊遮罩
 * @param msg 顯示訊息
 */
function _maskButton(masked, msg) {
	_maskPanel("panelButton", masked, msg);
}

/**
 * 「Body」區塊遮罩
 * 
 * @param masked 遮罩/移除遮罩. true: 遮罩區塊; false: 移除區塊遮罩
 * @param msg 顯示訊息
 */
function _maskBody(masked, msg) {
	_maskPanel("panelBody", masked, msg);
}

/**
 * 「訊息」區塊遮罩
 * 
 * @param masked 遮罩/移除遮罩. true: 遮罩區塊; false: 移除區塊遮罩
 * @param msg 顯示訊息
 */
function _maskInformation(masked, msg) {
	_maskPanel("panelInformation", masked, msg);
}

/**
 * 「頁首」區塊遮罩
 * 
 * @param masked 遮罩/移除遮罩. true: 遮罩區塊; false: 移除區塊遮罩
 * @param msg 顯示訊息
 */
function _maskHeader(masked, msg) {
	_maskPanel("panelHeader", masked, msg);
}

/**
 * 「頁尾」區塊遮罩
 * 
 * @param masked 遮罩/移除遮罩. true: 遮罩區塊; false: 移除區塊遮罩
 * @param msg 顯示訊息
 */
function _maskFooter(masked, msg) {
	_maskPanel("panelFooter", masked, msg);
}

/**
 * 「說明」區塊遮罩
 * 
 * @param masked 遮罩/移除遮罩. true: 遮罩區塊; false: 移除區塊遮罩
 * @param msg 顯示訊息
 */
function _maskDescription(masked, msg) {
	_maskPanel("panelDescription", masked, msg);
}

/**
 * 「預覽」區塊遮罩
 * 
 * @param masked 遮罩/移除遮罩. true: 遮罩區塊; false: 移除區塊遮罩
 * @param msg 顯示訊息
 */
function _maskPreview(masked, msg) {
	_maskPanel("panelPreview", masked, msg);
}

/**
 * 「編輯」區塊遮罩
 * 
 * @param masked 遮罩/移除遮罩. true: 遮罩區塊; false: 移除區塊遮罩
 * @param msg 顯示訊息
 */
function _maskEditor(masked, msg) {
	_maskPanel("panelEditor", masked, msg);
}

/**
 * 整個頁面區塊遮罩
 * 
 * @param masked 遮罩/移除遮罩. true: 遮罩區塊; false: 移除區塊遮罩
 * @param msg 顯示訊息
 */
function _maskAll(masked, msg) {
	_maskPanel("panelComposite", masked, msg);
}

/**
 * 製作combobox
 * 
 * @param select 
 * @param data 下拉選單的data格視為json
 * @param text 選單顯示的文字
 * @param id   選項的value
 */
function _makeCombobox(select, data, text, value){
	 var $select = $('#'+select);
	 $select.html('');
	   $.each(data, function(key, val){
	     $select.append('<option value ="' + eval('val.' + value ) + '">' + eval('val.' + text ) +'</option>');
	   })
}

/**
 * 清空form
 * 
 * @param form 
 */
function _resetMyForm(form) {
    document.getElementById(form).reset();
}


/**
 * 顯示訊息
 * 
 * @param message 訊息內容
 * @param type 訊息類型
 * @param toFadeOut 訊息顯示後是否淡出
 */
function _showMessage(message, type, toFadeOut) {
	var $msg = $('#processingMessage');
	$msg.html('');
	switch (type) {
	case "info":
		$msg.append('<div class="alert alert-info fade in" id ="info">');
		$( '<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>' ).appendTo( $( "#info" ) );
		$( '<strong>'+message+'</strong></div>' ).appendTo( $( "#info" ) );
		break;
	case "warn":
		$msg.append('<div class="alert alert-warning fade in" id ="warn">');
		$( '<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>' ).appendTo( $( "#warn" ) );
		$( '<strong>'+message+'</strong></div>' ).appendTo( $( "#warn" ) );
		break;
	case "error":
		$msg.append('<div class="alert alert-danger fade in" id ="error">');
		$( '<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>' ).appendTo( $( "#error" ) );
		$( '<strong>'+message+'</strong></div>' ).appendTo( $( "#error" ) );
		break;
	case "sucess":
		$msg.append('<div class="alert alert-success fade in" id ="sucess">');
		$( '<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>' ).appendTo( $( "#sucess" ) );
		$( '<strong>'+message+'</strong></div>' ).appendTo( $( "#sucess" ) );
		break;
	default:
		return;
	}
	if (toFadeOut)
	$("#"+type).fadeOut(3000);

};

/**
 * 顯示成功訊息
 * 
 * @param message 訊息內容
 */
function _showSucess(message) {
	_showMessage(message, "sucess", true);
}

/**
 * 顯示一般訊息
 * 
 * @param message 訊息內容
 */
function _showInformation(message) {
	_showMessage(message, "info", true);
}

/**
 * 顯示警告訊息
 * 
 * @param message 訊息內容
 */
function _showWarning(message) {
	_showMessage(message, "warn", true);
}

/**
 * 顯示錯誤訊息
 * 
 * @param message 訊息內容
 */
function _showError(message) {
	_showMessage(message, "error", true);
}

/**
 * 把資料param input 到form裡面
 * 
 * @param message 訊息內容
 */
function _formLoadData(form, data) {
	$.each($('#'+form).serializeArray(), function(i, field) {
		 $('[name='+field.name+']').val(eval('data.' + field.name ));
		});
}

/**
 * 遮罩全頁面
 * 
 * @param message 訊息內容
 */
var myApp;
myApp = myApp || (function () {
    var pleaseWaitDiv = $('<div class="modal hide" id="pleaseWaitDialog" data-backdrop="static" data-keyboard="false"><div class="modal-header"><h1>Processing...</h1></div><div class="modal-body"><div class="progress progress-striped active"><div class="bar" style="width: 100%;"></div></div></div></div>');
    return {
        showPleaseWait: function() {
        	pleaseWaitDiv.modal();
        },
        hidePleaseWait: function () {
        	pleaseWaitDiv.modal('hide');
        },

    };
})();


/**
 * 
 * <%//如果startDate > endDate, return -1 ; startDate = endDate, return 0, startDate < endDate, return 1%>
 *<%//只有起始值，return2, 只有結束值，return 3, 兩者都沒值，return 4%>
 */
function compareDate(startDate, endDate){
	var d1;
	var d2;
	if(startDate){
		d1 = DateTimeUtil.parseDate(startDate);
	}
	if(endDate){
		d2 = DateTimeUtil.parseDate(endDate);
	} 
	
	if(d1 && d2){
		if(d1 == d2){
			return 0;
		}else if(d1 > d2){
			return -1
		}else{
			return 1;			
		}
	}else if(d1 && !d2){
		//<%//只有起始值%>
		return 2;
	}else if(!d1 && d2){
		//<%//只有結束值%>
		return 3;
	}else{
		//<%//沒起始值，也沒結束值%>
		return 4;
	}
}

function getStatus(formId){
	var param = new Object();
	$('#'+formId).serializeArray().map(function(item) {
		if(item.value){
			eval('param.' + item.name + '= "' + item.value.trim() + '"');	//param.actTypeName = '三月值教班'
		}else{
			eval('param.' + item.name + '= ""' ); //param.actTypeName = ''
		}
	});
	
	return param
}	

function ConvertFormToJSON(formId){
    var array = $('#'+formId).serializeArray();
    var json = {};
    
    jQuery.each(array, function() {
        json[this.name] = this.value.trim() || '';
    });
    
    return json;
}

/** 
 * 關閉遮罩
 */
function openMask(){
	$("#myModal").modal('show');
}
/** 
 * 開啟遮罩
 */
function closeMask(){
	$('#myModal').modal('hide');
}
